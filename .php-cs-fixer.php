<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('include/language')
    ->exclude('install/language')
    ->exclude('vendor')
    ->exclude('node_modules')
    ->exclude('template')
    ->exclude('addons')
    ->exclude('tests/_support/_generated/')
    ->notPath('c3.php')
    ->in('src')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
        '@PSR12' => true,
        '@PHP80Migration' => true,
        //'@PHP74Migration' => true,
        //'strict_param' => true,
        //'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
