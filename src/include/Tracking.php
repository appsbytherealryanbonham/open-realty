<?php

declare(strict_types=1);

namespace OpenRealty;

use BrowscapPHP\Browscap;
use BrowscapPHP\Exception;
use BrowscapPHP\Formatter\LegacyFormatter;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use MatthiasMullie\Scrapbook\Adapters\Flysystem;
use MatthiasMullie\Scrapbook\Psr16\SimpleCache;
use Monolog\Logger;

class Tracking extends BaseClass
{
    public function viewStatistics(string $app_status_text = ''): string
    {
        $page = $this->newPageAdmin();
        $display = '';
        $yes_no = [0 => 'No', 1 => 'Yes'];

        $page->loadPage($this->config['admin_template_path'] . '/site_statistics.html');
        $html = $page->getTemplateSection('enable_tracking_block');
        $html = $page->formOptions($yes_no, $this->config['enable_tracking'] ? '1' : '0', $html);
        $page->replaceTemplateSection('enable_tracking_block', $html);

        $html = $page->getTemplateSection('enable_tracking_crawlers_block');
        $html = $page->formOptions($yes_no, $this->config['enable_tracking_crawlers'] ? '1' : '0', $html);
        $page->replaceTemplateSection('enable_tracking_crawlers_block', $html);

        $page->replaceTag('application_status_text', $app_status_text);

        $page->replacePermissionTags();
        $page->replaceLangTemplateTags();
        $page->autoReplaceTags('', true);
        $display .= $page->returnPage();
        return $display;
    }

    /**
     * @return boolean
     */
    public function record(float $render_time): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        if ($this->config['enable_tracking'] == 0 || php_sapi_name() == 'cli') {
            return false;
        }
        $cacheDir = $this->config['basepath'] . '/files/browsercap_cache';

        $fileCache = new LocalFilesystemAdapter($cacheDir);
        $filesystem = new Filesystem($fileCache);
        $cache = new SimpleCache(
            new Flysystem($filesystem)
        );

        $logger = new Logger('name');

        $bc = new Browscap($cache, $logger);
        $arrayFormatter = new LegacyFormatter();
        $bc->setFormatter($arrayFormatter);

        $tracking_ip = $_SERVER['HTTP_X_FORWARD_FOR'] ?? $_SERVER['REMOTE_ADDR'] ?? '';
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $tacking_agentstring = $_SERVER['HTTP_USER_AGENT'];
            try {
                $browser_data = (array)$bc->getBrowser($tacking_agentstring);
                $tracking_browser = $browser_data['Browser'];
                $tracking_browserversion = $browser_data['Version'];
                $tracking_os = $browser_data['Platform'];
                $is_crawler = $browser_data['Crawler'];
                if ($is_crawler && $this->config['enable_tracking_crawlers']) {
                    //Skip Crawlers
                    return false;
                }
            } catch (Exception) {
                return false;
            }
        } else {
            $tacking_agentstring = '';
            $tracking_browser = '';
            $tracking_browserversion = '';
            $tracking_os = '';
        }

        $tracking_referal = $_SERVER['HTTP_REFERER'] ?? '';
        $tracking_user = $_SESSION['userID'] ?? 0;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']) {
            $tracking_link_uri = 'https';
        } else {
            $tracking_link_uri = 'http';
        }
        $host = $_SERVER['HTTP_HOST'] ?? '';
        $uri = $_SERVER['REQUEST_URI'] ?? '';
        $tracking_link_uri .= '://' . $host . $uri;
        //Determine Interal Location
        $link_id = 0;
        $link_type = '';
        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'listingview':
                    $link_type = 'listingview';
                    if (isset($_GET['listingID'])) {
                        $link_id = $_GET['listingID'];
                    }
                    break;
                case 'blog_view_article':
                    $link_type = 'blog_view_article';
                    if (isset($_GET['ArticleID'])) {
                        $link_id = $_GET['ArticleID'];
                    }
                    break;
                case 'blog_archive':
                    $link_type = 'blog_archive';
                    break;
                case 'page_display':
                    $link_type = 'page_display';
                    if (isset($_GET['PageID'])) {
                        $link_id = $_GET['PageID'];
                    }
                    break;
                case 'agent':
                    $link_type = 'agent';
                    if (isset($_GET['user'])) {
                        $link_id = $_GET['user'];
                    }
                    break;
                case 'blog_tag':
                    $link_type = 'blog_tag';
                    if (isset($_GET['tag_id'])) {
                        $link_id = $_GET['tag_id'];
                    }
                    break;
                case 'blog_cat':
                    $link_type = 'blog_cat';
                    if (isset($_GET['cat_id'])) {
                        $link_id = $_GET['cat_id'];
                    }
                    break;
                case 'view_listing_image':
                    $link_type = 'view_listing_image';
                    if (isset($_GET['image_id'])) {
                        $link_id = $_GET['image_id'];
                    }
                    break;
                case 'searchpage':
                    $link_type = 'searchpage';
                    break;
                case 'searchresults':
                    $link_type = 'searchresults';
                    break;
                case 'blog_index':
                    $link_type = 'blog_index';
                    break;
                case 'view_users':
                    $link_type = 'view_users';
                    break;
                case 'signup':
                    if (isset($_GET['type'])) {
                        if ($_GET['type'] == 'member') {
                            $link_type = 'member_signup';
                        } elseif ($_GET['type'] == 'agent') {
                            $link_type = 'agent_signup';
                        }
                    }
                    break;
                case 'member_login':
                    $link_type = 'member_login';
                    break;
                case 'view_favorites':
                    $link_type = 'view_favorites';
                    break;
                case 'calculator':
                    $link_type = 'calculator';
                    break;
                case 'view_saved_searches':
                    $link_type = 'view_saved_searches';
                    break;
                case 'edit_profile':
                    $link_type = 'edit_profile';
                    break;
                case 'index':
                    $link_type = 'index';
                    break;
                default:
                    return false;
            }
        }
        if ($link_type == '') {
            return false;
        }
        //Make SQL Safe
        $sql_link_type = $misc->makeDbSafe($link_type);
        $sql_link_id = intval($link_id);
        $sql_tracking_ip = $misc->makeDbSafe($tracking_ip);
        $sql_tacking_agentstring = $misc->makeDbSafe($tacking_agentstring);
        $sql_tracking_browser = $misc->makeDbSafe($tracking_browser);
        $sql_tracking_browserversion = $misc->makeDbSafe($tracking_browserversion);
        $sql_tracking_os = $misc->makeDbSafe($tracking_os);
        $sql_tracking_referal = $misc->makeDbSafe($tracking_referal);
        $sql_tracking_user = intval($tracking_user);
        $sql_tracking_link_uri = $misc->makeDbSafe($tracking_link_uri);
        $sql_tracking_timestamp = $misc->makeDbSafe(time());
        $sql_tracking_loadtime = $misc->makeDbSafe($render_time);

        $sql = 'INSERT INTO ' . $this->config['table_prefix_no_lang'] . "tracking
		(tracking_timestamp,userdb_id,tracking_ip,tracking_referal,tracking_link_type,
		tracking_link_type_id,tracking_link_url,tracking_agentstring,tracking_browser,
		tracking_browserversion,tracking_os,tracking_loadtime)
		VALUES
		($sql_tracking_timestamp,$sql_tracking_user,$sql_tracking_ip,$sql_tracking_referal,$sql_link_type,
		$sql_link_id,$sql_tracking_link_uri,$sql_tacking_agentstring,$sql_tracking_browser,
		$sql_tracking_browserversion,$sql_tracking_os,$sql_tracking_loadtime);";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return true;
    }

    public function clearStatisticsLog(): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $display = '';
        //$display .= "<h3>$lang[log_delete]</h3>";
        // Check for Admin privs before doing anything
        if ($_SESSION['admin_privs'] == 'yes') {
            // find the number of log items
            $sql = 'TRUNCATE TABLE ' . $this->config['table_prefix_no_lang'] . 'tracking';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logAction($sql);
                $display .= $this->viewStatistics($lang['log_clear_error']);
            } else {
                $misc->logAction($lang['log_reset']);
                $display .= $this->viewStatistics($lang['log_cleared']);
            }
        } else {
            $display .= $this->viewStatistics($lang['clear_log_need_privs']);
        }
        return $display;
    }
}
