<?php

/**
 *  Filemanager PHP class
 *
 *  filemanager.class.php
 *  class for the filemanager.php connector
 *
 * @license    MIT License
 * @author     Riaan Los <mail (at) riaanlos (dot) nl>
 * @copyright  Authors
 */

declare(strict_types=1);

namespace CKEditorFileManager;

use JetBrains\PhpStorm\NoReturn;
use OpenRealty\Login;
use OpenRealty\Api\Commands\SettingsApi;
use OpenRealty\Api\Database;

class Filemanager
{
    protected array $config = [];
    protected array $get = [];
    protected array $post = [];
    protected array $properties = [];
    protected array $item = [];
    protected string $root = '';

    protected array $params = [];

    /**
     *  Check if user is authorized
     *
     * @return boolean true is access granted, false if no access
     */
    public function auth(): bool
    {
        // You can insert your own code over here to check if the user is authorized.
        // If you use a session variable, you've got to start the session first (session_start())

        //if (session_id()=='') session_start();
        $apiDB = new Database();
        $dbh = $apiDB->getDatabaseHandler();
        $settingApi = new SettingsApi($dbh);
        $ORconfig = $settingApi->read(['is_mobile' => false])['config'];

        $login = new Login($dbh, $ORconfig);
        $login_status = $login->loginCheck('Agent');
        if ($login_status !== true) {
            return false;
        } else {
            return true;
        }
    }

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->root = str_replace('connectors/php/filemanager.php', '', $_SERVER['SCRIPT_FILENAME'] ?? '');
        $this->properties = [
            'Date Created' => null,
            'Date Modified' => null,
            'Height' => null,
            'Width' => null,
            'Size' => null,
        ];
        $this->setParams();
    }

    #[NoReturn] public function error(string $string, bool $textarea = false)
    {
        $array = [
            'Error' => $string,
            'Code' => '-1',
            'Properties' => $this->properties,
        ];
        if ($textarea) {
            echo '<textarea>' . json_encode($array) . '</textarea>';
        } else {
            echo json_encode($array);
        }
        exit();
    }

    public function lang(string $string): string
    {
        /** @psalm-suppress UnresolvableInclude */
        require(dirname(__FILE__) . '/lang/en.php');
        $language = strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? '', 0, 2));
        if (file_exists('lang/' . $language . '.php')) {
            /** @psalm-suppress UnresolvableInclude */
            require('lang/' . $language . '.php');
        }
        /** @psalm-suppress UndefinedVariable */
        if (isset($lang[$string]) && $lang[$string] != '') {
            return $lang[$string];
        } else {
            return 'Language string error on ' . $string;
        }
    }

    public function getvar(string $var): bool
    {
        if (!isset($_GET[$var]) || $_GET[$var] == '') {
            $this->error(sprintf($this->lang('INVALID_VAR'), $var));
        } else {
            $this->get[$var] = $_GET[$var];
            return true;
        }
    }

    public function postvar(string $var): bool
    {
        if (!isset($_POST[$var]) || $_POST[$var] == '') {
            $this->error(sprintf($this->lang('INVALID_VAR'), $var));
        } else {
            $this->post[$var] = $_POST[$var];
            return true;
        }
    }

    public function getinfo(): array
    {
        $this->item = [];
        $this->item['properties'] = $this->properties;
        $this->getFileInfo();
        $full_path = $this->get['path'];
        /*if($this->config['add_host']) {
            $full_path = 'http://' . $_SERVER['HTTP_HOST'] . $this->get['path'];
        }*/
        if (array_key_exists('path', $this->item)) {
            $full_path = $this->item['path'];
        }
        return [
            'Path' => $full_path,
            'realpath' => array_key_exists('realpath', $this->item) ? $this->item['realpath'] : '',
            'Filename' => array_key_exists('filename', $this->item) ? $this->item['filename'] : '',
            'File Type' => array_key_exists('filetype', $this->item) ? $this->item['filetype'] : '',
            'Preview' => array_key_exists('preview', $this->item) ? $this->item['preview'] : '',
            'Properties' => $this->item['properties'],
            'Error' => "",
            'Code' => 0,
        ];
    }

    /**
     * @return array
     */
    public function getfolder(): array
    {
        $array = [];
        if (!is_dir($this->get['path'])) {
            $this->error(sprintf($this->lang('DIRECTORY_NOT_EXIST'), htmlentities($this->get['path'])));
        }
        if (!$handle = opendir($this->get['path'])) {
            $this->error(sprintf($this->lang('UNABLE_TO_OPEN_DIRECTORY'), htmlentities($this->get['path'])));
        } else {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && is_dir($file)) {
                    $array[$this->get['path'] . $file] = [
                        'Path' => $this->hostPrefixed($this->get['path'] . $file),
                        'Filename' => $file,
                        'File Type' => 'dir',
                        'Preview' => $this->hostPrefixed($this->config['icons']['path'] . $this->config['icons']['directory']),
                        'Properties' => [
                            'Date Created' => null,
                            'Date Modified' => null,
                            'Height' => null,
                            'Width' => null,
                            'Size' => null,
                        ],
                        'Error' => "",
                        'Code' => 0,
                    ];
                } elseif ($file != "." && $file != ".." && !str_starts_with($file, '.')) {
                    $this->item = [];
                    $this->item['properties'] = $this->properties;
                    $this->getFileInfo($this->get['path'] . $file);

                    if (!isset($this->params['type']) || (isset($this->params['type']) && $this->params['type'] == 'Images' && array_key_exists('filetype', $this->item) && in_array($this->item['filetype'], $this->config['images']))) {
                        $array[$this->get['path'] . $file] = [
                            'Path' => $this->get['path'] . $file,
                            'Filename' => array_key_exists('filename', $this->item) ? $this->item['filename'] : '',
                            'File Type' => array_key_exists('filetype', $this->item) ? $this->item['filetype'] : '',
                            'Preview' => array_key_exists('preview', $this->item) ? $this->item['preview'] : '',
                            'Properties' => $this->item['properties'],
                            'Error' => "",
                            'Code' => 0,
                        ];
                    }
                }
            }
            closedir($handle);
        }
        return $array;
    }

    public function rename(): array
    {
        if (str_ends_with($this->get['old'], '/')) {
            $this->get['old'] = substr($this->get['old'], 0, (strlen($this->get['old']) - 1));
        }
        $tmp = explode('/', $this->get['old']);
        $filename = $tmp[(sizeof($tmp) - 1)];
        $path = str_replace('/' . $filename, '', (string)$this->get['old']);
        if (!rename($this->get['old'], $path . '/' . $this->get['new'])) {
            if (is_dir($this->get['old'])) {
                $this->error(sprintf($this->lang('ERROR_RENAMING_DIRECTORY'), $filename, $this->get['new']));
            } else {
                $this->error(sprintf($this->lang('ERROR_RENAMING_FILE'), $filename, $this->get['new']));
            }
        }
        return [
            'Error' => "",
            'Code' => 0,
            'Old Path' => $this->get['old'],
            'Old Name' => $filename,
            'New Path' => $path . '/' . $this->get['new'],
            'New Name' => $this->get['new'],
        ];
    }

    public function delete(): array
    {
        $details = $this->getinfo();
        $this->get['realpath'] = $details['realpath'];
        if (is_dir($this->get['realpath'])) {
            $this->unlinkRecursive($this->get['realpath']);
            return [
                'Error' => "",
                'Code' => 0,
                'Path' => $this->get['realpath'],
            ];
        } elseif (file_exists($this->get['realpath'])) {
            unlink($this->get['realpath']);
            return [
                'Error' => "",
                'Code' => 0,
                'Path' => $this->get['realpath'],
            ];
        } else {
            $this->error($this->lang('INVALID_DIRECTORY_OR_FILE'));
        }
    }

    public function add(): array
    {
        $this->setParams();
        // phpcs:ignore
        if (!isset($_FILES['newfile']) || !is_uploaded_file($_FILES['newfile']['tmp_name'])) {
            $this->error($this->lang('INVALID_FILE_UPLOAD'), true);
        }
        if (($this->config['upload']['size'] && is_numeric($this->config['upload']['size'])) && ($_FILES['newfile']['size'] > ($this->config['upload']['size'] * 1024 * 1024))) {
            $this->error(sprintf($this->lang('UPLOAD_FILES_SMALLER_THAN'), $this->config['upload']['size'] . 'Mb'), true);
        }
        if ($this->config['upload']['imagesonly'] || (isset($this->params['type']) && $this->params['type'] == 'Images')) {
            // phpcs:ignore
            if (!($size = @getimagesize($_FILES['newfile']['tmp_name']))) {
                $this->error($this->lang('UPLOAD_IMAGES_ONLY'), true);
            }
            if (!in_array($size[2], [1, 2, 3, 7, 8])) {
                $this->error($this->lang('UPLOAD_IMAGES_TYPE_JPEG_GIF_PNG'), true);
            }
        }

        $_FILES['newfile']['name'] = $this->cleanString($_FILES['newfile']['name'], ['.', '-']);
        if (is_string($_FILES['newfile']['name'])) {
            if (!$this->config['upload']['overwrite']) {
                $_FILES['newfile']['name'] = $this->checkFilename($this->post['currentpath'], $_FILES['newfile']['name']);
            }
        }
        // phpcs:ignore
        move_uploaded_file($_FILES['newfile']['tmp_name'], $this->post['currentpath'] . $_FILES['newfile']['name']);

        return [
            'Path' => $this->post['currentpath'],
            'Name' => $_FILES['newfile']['name'],
            'Error' => "",
            'Code' => 0,
        ];
    }

    public function addfolder(): array
    {
        if (is_dir($this->get['path'] . $this->get['name'])) {
            $this->error(sprintf($this->lang('DIRECTORY_ALREADY_EXISTS'), $this->get['name']));
        }
        if (!mkdir($this->get['path'] . $this->get['name'], 0755)) {
            $this->error(sprintf($this->lang('UNABLE_TO_CREATE_DIRECTORY'), $this->get['name']));
        }
        return [
            'Parent' => $this->get['path'],
            'Name' => $this->get['name'],
            'Error' => "",
            'Code' => 0,
        ];
    }

    public function download(): void
    {
        if (isset($this->get['path']) && file_exists($this->get['path'])) {
            header("Content-type: application/force-download");
            header('Content-Disposition: inline; filename="' . $this->get['path'] . '"');
            header("Content-Transfer-Encoding: Binary");
            header("Content-length: " . filesize($this->get['path']));
            header('Content-Type: application/octet-stream');
            $tmp = explode('/', $this->get['path']);
            $filename = $tmp[(sizeof($tmp) - 1)];
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            readfile($this->get['path']);
        } else {
            $this->error(sprintf($this->lang('FILE_DOES_NOT_EXIST'), $this->get['path']));
        }
    }

    private function setParams(): void
    {
        $tmp = $_SERVER['HTTP_REFERER'] ?? '';
        $tmp = explode('?', $tmp);
        $params = [];
        if (isset($tmp[1]) && $tmp[1] != '') {
            $params_tmp = explode('&', $tmp[1]);
            foreach ($params_tmp as $value) {
                $tmp = explode('=', $value);
                if (isset($tmp[0]) && $tmp[0] != '' && isset($tmp[1]) && $tmp[1] != '') {
                    $params[$tmp[0]] = $tmp[1];
                }
            }
        }
        $this->params = $params;
    }

    private function getFileInfo(string $path = '', array $return = []): void
    {
        if ($path == '') {
            $path = $this->get['path'];
        }
        $tmp = explode('/', $path);
        unset($this->item['path']);
        $this->item['filename'] = $tmp[(sizeof($tmp) - 1)];
        /* Deal With OR Paths */
        $webpathstart = strpos($path, $_SESSION['filemanager_pathpart']);
        $webpathpart = is_int($webpathstart) ? substr($path, $webpathstart) : '';
        $this->item['realpath'] = $_SESSION['filemanager_basepath'] . '/' . $webpathpart;
        $realpath = $_SESSION['filemanager_basepath'] . '/' . $webpathpart;
        /* End of OR Path */
        $tmp = explode('.', $this->item['filename']);
        $this->item['filetype'] = $tmp[(sizeof($tmp) - 1)];
        $this->item['filemtime'] = filemtime($realpath);
        $this->item['filectime'] = filectime($realpath);
        $this->item['path'] = $_SESSION['filemanager_basepath'] . $webpathpart;
        $this->item['preview'] = $this->hostPrefixed($this->config['icons']['path'] . $this->config['icons']['default']); // @simo

        if (is_dir($path)) {
            $this->item['preview'] = $this->config['icons']['path'] . $this->config['icons']['directory'];
        } elseif (in_array($this->item['filetype'], $this->config['images'])) {
            //$this->item['preview'] = $this->hostPrefixed($path); // @simo
            //strip


            $this->item['preview'] = $_SESSION['filemanager_baseurl'] . '/' . $webpathpart;
            $this->item['path'] = $_SESSION['filemanager_basepath'] . '/' . $webpathpart;

            [$width, $height, $type, $attr] = getimagesize($path);
            $this->item['properties']['Height'] = $height;
            $this->item['properties']['Width'] = $width;
            $this->item['properties']['Size'] = filesize($realpath);
        } elseif (file_exists($this->root . $this->config['icons']['path'] . strtolower($this->item['filetype']) . '.png')) {
            $this->item['preview'] = $this->config['icons']['path'] . strtolower($this->item['filetype']) . '.png';
            $this->item['properties']['Size'] = filesize($realpath);
        }

        $this->item['properties']['Date Modified'] = date($this->config['date'], $this->item['filemtime']);
        //$return['properties']['Date Created'] = date($this->config['date'], $return['filectime']); // PHP cannot get create timestamp
    }

    /**
     * @return void
     */
    private function unlinkRecursive(string $dir, bool $deleteRootToo = true)
    {
        if (!$dh = @opendir($dir)) {
            return;
        }
        while (false !== ($obj = readdir($dh))) {
            if ($obj == '.' || $obj == '..') {
                continue;
            }

            if (!@unlink($dir . '/' . $obj)) {
                $this->unlinkRecursive($dir . '/' . $obj);
            }
        }

        closedir($dh);

        if ($deleteRootToo) {
            @rmdir($dir);
        }
    }

    /**
     * @return ($string is string ? string|null : string[]|null)
     */
    private function cleanString(string|array $string, array $allowed = []): array|string|null
    {
        $allow = null;
        if (!empty($allowed)) {
            foreach ($allowed as $value) {
                $allow .= "\\$value";
            }
        }
        $cleaned = null;
        if (is_array($string)) {
            $cleaned = [];
            foreach ($string as $key => $clean) {
                if (is_string($clean)) {
                    $cleaned[$key] = preg_replace("/[^{$allow}a-zA-Z0-9]/", '', $clean);
                }
            }
        } elseif (is_string($string)) {
            $cleaned = preg_replace("/[^{$allow}a-zA-Z0-9]/", '', $string);
        }
        return $cleaned;
    }

    private function checkFilename(string $path, string $filename, int $i = 0): string
    {
        if (!file_exists($path . $filename)) {
            return $filename;
        } else {
            $_i = $i;
            $tmp = explode(/*$this->config['upload']['suffix'] . */ $i . '.', $filename);
            if ($i == 0) {
                $i = 1;
            } else {
                $i++;
            }
            $filename = str_replace($_i . '.' . $tmp[(sizeof($tmp) - 1)], $i . '.' . $tmp[(sizeof($tmp) - 1)], $filename);
            return $this->checkFilename($path, $filename, $i);
        }
    }

    // @simo
    private function hostPrefixed(string $filepath): string
    {
        if (isset($this->config['add_host']) && $this->config['add_host']) {
            if (isset($_SERVER['HTTP_HOST'])) {
                return 'http://' . $_SERVER['HTTP_HOST'] . '/' . $filepath;
            }
            return '';
        } else {
            return $filepath; // @jason
        }
    }
}
