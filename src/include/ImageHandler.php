<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;

class ImageHandler extends MediaHandler
{
    /**
     * renderUserImages
     *
     * @param integer $userdb_id
     *
     * @return  string $display
     */
    public function renderUserImages(int $userdb_id): string
    {
        global $lang;

        $display = '';
        // grab the image data
        $media_api = $this->newMediaApi();
        try {
            $result = $media_api->read([
                'media_type' => 'userimages',
                'media_parent_id' => $userdb_id,
                'media_output' => 'URL',
            ]);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $num_images = $result['media_count'];

        if ($num_images > 0) {
            foreach ($result['media_object'] as $obj) {
                if (isset($obj['thumb_file_name'])) {
                    $thumb_file_name = $obj['thumb_file_name'];
                    $caption = $obj['caption'];
                    $imageID = $obj['media_id'];
                    // gotta grab the image size
                    $imagedata = GetImageSize($this->config['user_upload_path'] . "/$thumb_file_name");
                    if (is_array($imagedata)) {
                        $imagewidth = intval($imagedata[0]);
                        $imageheight = intval($imagedata[1]);
                        $max_width = $this->config['thumbnail_width'];
                        $max_height = $this->config['thumbnail_height'];
                        $resize_by = $this->config['resize_thumb_by'];
                        $displaywidth = $imagewidth;
                        $displayheight = $imageheight;
                        if (($max_width != $imagewidth) && ($max_height != $imageheight)) {
                            if ($resize_by == 'width') {
                                $shrinkage = $imagewidth / $max_width;
                                $displaywidth = $max_width;
                                $displayheight = round($imageheight / $shrinkage);
                            } elseif ($resize_by == 'height') {
                                $shrinkage = $imageheight / $max_height;
                                $displayheight = $max_height;
                                $displaywidth = round($imagewidth / $shrinkage);
                            } elseif ($resize_by == 'both') {
                                $displayheight = $max_height;
                                $displaywidth = $max_width;
                            } elseif ($resize_by == 'bestfit') {
                                $shrinkage_width = $imagewidth / $max_width;
                                $shrinkage_height = $imageheight / $max_height;
                                $shrinkage = max($shrinkage_width, $shrinkage_height);
                                $displayheight = round($imageheight / $shrinkage);
                                $displaywidth = round($imagewidth / $shrinkage);
                            }
                        }
                        $display .= "<a href=\"index.php?action=view_user_image&amp;image_id=$imageID\"> ";
                        $display .= "<img src=\"" . $this->config['user_view_images_path'] . "/$thumb_file_name\" height=\"$displayheight\" width=\"$displaywidth\"></a>";
                        $display .= "<div class=\"user_images_caption\">$caption</div>";
                    }
                }
            }
        } else {
            $display .= '<img src="' . $this->config['baseurl'] . '/images/nophoto.gif" alt="' . $lang['no_photo'] . '" /><br />';
        } // end ($num_images > 0)
        return $display;
    }

    /**
     * @param string $type
     * @return string
     */
    public function viewImage(string $type): string
    {
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $page = $this->newPageUser();
        $display = '';
        if (!isset($_GET['image_id'])) {
            return $lang['image_not_found'];
        }
        $sql_imageID = intval($_GET['image_id']);
        if ($type == 'listing') {
            // get the image data
            $sql = 'SELECT listingsimages_caption, listingsimages_file_name, listingsimages_description, listingsdb_id 
					FROM ' . $this->config['table_prefix'] . "listingsimages 
					WHERE (listingsimages_id = $sql_imageID)";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            while (!$recordSet->EOF) {
                $caption = $recordSet->fields('listingsimages_caption');
                $file_name = $recordSet->fields('listingsimages_file_name');
                $description = $recordSet->fields('listingsimages_description');
                $listing_id = $recordSet->fields('listingsdb_id');
                $display .= '<div class="view_image">';
                $display .= '<span class="image_caption">';
                if ($caption != '') {
                    $display .= "$caption - ";
                }
                //SEO Friendly Links
                $url = $page->magicURIGenerator('listing', $listing_id, true);
                $url = '<a href="' . $url . '">';
                $display .= $url . $lang['return_to_listing'] . '</a></span><br />';

                if (str_starts_with($file_name, 'http://') || str_starts_with($file_name, 'https://') || str_starts_with($file_name, '//')) {
                    $display .= '		<img src="' . htmlentities($file_name) . '" alt="' . $caption . '">';
                } else {
                    $display .= '		<img src="' . $this->config['listings_view_images_path'] . '/' . $file_name . '" alt="' . $caption . '">';
                }

                $display .= '<br />';
                $display .= $description;
                $display .= '</div>';
                $recordSet->MoveNext();
            }
        } elseif ($type == 'userimage') {
            $sql = 'SELECT userimages_caption, userimages_file_name, userimages_description, userdb_id 
					FROM ' . $this->config['table_prefix'] . "userimages 
					WHERE (userimages_id = $sql_imageID)";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            while (!$recordSet->EOF) {
                $caption = $recordSet->fields('userimages_caption');
                $file_name = $recordSet->fields('userimages_file_name');
                $description = $recordSet->fields('userimages_description');
                $user_id = $recordSet->fields('userdb_id');
                $display .= '<table class="form_" align="center">';
                $display .= '<tr>';
                $display .= '	<td class="row_main">';
                $display .= '		<h3>';
                if ($caption != '') {
                    $display .= "$caption - ";
                }
                $display .= '<a href="index.php?action=view_user&amp;user=' . $user_id . '">' . $lang['return_to_user'] . '</a></h3>';
                $display .= '		<center>';
                $display .= '		<img src="' . $this->config['user_view_images_path'] . '/' . $file_name . '" alt="' . $caption . '" border="1">';

                $display .= '		</center>';
                $display .= '		<br />';
                $display .= $description;
                $display .= '	</td>';
                $display .= '</tr>';
                $display .= '</table>';
                $recordSet->MoveNext();
            }
        }
        return $display;
    }

    public function ajaxDisplayListingImages(int $listing_id): string
    {
        global $ORconn;

        $status_text = '';
        $misc = $this->newMisc();
        $listing_pages = $this->newListingPages();

        $login = $this->newLogin();
        //Get Listing owner
        $listing_agent_id = $listing_pages->getListingAgentValue('userdb_id', $listing_id);
        //Make sure we can Edit this lisitng
        $has_permission = true;
        if ($_SESSION['userID'] != $listing_agent_id) {
            $security = $login->verifyPriv('edit_all_listings');
            if ($security !== true) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/listing_editor_image_display.html');
            //Load Listing Images
            $sql = 'SELECT listingsimages_id, listingsimages_caption, listingsimages_file_name, listingsimages_thumb_file_name 
					FROM ' . $this->config['table_prefix'] . "listingsimages 
					WHERE (listingsdb_id = $listing_id) 
					ORDER BY listingsimages_rank";
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $page->page = str_replace('{listing_id}', (string)$listing_id, $page->page);
            $html = $page->getTemplateSection('image_block');
            $new_html = '';
            $num_images = $recordSet->RecordCount();
            while (!$recordSet->EOF) {
                $new_html .= $html;
                $caption = $recordSet->fields('listingsimages_caption');
                $thumb_file_name = $recordSet->fields('listingsimages_thumb_file_name');
                $file_name = $recordSet->fields('listingsimages_file_name');
                $image_id = $recordSet->fields('listingsimages_id');
                // gotta grab the image size
                if (str_starts_with($thumb_file_name, 'http://') || str_starts_with($thumb_file_name, 'https://') || str_starts_with($thumb_file_name, '//')) {
                    $new_html = str_replace('{image_thumb_src}', htmlentities($file_name), $new_html);
                    $resize_by = $this->config['resize_thumb_by'];
                    if ($resize_by == 'width') {
                        $new_html = str_replace('{thumb_height}', '', $new_html);
                        $new_html = str_replace('{thumb_width}', (string)$this->config['thumbnail_width'], $new_html);
                    } elseif ($resize_by == 'height') {
                        $new_html = str_replace('{thumb_height}', (string)$this->config['thumbnail_height'], $new_html);
                        $new_html = str_replace('{thumb_width}', '', $new_html);
                    } else {
                        $new_html = str_replace('{thumb_height}', (string)$this->config['thumbnail_height'], $new_html);
                        $new_html = str_replace('{thumb_width}', (string)$this->config['thumbnail_width'], $new_html);
                    }

                    $new_html = str_replace('{image_caption}', htmlentities($caption, ENT_COMPAT, $this->config['charset']), $new_html);
                    $new_html = str_replace('{image_id}', $image_id, $new_html);
                } else {
                    if (file_exists($this->config['listings_upload_path'] . "/$thumb_file_name")) {
                        $thumb_imagedata = GetImageSize($this->config['listings_upload_path'] . "/$thumb_file_name");
                        $thumb_imagewidth = $thumb_imagedata[0];
                        $thumb_imageheight = $thumb_imagedata[1];
                        $thumb_max_width = $this->config['thumbnail_width'];
                        $thumb_max_height = $this->config['thumbnail_height'];
                        $resize_by = $this->config['resize_thumb_by'];
                        $thumb_displaywidth = $thumb_imagewidth;
                        $thumb_displayheight = $thumb_imageheight;

                        if ($resize_by == 'width' && $thumb_max_width != $thumb_imagewidth) {
                            $shrinkage = $thumb_imagewidth / $thumb_max_width;
                            $thumb_displaywidth = $thumb_max_width;
                            $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                        } elseif ($resize_by == 'height' && $thumb_max_height != $thumb_imageheight) {
                            $shrinkage = $thumb_imageheight / $thumb_max_height;
                            $thumb_displayheight = $thumb_max_height;
                            $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                        } elseif ($resize_by == 'both') {
                            $thumb_displayheight = $thumb_max_height;
                            $thumb_displaywidth = $thumb_max_width;
                        } elseif ($resize_by == 'bestfit') {
                            $shrinkage_width = $thumb_imagewidth / $thumb_max_width;
                            $shrinkage_height = $thumb_imageheight / $thumb_max_height;
                            $shrinkage = max($shrinkage_width, $shrinkage_height);
                            $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                            $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                        }
                        $new_html = str_replace('{image_thumb_src}', $this->config['listings_view_images_path'] . "/$thumb_file_name", $new_html);
                        $new_html = str_replace('{thumb_height}', (string)$thumb_displayheight, $new_html);
                        $new_html = str_replace('{thumb_width}', (string)$thumb_displaywidth, $new_html);
                        $new_html = str_replace('{image_caption}', htmlentities($caption, ENT_COMPAT, $this->config['charset']), $new_html);
                        $new_html = str_replace('{image_id}', (string)$image_id, $new_html);
                    } else {
                        $new_html = str_replace('{image_thumb_src}', $this->config['baseurl'] . "/images/nophoto.gif", $new_html);
                        $new_html = str_replace('{thumb_height}', '', $new_html);
                        $new_html = str_replace('{thumb_width}', '', $new_html);
                        $new_html = str_replace('{image_caption}', htmlentities($caption, ENT_COMPAT, $this->config['charset']), $new_html);
                        $new_html = str_replace('{image_id}', $image_id, $new_html);
                    }
                }
                $recordSet->MoveNext();
            } // end while
            $page->replaceTemplateSection('image_block', $new_html);
            $avaliable_images = $this->config['max_listings_uploads'] - $num_images;
            if ($avaliable_images > 0) {
                $page->page = $page->cleanupTemplateBlock('image_upload', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('image_upload', $page->page);
            }
            //End Listing Images
            //Finish Loading Template
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return 'Permission Denied';
        }
    }

    /**
     * ajax_display_user_images
     *
     * @param integer $userdb_id
     *
     * @return  string $page
     */
    public function ajaxDisplayUserImages(int $userdb_id): string
    {
        $login = $this->newLogin();
        $status_text = '';
        //Make sure we can Edit this lisitng
        $has_permission = true;
        if ($_SESSION['userID'] != $userdb_id) {
            $security = $login->verifyPriv('edit_all_users');
            if ($security !== true) {
                $has_permission = false;
            }
        }
        if ($has_permission) {
            $page = $this->newPageAdmin();
            $page->loadPage($this->config['admin_template_path'] . '/user_editor_image_display.html');

            // grab the image data
            $media_api = $this->newMediaApi();
            try {
                $result = $media_api->read([
                    'media_type' => 'userimages',
                    'media_parent_id' => $userdb_id,
                    'media_output' => 'URL',
                ]);
            } catch (Exception $e) {
                die($e->getMessage());
            }

            $num_images = $result['media_count'];
            $page->page = str_replace('{user_id}', (string)$userdb_id, $page->page);
            $html = $page->getTemplateSection('image_block');
            $new_html = '';

            foreach ($result['media_object'] as $obj) {
                if (isset($obj['thumb_file_name'])) {
                    $new_html .= $html;

                    $thumb_file_name = $obj['thumb_file_name'];
                    $caption = $obj['caption'];
                    $file_name = $obj['file_name'];
                    $image_id = $obj['media_id'];

                    // gotta grab the image size
                    if (str_starts_with($thumb_file_name, 'http://') || str_starts_with($thumb_file_name, 'https://') || str_starts_with($thumb_file_name, '//')) {
                        $new_html = str_replace('{image_thumb_src}', htmlentities($file_name), $new_html);
                        $resize_by = $this->config['user_resize_thumb_by'];
                        if ($resize_by == 'width') {
                            $new_html = str_replace('{thumb_height}', '', $new_html);
                            $new_html = str_replace('{thumb_width}', (string)$this->config['user_thumbnail_width'], $new_html);
                        } elseif ($resize_by == 'height') {
                            $new_html = str_replace('{thumb_height}', (string)$this->config['user_thumbnail_height'], $new_html);
                            $new_html = str_replace('{thumb_width}', '', $new_html);
                        } else {
                            $new_html = str_replace('{thumb_height}', (string)$this->config['user_thumbnail_height'], $new_html);
                            $new_html = str_replace('{thumb_width}', (string)$this->config['user_thumbnail_width'], $new_html);
                        }
                    } else {
                        if (file_exists($this->config['user_upload_path'] . "/$thumb_file_name")) {
                            $thumb_imagedata = GetImageSize($this->config['user_upload_path'] . "/$thumb_file_name");
                            $thumb_imagewidth = intval($thumb_imagedata[0]);
                            $thumb_imageheight = intval($thumb_imagedata[1]);
                            $thumb_max_width = intval($this->config['user_thumbnail_width']);
                            $thumb_max_height = intval($this->config['user_thumbnail_height']);
                            $resize_by = $this->config['user_resize_thumb_by'];
                            $thumb_displaywidth = $thumb_imagewidth;
                            $thumb_displayheight = $thumb_imageheight;
                            if ($resize_by == 'width') {
                                $shrinkage = $thumb_imagewidth / $thumb_max_width;
                                $thumb_displaywidth = $thumb_max_width;
                                $thumb_displayheight = (int)round($thumb_imageheight / $shrinkage);
                            } elseif ($resize_by == 'height') {
                                $shrinkage = $thumb_imageheight / $thumb_max_height;
                                $thumb_displayheight = $thumb_max_height;
                                $thumb_displaywidth = (int)round($thumb_imagewidth / $shrinkage);
                            } elseif ($resize_by == 'both') {
                                $thumb_displayheight = $thumb_max_height;
                                $thumb_displaywidth = $thumb_max_width;
                            } elseif ($resize_by == 'bestfit') {
                                $shrinkage_width = $thumb_imagewidth / $thumb_max_width;
                                $shrinkage_height = $thumb_imageheight / $thumb_max_height;
                                $shrinkage = max($shrinkage_width, $shrinkage_height);
                                $thumb_displayheight = (int)round($thumb_imageheight / $shrinkage);
                                $thumb_displaywidth = (int)round($thumb_imagewidth / $shrinkage);
                            }

                            $new_html = str_replace('{image_thumb_src}', $this->config['user_view_images_path'] . "/$thumb_file_name", $new_html);
                            $new_html = str_replace('{thumb_height}', (string)$thumb_displayheight, $new_html);
                            $new_html = str_replace('{thumb_width}', (string)$thumb_displaywidth, $new_html);
                        } else {
                            $new_html = str_replace('{image_thumb_src}', $this->config['baseurl'] . "/images/nophoto.gif", $new_html);
                            $new_html = str_replace('{thumb_height}', '', $new_html);
                            $new_html = str_replace('{thumb_width}', '', $new_html);
                        }
                    }
                    $new_html = str_replace('{image_caption}', htmlentities($caption, ENT_COMPAT, $this->config['charset']), $new_html);
                    $new_html = str_replace('{image_id}', (string)$image_id, $new_html);
                }
            } // end while

            $page->replaceTemplateSection('image_block', $new_html);
            $avaliable_images = $this->config['max_listings_uploads'] - $num_images;
            if ($avaliable_images > 0) {
                $page->page = $page->cleanupTemplateBlock('image_upload', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('image_upload', $page->page);
            }
            //End Listing Images
            //Finish Loading Template
            $page->replaceTag('application_status_text', $status_text);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            return $page->returnPage();
        } else {
            return 'Permission Denied';
        }
    }

    public function renderListingsMainImageSlideShow(int $listingID, string $template): string
    {
        // shows the images connected to a given image
        global $ORconn, $lang;

        $page = $this->newPageUser();
        $misc = $this->newMisc();

        $slidshow_thumb_group_template = $page->getTemplateSection('slideshow_thumbnail_group_block', $template);
        $slidshow_thumb_group_output = '';
        $slidshow_thumb_template = $page->getTemplateSection('slideshow_thumbnail_block', $slidshow_thumb_group_template);
        $slidshow_thumb_output = '';

        $sql = 'SELECT listingsimages_id,listingsimages_thumb_file_name,listingsimages_file_name,listingsimages_caption,listingsimages_description 
				FROM ' . $this->config['table_prefix'] . "listingsimages 
				WHERE (listingsdb_id = $listingID) 
				ORDER BY listingsimages_rank";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $num_images = $recordSet->RecordCount();
        //TODO Make Number of Thumbs per Group a Site Config Option.
        $group_max_thumbs = $this->config['listingimages_slideshow_group_thumb'];
        $thumb_count = 1;
        if ($num_images > 0) {
            while (!$recordSet->EOF) {
                $file_name = $recordSet->fields('listingsimages_file_name');
                $thumb_file_name = $recordSet->fields('listingsimages_thumb_file_name');
                $caption = $recordSet->fields('listingsimages_caption');
                $description = $recordSet->fields('listingsimages_description');
                if (str_starts_with($file_name, 'http://') || str_starts_with($file_name, 'https://') || str_starts_with($file_name, '//')) {
                    $src = $file_name;
                    $thumb_src = $thumb_file_name;
                } else {
                    //Make Sure File Exists.
                    if (!file_exists($this->config['listings_upload_path'] . '/' . $file_name)) {
                        $recordSet->MoveNext();
                        continue;
                    }
                    $src = $this->config['listings_view_images_path'] . '/' . $file_name;
                    $thumb_src = $this->config['listings_view_images_path'] . '/' . $thumb_file_name;
                }
                $slidshow_thumb_output .= $slidshow_thumb_template;
                $slidshow_thumb_output = str_replace('{slideshow_thumbnail}', $thumb_src, $slidshow_thumb_output);
                $slidshow_thumb_output = str_replace('{slideshow_width}', (string)$this->config['thumbnail_width'], $slidshow_thumb_output);
                $slidshow_thumb_output = str_replace('{slideshow_width_mainimage}', (string)$this->config['max_listings_upload_width'], $slidshow_thumb_output);
                $slidshow_thumb_output = str_replace('{slideshow_caption}', htmlentities($caption, ENT_COMPAT, $this->config['charset']), $slidshow_thumb_output);
                $slidshow_thumb_output = str_replace('{slideshow_description}', htmlentities($description, ENT_COMPAT, $this->config['charset']), $slidshow_thumb_output);
                $slidshow_thumb_output = str_replace('{slideshow_mainimage}', $src, $slidshow_thumb_output);
                $slidshow_thumb_output = str_replace('{slideshow_title}', $src, $slidshow_thumb_output);
                if ($thumb_count == $group_max_thumbs) {
                    //Complete this group
                    $slidshow_thumb_group_output .= $page->replaceTemplateSection('slideshow_thumbnail_block', $slidshow_thumb_output, $slidshow_thumb_group_template);
                    $slidshow_thumb_output = '';
                    $thumb_count = 1;
                } else {
                    $thumb_count++;
                }
                $recordSet->MoveNext();
            } // end while
            if ($slidshow_thumb_output != '') {
                $slidshow_thumb_group_output .= $page->replaceTemplateSection('slideshow_thumbnail_block', $slidshow_thumb_output, $slidshow_thumb_group_template);
            }
            $template = $page->replaceTemplateSection('slideshow_thumbnail_group_block', $slidshow_thumb_group_output, $template);
            $template = $page->cleanupTemplateBlock('slideshow_display', $template);
        } else {
            if ($this->config['show_no_photo']) {
                $template = str_replace('{slideshow_thumbnail}', $this->config['baseurl'] . '/images/nophoto.gif', $template);
                $template = str_replace('{slideshow_width}', (string)$this->config['thumbnail_width'], $template);
                $template = str_replace('{slideshow_width_mainimage}', (string)$this->config['max_listings_upload_width'], $template);
                $template = str_replace('{slideshow_alt}', htmlentities($lang['no_photo'], ENT_COMPAT, $this->config['charset']), $template);
                $template = str_replace('{slideshow_mainimage}', $this->config['baseurl'] . '/images/nophotobig.gif', $template);
                $template = str_replace('{slideshow_title}', $this->config['baseurl'] . '/images/nophotobig.gif', $template);
                $template = $page->cleanupTemplateBlock('slideshow_display', $template);
            } else {
                $template = $page->removeTemplateBlock('slideshow_display', $template);
            }
            $template = $page->cleanupTemplateBlock('slideshow_thumbnail', $template);
            $template = $page->cleanupTemplateBlock('slideshow_thumbnail_group', $template);
        }
        return $template;
    }

    public function renderListingsImages(int $listingID, string $showcap): string
    {
        // shows the images connected to a given image
        global $ORconn, $lang;

        $misc = $this->newMisc();
        $page = $this->newPageUser();
        // grab the images
        $sql = 'SELECT listingsimages_id, listingsimages_caption, listingsimages_thumb_file_name 
				FROM ' . $this->config['table_prefix'] . "listingsimages 
				WHERE (listingsdb_id = $listingID) 
				ORDER BY listingsimages_rank";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $display = '';
        $num_images = $recordSet->RecordCount();
        if ($num_images > 0) {
            while (!$recordSet->EOF) {
                $caption = $recordSet->fields('listingsimages_caption');
                $thumb_file_name = $recordSet->fields('listingsimages_thumb_file_name');
                // $file_name = $recordSet->fields('listingsimages_file_name');
                $imageID = $recordSet->fields('listingsimages_id');
                if (str_starts_with($thumb_file_name, 'http://') || str_starts_with($thumb_file_name, 'https://') || str_starts_with($thumb_file_name, '//')) {
                    $url = $page->magicURIGenerator('listing_image', $imageID, true);
                    $display .= '<a href="' . $url . '">';
                    if ($caption != '') {
                        $alt = $caption;
                    } else {
                        $alt = $thumb_file_name;
                    }
                    $display .= '<img src="' . htmlentities($thumb_file_name) . "\" height=\"" . $this->config['thumbnail_height'] . "\" width=\"" . $this->config['thumbnail_width'] . "\" alt=\"$alt\" /></a><br /> ";
                    if ($showcap == 'yes') {
                        $display .= '<strong>' . urldecode($caption) . '</strong><br /><br />';
                    } else {
                        $display .= '<br />';
                    }
                } elseif ($thumb_file_name != '' && file_exists($this->config['listings_upload_path'] . "/$thumb_file_name")) {
                    $thumb_imagedata = GetImageSize($this->config['listings_upload_path'] . "/$thumb_file_name");
                    $thumb_imagewidth = $thumb_imagedata[0];
                    $thumb_imageheight = $thumb_imagedata[1];
                    $thumb_max_width = $this->config['thumbnail_width'];
                    $thumb_max_height = $this->config['thumbnail_height'];
                    $resize_by = $this->config['resize_thumb_by'];


                    $thumb_displaywidth = $thumb_imagewidth;
                    $thumb_displayheight = $thumb_imageheight;

                    if ($resize_by == 'width') {
                        $shrinkage = $thumb_imagewidth / $thumb_max_width;
                        $thumb_displaywidth = $thumb_max_width;
                        $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                    } elseif ($resize_by == 'height') {
                        $shrinkage = $thumb_imageheight / $thumb_max_height;
                        $thumb_displayheight = $thumb_max_height;
                        $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                    } elseif ($resize_by == 'both') {
                        $thumb_displayheight = $thumb_max_height;
                        $thumb_displaywidth = $thumb_max_width;
                    } elseif ($resize_by == 'bestfit') {
                        $shrinkage_width = $thumb_imagewidth / $thumb_max_width;
                        $shrinkage_height = $thumb_imageheight / $thumb_max_height;
                        $shrinkage = max($shrinkage_width, $shrinkage_height);
                        $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                        $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                    }

                    $url = $page->magicURIGenerator('listing_image', $imageID, true);
                    $display .= '<a href="' . $url . '">';
                    if ($caption != '') {
                        $alt = $caption;
                    } else {
                        $alt = $thumb_file_name;
                    }
                    $display .= "<img src=\"" . $this->config['listings_view_images_path'] . "/$thumb_file_name\" height=\"$thumb_displayheight\" width=\"$thumb_displaywidth\" alt=\"$alt\" /></a><br /> ";
                    if ($showcap == 'yes') {
                        $display .= '<strong>' . urldecode($caption) . '</strong><br /><br />';
                    } else {
                        $display .= '<br />';
                    }
                } // end if ($thumb_file_name != "")
                $recordSet->MoveNext();
            }
        } else {
            if ($this->config['show_no_photo']) {
                $display .= "<img src=\"" . $this->config['baseurl'] . "/images/nophoto.gif\" width=\"" . $this->config['thumbnail_width'] . "\" alt=\"$lang[no_photo]\" /><br /> ";
            }
        }
        return $display;
    }

    public function renderListingsMainImage(int $listingID, string $showdesc, string $java): string
    {
        // shows the main image
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $display = '';
        // grab the images
        $sql = 'SELECT listingsimages_id, listingsimages_caption, listingsimages_file_name, listingsimages_description 
				FROM ' . $this->config['table_prefix'] . "listingsimages 
				WHERE (listingsdb_id = $listingID) 
				ORDER BY listingsimages_rank";
        $recordSet = $ORconn->execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $num_images = $recordSet->RecordCount();
        $first = false;
        $width = '';
        $height = '';
        if ($num_images > 0) {
            while (!$recordSet->EOF && !$first) {
                $file_name = $recordSet->fields('listingsimages_file_name');
                $caption = $recordSet->fields('listingsimages_caption');
                $description = $recordSet->fields('listingsimages_description');
                $display_method = $this->config['main_image_display_by'];
                $max_width = $this->config['main_image_width'];
                $max_height = $this->config['main_image_height'];
                $display_width = $max_width;
                $display_height = $max_height;

                if (str_starts_with($file_name, 'http://') || str_starts_with($file_name, 'https://') || str_starts_with($file_name, '//')) {
                    $img_src = htmlentities($file_name);
                    $width = $display_width;
                    $height = $display_height;
                } else {
                    if (!file_exists($this->config['listings_upload_path'] . '/' . $file_name)) {
                        $recordSet->MoveNext();
                        continue;
                    }
                    // Figure out display sizes based on display method
                    if ($display_method == 'width') {
                        $width = ' width="' . $max_width . '"';
                    } elseif ($display_method == 'height') {
                        $height = ' height="' . $max_height . '"';
                    } elseif ($display_method == 'both') {
                        $width = ' width="' . $max_width . '"';
                        $height = ' height="' . $max_height . '"';
                    }
                    $img_src = $this->config['listings_view_images_path'] . '/' . $file_name;
                    $first = true;
                }

                if ($java == 'yes') {
                    if ($showdesc == 'yes') {
                        $display = "<script type=\"text/javascript\"> function imgchange(id,caption,description){if(document.images){document.getElementById('main').src = id; document.getElementById('main').alt = caption; document.getElementById('main_image_description').innerHTML = description; } else { document.getElementById('main').src = \"images/nophoto.gif\";document.getElementById('main_image_description').innerHTML = ''; }}</script>";
                        $display .= "<img src=\"$img_src\" $width$height id=\"main\" alt=\"$caption\" /><br /><div id=\"main_image_description\">$description</div>";
                    } else {
                        $display = "<script type=\"text/javascript\"> function imgchange(id,caption,description){if(document.images){document.getElementById('main').src = id; document.getElementById('main').alt = caption;} else { document.getElementById('main').src = \"images/nophoto.gif\"; }}</script>";
                        $display .= "<img src=\"$img_src\" $width$height id=\"main\" alt=\"$caption\" /><br />";
                    }
                } else {
                    if ($showdesc == 'yes') {
                        $display .= "<img src=\"$img_src\" $width$height alt=\"$caption\" /><br /><div id=\"main_image_description\">$description</div>";
                    } else {
                        $display .= "<img src=\"$img_src\" $width$height alt=\"$caption\" /><br />";
                    }
                }

                $recordSet->MoveNext();
            } // end while
        } else {
            if ($this->config['show_no_photo']) {
                $display .= "<img src=\"" . $this->config['baseurl'] . "/images/nophotobig.gif\" width=\"$width\" id=\"main\" alt=\"$lang[no_photo]\" /><br />";
            }
        }
        return $display;
    }

    public function renderListingsImagesJava(int $listingID, string $showcap, string $mouseover = 'no'): string
    {
        // shows the images connected to a given image
        global $lang, $ORconn;

        $misc = $this->newMisc();
        // grab the images
        $sql = 'SELECT listingsimages_caption, listingsimages_file_name, listingsimages_thumb_file_name, listingsimages_description 
				FROM ' . $this->config['table_prefix'] . "listingsimages 
				WHERE (listingsdb_id = $listingID) 
				ORDER BY listingsimages_rank";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $display = '';
        $num_images = $recordSet->RecordCount();
        if ($num_images > 0) {
            // $display .= "<td width=\"$style[image_column_width]\" valign=\"top\" class=\"row_main\" align=\"center\">";
            while (!$recordSet->EOF) {
                $caption = $recordSet->fields('listingsimages_caption');
                $thumb_file_name = $recordSet->fields('listingsimages_thumb_file_name');
                $file_name = $recordSet->fields('listingsimages_file_name');
                // $imageID = $recordSet->fields('listingsimages_id');
                $description = $recordSet->fields('listingsimages_description');
                $description = htmlentities($description, ENT_COMPAT, $this->config['charset']);
                $caption = htmlentities($caption, ENT_COMPAT, $this->config['charset']);
                $thumb_max_width = $this->config['thumbnail_width'];
                $thumb_max_height = $this->config['thumbnail_height'];
                if (str_starts_with($thumb_file_name, 'http://') || str_starts_with($thumb_file_name, 'https://') || str_starts_with($thumb_file_name, '//')) {
                    $thumb_displayheight = $thumb_max_height;
                    $thumb_displaywidth = $thumb_max_width;
                    $thumb_src = $thumb_file_name;
                    $image_src = $file_name;
                } else {
                    if (!file_exists($this->config['listings_upload_path'] . '/' . $thumb_file_name)) {
                        $recordSet->MoveNext();
                        continue;
                    }
                    // gotta grab the image size
                    $thumb_imagedata = GetImageSize($this->config['listings_upload_path'] . "/$thumb_file_name");
                    $thumb_imagewidth = $thumb_imagedata[0];
                    $thumb_imageheight = $thumb_imagedata[1];
                    $thumb_src = $this->config['listings_view_images_path'] . '/' . $thumb_file_name;
                    $resize_by = $this->config['resize_thumb_by'];

                    $thumb_displaywidth = $thumb_imagewidth;
                    $thumb_displayheight = $thumb_imageheight;

                    if ($resize_by == 'width') {
                        $shrinkage = $thumb_imagewidth / $thumb_max_width;
                        $thumb_displaywidth = $thumb_max_width;
                        $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                    } elseif ($resize_by == 'height') {
                        $shrinkage = $thumb_imageheight / $thumb_max_height;
                        $thumb_displayheight = $thumb_max_height;
                        $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                    } elseif ($resize_by == 'both') {
                        $thumb_displayheight = $thumb_max_height;
                        $thumb_displaywidth = $thumb_max_width;
                    } elseif ($resize_by == 'bestfit') {
                        $shrinkage_width = $thumb_imagewidth / $thumb_max_width;
                        $shrinkage_height = $thumb_imageheight / $thumb_max_height;
                        $shrinkage = max($shrinkage_width, $shrinkage_height);
                        $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                        $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                    }

                    $image_src = $this->config['listings_view_images_path'] . '/' . $file_name;
                }
                if ($mouseover == 'no') {
                    $display .= "<a href=\"javascript:imgchange('$image_src'," . $misc->makeDbSafe($caption) . "," . $misc->makeDbSafe($description) . ")\"> ";
                    $display .= "<img src=\"$thumb_src\" height=\"$thumb_displayheight\" width=\"$thumb_displaywidth\" alt=\"$caption\" /></a><br />";
                } else {
                    $display .= '<img src="' . $thumb_src . '" height="' . $thumb_displayheight . '" width="' . $thumb_displaywidth . '" alt="' . $caption . '" onmouseover="imgchange(' . $misc->makeDbSafe($image_src) . ',' . $misc->makeDbSafe($caption) . ',' . $misc->makeDbSafe($description) . ')" /><br />';
                }
                if ($showcap == 'yes') {
                    $display .= '<strong>' . $caption . '</strong><br /><br />';
                } else {
                    $display .= '<br />';
                }
                $recordSet->MoveNext();
            }
        } else {
            if ($this->config['show_no_photo']) {
                $display .= "<img src=\"" . $this->config['baseurl'] . "/images/nophoto.gif\" width=\"" . $this->config['thumbnail_width'] . "\" alt=\"$lang[no_photo]\" /><br /> ";
            }
        }
        return $display;
    }

    public function renderListingsImagesJavaRows(int $listingID, string $mouseover = 'no'): string
    {
        // shows the images connected to a given image
        global $lang, $ORconn;

        $misc = $this->newMisc();
        // grab the images
        $var_reset = 1; // Reset the var (counter) (DO NOT CHANGE)
        $user_col_max = $this->config['number_columns']; // How Many To show Per Row
        $sql = 'SELECT listingsimages_caption, listingsimages_file_name, listingsimages_thumb_file_name, listingsimages_description 
				FROM ' . $this->config['table_prefix'] . "listingsimages 
				WHERE (listingsdb_id = $listingID) 
				ORDER BY listingsimages_rank";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $display = '';
        $num_images = $recordSet->RecordCount();
        if ($num_images > 0) {
            $display .= '<table id="imagerows">';

            while (!$recordSet->EOF) {
                $caption = $recordSet->fields('listingsimages_caption');
                $thumb_file_name = $recordSet->fields('listingsimages_thumb_file_name');
                $file_name = $recordSet->fields('listingsimages_file_name');
                $description = $recordSet->fields('listingsimages_description');
                $description = htmlentities($description, ENT_COMPAT, $this->config['charset']);
                $caption = htmlentities($caption, ENT_COMPAT, $this->config['charset']);

                $thumb_max_width = $this->config['thumbnail_width'];
                $thumb_max_height = $this->config['thumbnail_height'];
                if (str_starts_with($thumb_file_name, 'http://') || str_starts_with($thumb_file_name, 'https://') || str_starts_with($thumb_file_name, '//')) {
                    $thumb_displayheight = $thumb_max_height;
                    $thumb_displaywidth = $thumb_max_width;
                    $thumb_src = $thumb_file_name;
                    $image_src = $file_name;
                } else {
                    // gotta grab the image size
                    $thumb_imagedata = GetImageSize($this->config['listings_upload_path'] . "/$thumb_file_name");
                    $thumb_imagewidth = $thumb_imagedata[0];
                    $thumb_imageheight = $thumb_imagedata[1];
                    $thumb_src = $this->config['listings_view_images_path'] . '/' . $thumb_file_name;
                    $resize_by = $this->config['resize_thumb_by'];

                    $thumb_displaywidth = $thumb_imagewidth;
                    $thumb_displayheight = $thumb_imageheight;

                    if ($resize_by == 'width') {
                        $shrinkage = $thumb_imagewidth / $thumb_max_width;
                        $thumb_displaywidth = $thumb_max_width;
                        $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                    } elseif ($resize_by == 'height') {
                        $shrinkage = $thumb_imageheight / $thumb_max_height;
                        $thumb_displayheight = $thumb_max_height;
                        $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                    } elseif ($resize_by == 'both') {
                        $thumb_displayheight = $thumb_max_height;
                        $thumb_displaywidth = $thumb_max_width;
                    } elseif ($resize_by == 'bestfit') {
                        $shrinkage_width = $thumb_imagewidth / $thumb_max_width;
                        $shrinkage_height = $thumb_imageheight / $thumb_max_height;
                        $shrinkage = max($shrinkage_width, $shrinkage_height);
                        $thumb_displayheight = round($thumb_imageheight / $shrinkage);
                        $thumb_displaywidth = round($thumb_imagewidth / $shrinkage);
                    }

                    $image_src = $this->config['listings_view_images_path'] . '/' . $file_name;
                }
                if ($var_reset == 1) {
                    $display .= '<tr>';
                }
                if ($caption == '') {
                    $caption = $thumb_file_name;
                }
                if ($mouseover == 'no') {
                    $display .= "<td><a href=\"javascript:imgchange('$image_src'," . $misc->makeDbSafe($caption) . "," . $misc->makeDbSafe($description) . ")\"> ";
                    $display .= "<img src=\"$thumb_src\" height=\"$thumb_displayheight\" width=\"$thumb_displaywidth\" alt=\"$caption\" /></a>";
                    $display .= '</td>';
                } else {
                    $display .= '<td><img src="' . $thumb_src . '" height="' . $thumb_displayheight . '" width="' . $thumb_displaywidth . '" alt="' . $caption . '" onmouseover="imgchange(\'' . $image_src . '\',' . $misc->makeDbSafe($caption) . ',' . $misc->makeDbSafe($description) . ')" /></td>';
                }
                if ($var_reset == $user_col_max) {
                    $display .= '</tr>';
                    $var_reset = 1;
                } else {
                    $var_reset++;
                }
                $recordSet->MoveNext();
            } // end while
            if ($var_reset != 1) {
                $display .= '</tr>';
            }
            $display .= '</table>';
        } else {
            if ($this->config['show_no_photo']) {
                $display .= '<table id="imagerows">';
                $display .= "<tr><td><img src=\"" . $this->config['baseurl'] . "/images/nophoto.gif\" width=\"" . $this->config['thumbnail_width'] . "\" alt=\"" . $lang['no_photo'] . "\" /></td></tr></table>";
            }
        }
        return $display;
    }
}
