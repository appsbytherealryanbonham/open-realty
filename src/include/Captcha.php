<?php

declare(strict_types=1);

namespace OpenRealty;

use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;

/**
 * captcha
 * This class is abstraction layer class for recaptcha and secureimage
 */
class Captcha extends BaseClass
{
    /**
     * @return string
     */
    public function show(): string
    {
        global $ORconn, $jscript;
        $misc = $this->newMisc();
        $display = '';
        if ($this->config['captcha_system'] == 'securimage') {
            $builder = new CaptchaBuilder();
            $builder->build();
            $_SESSION['phrase'] = trim($builder->getPhrase());
            $display .= '<div class="captcha_outer"><div class="captcha_inner"><img src="' . $builder->inline() . '" /></div><input type="input" name="captcha_code" /></div>';
        } else {
            // new v2 reCaptcha
            // add following var to api url to force language,  it is supposed to auto-detect. ?hl='.$this->config["lang"].'
            $jscript .= '<script src="https://www.google.com/recaptcha/api.js" async defer></script>';

            $sql = 'SELECT controlpanel_recaptcha_sitekey 
				FROM ' . $this->config['table_prefix_no_lang'] . 'controlpanel';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $sitekey = $recordSet->fields('controlpanel_recaptcha_sitekey');

            $display .= '<div class="captcha_outer">
						     <div class="g-recaptcha" data-sitekey="' . $sitekey . '"></div>
						</div>';
        }

        return $display;
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        global $ORconn;

        $misc = $this->newMisc();
        $correct_code = false;

        if ($this->config['captcha_system'] == 'securimage') {
            if (isset($_POST['captcha_code']) && is_string($_POST['captcha_code']) && isset($_SESSION['phrase'])) {
                if (PhraseBuilder::comparePhrases(strtolower($_SESSION['phrase']), strtolower($_POST['captcha_code']))) {
                    $correct_code = true;
                }
            }
        } else {
            //V2 reCaptcha
            if (isset($_POST['g-recaptcha-response'])) {
                $sql = 'SELECT controlpanel_recaptcha_secretkey 
								FROM ' . $this->config['table_prefix_no_lang'] . 'controlpanel';
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $secretkey = $recordSet->fields('controlpanel_recaptcha_secretkey');

                $data = [
                    'secret' => $secretkey,
                    'response' => $_POST['g-recaptcha-response'],
                    'remoteip' => $_SERVER['REMOTE_ADDR'] ?? '',
                ];

                $curl = curl_init();

                curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_VERBOSE, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_MAXREDIRS, 6);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($curl, CURLOPT_TIMEOUT, 60);

                $response = curl_exec($curl);
                if (is_bool($response)) {
                    return false;
                }
                $captcha_success = json_decode($response);

                curl_close($curl);
                if ($captcha_success->success) {
                    $correct_code = true;
                }
            }
        }
        return $correct_code;
    }
}
