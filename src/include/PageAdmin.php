<?php

declare(strict_types=1);

namespace OpenRealty;

use OpenRealty\Api\Commands\ListingApi;
use Exception;

class PageAdmin extends Page
{
    public function replaceTags(array $tags = []): void
    {
        global $lang, $jscript_last;
        $data = '';
        $misc = $this->newMisc();
        $listing_api = $this->newListingApi();

        if (count($tags) > 0) {
            // Remove tags not found in teh template
            $new_tags = $tags;
            $tags = [];
            foreach ($new_tags as $tag) {
                if (str_contains($this->page, '{' . $tag . '}')) {
                    $tags[] = $tag;
                }
            }
            unset($new_tags);
            foreach ($tags as $tag) {
                $data = '';
                switch ($tag) {
                    case 'csrf_token':
                        $data = $misc->generateCsrfToken();
                        break;
                    case 'select_language':
                        // $multilingual = new Multilingual();
                        // $data = $multilingual->multilingualSelect();
                        break;
                    case 'version':
                        $data = $lang['version'] . ' ' . $this->config['version'];
                        break;
                    case 'company_name':
                        $data = $this->config['company_name'];
                        break;
                    case 'company_location':
                        $data = $this->config['company_location'];
                        break;
                    case 'company_logo':
                        $data = $this->config['company_logo'];
                        break;
                    case 'site_title':
                        $data = $this->config['seo_default_title'];
                        break;
                    case 'lang_index_home':
                        $data = $lang['index_home'];
                        break;
                    case 'lang_index_admin':
                        $data = $lang['index_admin'];
                        break;
                    case 'lang_index_logout':
                        $data = $lang['index_logout'];
                        break;
                    case 'baseurl':
                        $data = $this->config['baseurl'];
                        break;
                    case 'general_info':
                        $admin = $this->newGeneralAdmin();
                        ;
                        $data = $admin->generalInfo();
                        break;
                    case 'openrealty_links':
                        $admin = $this->newGeneralAdmin();
                        ;
                        $data = $admin->openrealtyLinks();
                        break;
                    case 'addon_links':
                        // Show Addons
                        $addons = $this->loadAddons();

                        $admin = $this->newGeneralAdmin();

                        $addon_links = [];
                        //print_r($addons);
                        foreach ($addons as $addon) {
                            //echo 'Loading '.$addon;
                            $addon_link = [];
                            $addon_link = $admin->displayAddons($addon);
                            foreach ($addon_link as $link) {
                                if (trim($link) !== '') {
                                    $addon_links[] = $link;
                                }
                            }
                        }
                        $current_link = 0;
                        $cell_count = 0;
                        $link_count = count($addon_links);
                        while ($current_link < $link_count) {
                            if ($addon_links[$current_link]) {
                                $data .= '<div class="col-xl-2 col-sm-3 mb-4"><div class="card h-100"><div class="card-footer h-100 p-3">
                                <p class="align-bottom">' . $addon_links[$current_link] . '</p></div></div></div>';
                                $cell_count++;
                            }
                            $current_link++;
                        } // while
                        //$data .= '<div class="clear"></div>';
                        break;
                    case 'addon_menu_links':
                        // Show Addons
                        $addons = $this->loadAddons();

                        $admin = $this->newGeneralAdmin();
                        ;
                        $addon_links = [];
                        //print_r($addons);
                        foreach ($addons as $addon) {
                            //echo 'Loading '.$addon;
                            $addon_link = [];
                            $addon_link = $admin->displayAddons($addon);
                            foreach ($addon_link as $link) {
                                if (trim($link) !== '') {
                                    $addon_links[] = $link;
                                }
                            }
                        }
                        $current_link = 0;
                        $cell_count = 0;
                        $link_count = count($addon_links);

                        if ($link_count > 0) {
                            $data .= '';

                            //$data .= '<div id="addon_links">';
                            while ($current_link < $link_count) {
                                if ($addon_links[$current_link]) {
                                    $data .= $addon_links[$current_link];
                                    $cell_count++;
                                }
                                $current_link++;
                            } // while
                            //$data .= "</div>";
                        }
                        break;

                    case 'lang':
                        if (isset($_SESSION['users_lang']) && $_SESSION['users_lang'] != $this->config['lang']) {
                            $data = $_SESSION['users_lang'];
                        } else {
                            $data = $this->config['lang'];
                        }
                        break;
                    case 'user_id':
                        $data = strval($_SESSION['userID'] ?? 0);
                        break;
                    case 'template_url':
                        $data = $this->config['admin_template_url'];
                        break;
                    case 'load_js':
                        $data = $this->loadJS(true);
                        break;
                    case 'load_ORjs':
                        $data = $this->loadORJS(true);
                        break;
                    case 'load_js_last':
                        $data = is_string($jscript_last) ? $jscript_last : '';
                        break;
                    case 'content':
                        $data = $this->replaceAdminActions();
                        break;
                    case 'charset':
                        $data = $this->config['charset'];
                        break;

                    case 'curley_open':
                        $data = '{';
                        break;
                    case 'curley_close':
                        $data = '}';
                        break;
                    case 'powered_by_tag':
                        $data = '<a href="https://gitlab.com/appsbytherealryanbonham/open-realty" title="Powered By Open-Realty"><img id="or_poweredby_logo" style="display:block;width:110px;height:29px; clip: auto; clip-path: none; z-index: auto; transform: none;" src="' . $this->config['baseurl'] . '/index.php?action=powered_by" alt="Powered By Open-Realty®" /></a>';
                        break;
                    case (preg_match('/getvar_([^{}]*?)/', $tag) ? $tag : false):
                        $getvar = str_replace('getvar_', '', $tag);
                        if (isset($_GET[$getvar]) && is_scalar($_GET[$getvar]) && !empty($_GET[$getvar])) {
                            $data = htmlentities($_GET[$getvar]);
                        }
                        break;
                    case 'current_language':
                        $data = $_SESSION['language_template'] ?? $this->config['lang'];
                        break;
                    case 'active_listing_count':
                        try {
                            $result = $listing_api->search(['parameters' => [], 'limit' => 0, 'offset' => 0, 'count_only' => true]);
                            $data = (string)$result['listing_count'];
                        } catch (Exception) {
                        }

                        break;
                    case (preg_match('/^active_listing_count_pclass_([0-9]*)/', $tag, $pclass) ? $tag : !$tag):
                        try {
                            $result = $listing_api->search(['parameters' => ['pclass' => [(int)$pclass[1]]], 'limit' => 0, 'offset' => 0, 'count_only' => true]);
                            $data = (string)$result['listing_count'];
                        } catch (Exception) {
                        }

                        break;
                    case (preg_match('/^listing_stat_([a-z]*)_field_([^{}]*?)_value_pclass_([0-9]*)/', $tag, $args) ? $tag : !$tag):
                        try {
                            $result = $listing_api->getStatistics(['function' => $args[1], 'field_name' => $args[2], 'pclass' => [(int)$args[3]], 'format' => true]);
                        } catch (Exception) {
                        }
                        if (isset($result[$args[1]])) {
                            $data = (string)$result[$args[1]];
                        }
                        break;
                    case (preg_match('/^listing_stat_([a-z]*)_field_([^{}]*?)_value/', $tag, $args) ? $tag : !$tag):
                        try {
                            $result = $listing_api->getStatistics(['function' => $args[1], 'field_name' => $args[2], 'format' => true]);
                        } catch (Exception) {
                        }
                        if (isset($result[$args[1]])) {
                            $data = (string)$result[$args[1]];
                        }
                        break;
                    default:
                        if (preg_match('/^addon_(.*?)_.*/', $tag, $addon_name)) {
                            $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name[1] . '\\Addon';
                            if (class_exists($addonClassName)) {
                                $addonClass = new $addonClassName($this->dbh, $this->config);
                                $data = $addonClass->runTemplateUserFields($tag);
                            }
                        }
                        break;
                }
                $this->page = str_replace('{' . $tag . '}', $data, $this->page);
            }
        }
    }

    public function replaceAdminActions(): string
    {
        $login = $this->newLogin();
        $login_status = $login->loginCheck('Agent');
        $data = '';
        if ($login_status !== true) {
            // Run theese commands even if not logged in.

            switch ($_GET['action']) {
                case 'send_forgot':
                    $data = $login->forgotPassword();
                    break;
                case 'forgot':
                    $data = $login->forgotPasswordReset();
                    break;
                default:
                    $data .= $login_status;
                    break;
            }
        } else {
            switch ($_GET['action']) {
                case 'index':
                    $admin = $this->newGeneralAdmin();
                    ;
                    $data = $admin->indexPage();
                    break;
                case 'edit_page':
                    $listing = $this->newPageEditor();
                    ;
                    $data = $listing->pageEditIndex();
                    break;
                case 'edit_page_post':
                    $listing = $this->newPageEditor();
                    ;
                    $data = $listing->pageEdit();
                    break;
                case 'edit_my_listings':
                    $listing_editor = $this->newListingEditor();
                    $data = $listing_editor->editListings();
                    break;
                case 'edit_listings':
                    $listing_editor = $this->newListingEditor();
                    $data = $listing_editor->editListings(false);
                    break;
                case 'configure':
                    $listing_editor = $this->newConfigurator();
                    $data = $listing_editor->showConfigurator();
                    break;
                case 'edit_listing_template':
                    $listing = $this->newTemplateEditor();
                    $data = $listing->editListingTemplate();
                    break;
                case 'edit_agent_template_add_field':
                    $listing = $this->newTemplateEditor();
                    $data = $listing->addUserTemplateField('agent');
                    break;
                case 'edit_member_template_add_field':
                    $listing = $this->newTemplateEditor();
                    $data = $listing->addUserTemplateField('member');
                    break;
                case 'user_manager':
                    $user_managment = $this->newUserManagement();
                    $data = $user_managment->showUserManager();
                    break;
                case 'edit_user':
                    $user_managment = $this->newUserManagement();
                    $data = $user_managment->showEditUser();
                    break;
                case 'edit_agent_template':
                    $_GET['type'] = "agent";

                    $fields = $this->newTemplateEditor();
                    $data = $fields->editUserTemplate($_GET['type']);
                    break;
                case 'edit_member_template':
                    $_GET['type'] = "member";

                    $fields = $this->newTemplateEditor();
                    $data = $fields->editUserTemplate($_GET['type']);
                    break;
                case 'edit_listing_template_add_field':
                    $listing = $this->newTemplateEditor();
                    $data = $listing->addListingTemplateField();
                    break;
                case 'view_log':
                    $log = $this->newLog();
                    $data = $log->view();
                    break;
                case 'clear_log':
                    $log = $this->newLog();
                    $data = $log->clearLog();
                    break;
                case 'show_property_classes':
                    $propertyclass = $this->newPropertyClass();
                    if (isset($_GET['statustext']) && is_string($_GET['statustext'])) {
                        $statustext = strip_tags($_GET['statustext']);
                        $data = $propertyclass->showClasses($statustext);
                    } else {
                        $data = $propertyclass->showClasses();
                    }


                    break;
                case 'delete_property_class':
                    $propertyclass = $this->newPropertyClass();
                    $data = $propertyclass->deletePropertyClass();
                    break;
                case 'insert_property_class':
                    $propertyclass = $this->newPropertyClass();
                    $data = $propertyclass->insertPropertyClass();
                    break;
                case 'edit_blog':
                    $listing = $this->newBlogEditor();
                    $data = $listing->blogEditIndex();
                    break;
                case 'edit_blog_post':
                    $listing = $this->newBlogEditor();
                    $data = $listing->blogEdit();
                    break;
                case 'edit_blog_post_comments':
                    $listing = $this->newBlogEditor();
                    $data = $listing->editPostComments();
                    break;
                case 'addon_manager':
                    $am = $this->newAddonManager();
                    $data = $am->displayAddonManager();
                    break;
                case 'send_notifications':
                    $notify = $this->newNotification();
                    $data = $notify->notifyUsersOfAllNewListings();
                    break;
                case 'edit_listing':
                    $listing_editor = $this->newListingEditor();
                    if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {
                        $data = $listing_editor->displayListingEditor((int)$_GET['edit']);
                    }
                    break;
                case 'leadmanager':
                    $lead_manager = $this->newLeadManager();
                    $data = $lead_manager->showLeads(true);
                    break;
                case 'my_leadmanager':
                    $lead_manager = $this->newLeadManager();
                    $data = $lead_manager->showLeads();
                    break;
                case 'leadmanager_feedback_edit':
                    $lead_manager = $this->newLeadManager();
                    if (isset($_GET['feedback_id']) && is_numeric($_GET['feedback_id'])) {
                        $data = $lead_manager->showFeedbackEdit((int)$_GET['feedback_id'], true);
                    }
                    break;
                case 'leadmanager_my_feedback_edit':
                    $lead_manager = $this->newLeadManager();
                    if (isset($_GET['feedback_id']) && is_numeric($_GET['feedback_id'])) {
                        $data = $lead_manager->showFeedbackEdit((int)$_GET['feedback_id']);
                    }
                    break;
                case 'leadmanager_form_edit':
                    $lead_manager = $this->newLeadManager();
                    $data = $lead_manager->formEdit();
                    break;
                case 'leadmanager_viewfeedback':
                    $lead_manager = $this->newLeadManager();
                    $data = $lead_manager->feedbackView(true);
                    break;
                case 'leadmanager_my_viewfeedback':
                    $lead_manager = $this->newLeadManager();
                    $data = $lead_manager->feedbackView();
                    break;
                case 'view_statistics':
                    $tracking = $this->newTracking();
                    $data = $tracking->viewStatistics();
                    break;
                case 'clear_statistics_log':
                    $tracking = $this->newTracking();
                    $data = $tracking->clearStatisticsLog();
                    break;
                case 'generate_sitemap':
                    $sitemap = $this->newSitemap();
                    $data = $sitemap->generate();
                    break;
                case 'twitterback':
                    $social = $this->newSocial();
                    $data = $social->twitterCallback();
                    break;
                case 'leadmanager_add_lead':
                    $lead_manager = $this->newLeadManager();
                    $data = $lead_manager->showAddLead();
                    break;
                case 'edit_menu':
                    $menu_editor = $this->newMenuEditor();
                    $data = $menu_editor->showEditor();
                    break;
                default:
                    // Handle Addons
                    $addon_name = [];
                    if (is_string($_GET['action'])) {
                        if (preg_match('/^addon_(.\S*?)_.*/', $_GET['action'], $addon_name)) {
                            $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name[1] . '\\Addon';
                            if (class_exists($addonClassName)) {
                                $addonClass = new $addonClassName($this->dbh, $this->config);
                                $data = $addonClass->runActionAdminTemplate();
                            }
                        }
                    }
            }
        }
        return $data;
    }
}
