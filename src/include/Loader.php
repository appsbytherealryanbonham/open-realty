<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;
use OpenRealty\Api\Commands\SettingsApi;
use OpenRealty\Api\Database;
use PDO;
use PhpXmlRpc\Server;

class Loader
{
    private float $start_time;

    private float $end_time = 0;

    private array $config;
    private PDO $dbh;

    private bool $isMobile;

    private bool $recordTracking = false;

    public function __construct()
    {
        $this->setStartTime();
        $this->loadCodeception();
        $this->supportCLICommands();
        $this->setPHPValues();
        $this->isMobile = $this->detectIsMobile();
        $this->loadDBH();
        $this->loadSettings();
        $this->loadCommonFile();
        $this->setSessionCookie();
        $this->loadUserSelectedTemplate();
        $this->setLegacyGlobalConfig();
        $this->loadUserSelectedLanguage();
        ob_start();
        $this->determineLoadType();
        $this->flushAndExit();
    }

    private function setStartTime(): void
    {
        $this->start_time = $this->getMicroTime();
    }

    private function getMicroTime(): float
    {
        [$usec, $sec] = explode(' ', microtime());
        return ((float)$usec + (float)$sec);
    }

    private function loadCodeception(): void
    {
        $c3File = dirname(__FILE__, 2) . '/c3.php';
        if (file_exists($c3File)) {
            /** @psalm-suppress MissingFile */
            include_once $c3File;
        }
    }

    private function supportCLICommands(): void
    {
        global $argv;
        //Support CLI Commands
        if (php_sapi_name() == 'cli') {
            parse_str($argv[1], $_POST);
            parse_str($argv[2], $_GET);
        }
    }

    private function setPHPValues(): void
    {
        if (file_exists(dirname(__FILE__, 2) . '/devignoreinstall')) {
            //Set Session INI to force garbage collection to run 100% of the time.
            @ini_set('session.gc_probability', '100'); // phpcs:ignore
            @ini_set('session.gc_divisor', '100'); // phpcs:ignore
            error_reporting(E_ALL);
        } else {
            error_reporting(E_ALL ^ E_NOTICE);
        }

        // This Fixes XHTML Validation issues, with PHP
        @ini_set('pcre.backtrack_limit', '10000000'); // phpcs:ignore
        // Set Float Percession, neded for lat/long work
        @ini_set('precision', '14'); // phpcs:ignore
    }

    private function detectIsMobile(): bool
    {
        //echo $_SERVER['HTTP_USER_AGENT'];
        $regex_match = '/(nokia|iphone|android(?!(.*xoom\sbuild))|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|';
        $regex_match .= 'htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|';
        $regex_match .= 'blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|';
        $regex_match .= 'symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|';
        $regex_match .= 'jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220|skyfire';
        $regex_match .= ')/i';
        if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        } elseif (isset($_SERVER['HTTP_PROFILE'])) {
            return true;
        } elseif (isset($_SERVER['HTTP_USER_AGENT'])) {
            return (bool)preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT']));
        } else {
            return false;
        }
    }

    private function loadDBH(): void
    {
        // Load PDO Connection
        $apiDB = new Database();
        $this->dbh = $apiDB->getDatabaseHandler();
    }

    private function loadSettings(): void
    {
        //Load Settings
        $settingApi = new SettingsApi($this->dbh);
        $this->config = $settingApi->read(['is_mobile' => $this->isMobile])['config'];
        $config = $this->config;
    }

    private function setSessionCookie(): void
    {
        session_start([
            "cookie_httponly" => true,
            "cookie_samesite" => "Lax",
            "cookie_secure" => str_contains($this->config["baseurl"], "https://"),
            "use_strict_mode" => true,
            "sid_bits_per_character" => 6,
        ]);
    }

    private function loadCommonFile(): void
    {
        $commonFile = dirname(__FILE__) . '/common.php';
        if (file_exists($commonFile)) {
            /** @psalm-suppress MissingFile */
            require_once $commonFile;
        } else {
            die('common.php missing');
        }
    }

    private function loadUserSelectedTemplate(): void
    {
        $misc = new Misc($this->dbh, $this->config);
        if ($this->config['allow_template_change']) {
            // Check for User Selected Template
            if (isset($_POST['select_users_template']) && is_string($_POST['select_users_template'])) {
                if (!isset($_POST['token']) || !is_string($_POST['token']) || !$misc->validateCsrfToken($_POST['token'])) {
                    //File name contains non alphanum chars die to prevent file system attacks.
                    http_response_code(500);
                    echo 'CSRF token validation failed.';
                    die;
                }
                if (preg_match('/[^A-Za-z0-9_]/', $_POST['select_users_template'])) {
                    http_response_code(500);
                    //File name contains non alphanum chars die to prevent file system attacks.
                    die;
                }
                $_SESSION['template'] = $_POST['select_users_template'];
                $this->config['template'] = $_POST['select_users_template'];
                $this->config['template_path'] = $this->config['basepath'] . '/template/' . $this->config['template']; // leave off the trailing slashes
                $this->config['template_url'] = $this->config['baseurl'] . '/template/' . $this->config['template']; // leave off the trailing slashes
                unset($_POST['select_users_template']);
            }
            // Multilingual Add-on
            if (isset($_POST['selected_language_template']) && is_string($_POST['selected_language_template'])) {
                if (preg_match('/[^A-Za-z0-9_]/', $_POST['selected_language_template'])) {
                    //File name contains non alphanum chars die to prevent file system attacks.
                    die;
                }
                $_SESSION['language_template'] = $_POST['selected_language_template'];
                $_SESSION['template'] = $_POST['selected_language_template'];
                $this->config['template'] = $_SESSION['template'];
                $this->config['template_path'] = $this->config['basepath'] . '/template/' . $this->config['template']; // leave off the trailing slashes
                $this->config['template_url'] = $this->config['baseurl'] . '/template/' . $this->config['template']; // leave off the trailing slashes
            } elseif (isset($_GET['selected_language_template']) && is_string($_GET['selected_language_template'])) {
                if (preg_match('/[^A-Za-z0-9_]/', $_GET['selected_language_template'])) {
                    //File name contains non alphanum chars die to prevent file system attacks.
                    die;
                }
                $_SESSION['language_template'] = $_GET['selected_language_template'];
                $_SESSION['template'] = $_GET['selected_language_template'];
                $this->config['template'] = $_SESSION['template'];
                $this->config['template_path'] = $this->config['basepath'] . '/template/' . $this->config['template']; // leave off the trailing slashes
                $this->config['template_url'] = $this->config['baseurl'] . '/template/' . $this->config['template']; // leave off the trailing slashes
            }
        }
    }

    private function setLegacyGlobalConfig(): void
    {
        $config = $this->config;
    }

    private function loadUserSelectedLanguage(): void
    {
        if ($this->config['allow_language_change']) {
            if (isset($_POST['select_users_lang']) && is_string($_POST['select_users_lang'])) {
                if (preg_match('/[^A-Za-z0-9_]/', $_POST['select_users_lang'])) {
                    //File name contains non alphanum chars die to prevent file system attacks.
                    die;
                }
                $_SESSION['users_lang'] = $_POST['select_users_lang'];
            }
        }

        // Determine which Language File to Use
        if (isset($_SESSION['users_lang']) && $_SESSION['users_lang'] != $this->config['lang']) {
            /** @psalm-suppress UnresolvableInclude */
            include_once $this->config['basepath'] . '/include/language/' . $_SESSION['users_lang'] . '/lang.inc.php';
        } else {
            // Use Site Default Language
            if (isset($_SESSION['users_lang'])) {
                unset($_SESSION['users_lang']);
            }
            /** @psalm-suppress UnresolvableInclude */
            include_once $this->config['basepath'] . '/include/language/' . $this->config['lang'] . '/lang.inc.php';
        }

        // Multilingual Add-on
        //        $filename = $this->config['basepath'] . '/addons/multilingual/addon.inc.php';
        //        if (file_exists($filename)) {
        //            if (!isset($_SESSION['language_template'])) {
        //                /** @psalm-suppress UnresolvableInclude */
        //                include_once $this->config['basepath'] . '/addons/multilingual/language/' . $this->config['lang'] . '/lang.inc.php';
        //            } else {
        //                /** @psalm-suppress UnresolvableInclude */
        //                include_once $this->config['basepath'] . '/include/language/' . $_SESSION['language_template'] . '/lang.inc.php';
        //                /** @psalm-suppress UnresolvableInclude */
        //                include_once $this->config['basepath'] . '/addons/multilingual/language/' . $_SESSION['language_template'] . '/lang.inc.php';
        //            }
        //        }
    }

    /**
     * @psalm-suppress ConflictingReferenceConstraint
     * @return void
     */
    private function determineLoadType(): void
    {
        $uri = $_SERVER['REQUEST_URI'] ?? '';
        if (str_contains($uri, 'admin/index.php') || str_ends_with($uri, 'admin/')) {
            $this->loadAdminPage();
        } elseif (str_contains($uri, 'admin/ajax.php')) {
            $this->loadAdminAjax();
        } elseif (str_contains($uri, 'ajax.php')) {
            $this->loadUserAjax();
        } elseif (str_contains($uri, 'pingback.php')) {
            $this->loadPingBack();
        } else {
            $this->loadUserPage();
        }
    }

    private function loadAdminPage(): void
    {
        $this->setUserMetaTagVariabless();
        $this->handleLogOut('admin');
        $page = new PageAdmin($this->dbh, $this->config);

        if (!isset($_GET['action'])) {
            $page->magicURIParser(true);
        }
        $this->handleMissingAction($page);
        $this->loadCss($page, $this->config['admin_template_path']);
        /** @var PageAdmin $page */
        $this->loadAdminMainTemplateFile($page, $this->config['admin_template_path']);
        $this->loadConentSecurityPolicy();
        $page->replaceTags(['content']);
        $page->replaceCurrentUserTags();
        $page->replacePermissionTags();
        $page->replaceUrls();
        $page->autoReplaceTags('', true);
        $page->replaceTags(['load_js', 'load_ORjs', 'load_js_last']);
        $page->replaceCssTemplateTags(true);
        $page->replaceMetaTemplateTags();
        $page->replaceLangTemplateTags();
        $page->outputPage();
    }

    private function loadPingBack(): void
    {
        //Set XML Header
        header('Content-Type: application/xml');
        header('Cache-control: private'); //IE6 Form Refresh Fix
        $pingBackFunctions = new PingBackFunctions($this->dbh, $this->config);
        $a = ['pingback.ping' => ['function' => [$pingBackFunctions, 'processPing']]];
        $s = new Server($a, false);
        try {
            $s->service();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    private function setUserMetaTagVariabless(): void
    {
        global $meta_canonical, $meta_follow, $meta_index;
        $meta_follow = true;
        if (isset($_GET['printer_friendly']) && $_GET['printer_friendly'] == 'yes') {
            $meta_index = false;
        } else {
            $meta_index = true;
        }
        $meta_canonical = null;
    }

    private function handleLogOut(string $type = 'user'): void
    {
        if (isset($_GET['action']) && ($_GET['action'] == 'logout' || $_GET['action'] == 'log_out')) {
            $login = new Login($this->dbh, $this->config);
            $login->logOut($type);
        }
    }

    private function handleMissingAction(PageUser|PageAdmin &$page): void
    {
        if (!isset($_GET['action'])) {
            $page->magicURIParser(true);
        }
        if (isset($_GET['action']) && is_string($_GET['action'])) {
            if (str_contains($_GET['action'], '://') || $_GET['action'] == 'notfound') {
                $_GET['action'] = 'index';
            }
        }
    }

    private function loadCss(PageUser|PageAdmin &$page, string $template_path): void
    {
        if (isset($_GET['action']) && $_GET['action'] == 'load_css' && isset($_GET['css_file']) && is_string($_GET['css_file'])) {
            if (preg_match('/[^A-Za-z0-9_]/', $_GET['css_file'])) {
                //File name contains non alphanum chars die to prevent file system attacks.
                die;
            }

            $file = $template_path . '/' . $_GET['css_file'] . '.css';
            //If we are using seo urls, check to see if page should be sent or if the cached copy is ok
            if ($this->config['url_style'] != '1') {
                if (file_exists($template_path . '/' . $_GET['css_file'] . '.css')) {
                    $my_file = $template_path . '/' . $_GET['css_file'] . '.css';
                } elseif (file_exists($this->config['basepath'] . '/template/default/' . $_GET['css_file'] . '.css')) {
                    $my_file = $this->config['basepath'] . '/template/default/' . $_GET['css_file'] . '.css';
                } else {
                    header('HTTP/1.1 404 File Not Found');
                    exit;
                }

                $last_modified_time = filemtime($my_file);
                $etag = md5_file($my_file);
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $last_modified_time) . ' GMT');
                header('Etag: ' . $etag);
                if (
                    (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && @strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time) ||
                    (isset($_SERVER['HTTP_IF_NONE_MATCH']) && $_SERVER['HTTP_IF_NONE_MATCH'] == $etag)
                ) {
                    header('HTTP/1.1 304 Not Modified');
                    exit;
                }
            }
            $page->loadPage($file);
            $page->replaceTags(['company_logo', 'baseurl', 'template_url']);
            $css_text = $page->returnPage();
            header('Content-type: text/css');
            echo $css_text;
            $this->flushAndExit();
        }
    }

    private function flushAndExit(): void
    {
        $this->setEndTime();
        $this->recordTracingEvent();
        ob_end_flush();
        session_write_close();
    }

    private function setEndTime(): void
    {
        $this->end_time = $this->getMicroTime();
    }

    private function recordTracingEvent(): void
    {
        $render_time = $this->end_time - $this->start_time;
        if ($this->recordTracking) {
            $tracking = new Tracking($this->dbh, $this->config);
            $tracking->record($render_time);
        }
        header('X-RenderTime:' . $render_time);
    }

    private function loadAdminMainTemplateFile(PageAdmin &$page, string $template_path): void
    {
        if (isset($_GET['popup']) && $_GET['popup'] != 'blank') {
            $page->loadPage($template_path . '/popup.html', true);
        } else {
            $page->loadPage($template_path . '/main.html', true);
        }
    }

    private function loadConentSecurityPolicy(): void
    {
        header("Content-Security-Policy: default-src 'self'; script-src 'self' 'unsafe-inline'; style-src 'self' data: 'unsafe-inline'; img-src * 'self' data:; font-src 'self' data:; worker-src blob: 'self';");
    }

    private function loadAdminAjax(): void
    {
        $page = new PageAdminAjax($this->dbh, $this->config);
        $page->page = $page->callAjax();
        $page->outputPage();
    }

    private function loadUserAjax(): void
    {
        $page = new PageUserAjax($this->dbh, $this->config);
        //Create dumb page for the template engine
        $page->page = $page->callAjax();
        $page->outputPage();
    }

    private function loadUserPage(): void
    {
        $this->enableTracking();
        $this->checkForInstallFiles();
        $this->checkDefaultEmailIsSet();
        $this->setUserMetaTagVariabless();
        $this->handleLogOut();
        $page = new PageUser($this->dbh, $this->config);
        if (!isset($_GET['action'])) {
            $page->magicURIParser();
        } else {
            $page->setSessionReferrer();
        }
        $this->handleMissingAction($page);
        //Load Powered BY
        if (isset($_GET['action']) && $_GET['action'] == 'powered_by') {
            echo $page->outputPoweredBy();
            $this->flushAndExit();
        }
        $this->checkMaintenanceMode($page);
        $this->loadCss($page, $this->config['template_path']);
        /** @var PageUser $page */
        $this->loadUserMainTemplateFile($page);
        $this->checkUserIsBanned($page, $this->config['template_path']);

        $page->replaceTags(['content']);


        //Replace Permission tags first
        $page->replaceForeachPclassBlock();
        $page->replaceIfAddonBlock();
        $page->replaceCustomAgentSearchBlock();
        $page->replaceCustomListingSearchBlock();
        $page->replaceCustomBlogSearchBlock();
        $page->replaceCurrentUserTags();
        $page->replacePermissionTags();
        $page->replaceUrls();
        $page->replaceMetaTemplateTags();
        $page->replaceBlogTemplateTags();
        $page->replaceSearchFieldTags();
        $page->autoReplaceTags();
        // Load js last to make sure all custom js was added
        $page->replaceTags(['load_js', 'load_ORjs', 'load_js_last']);
        //Replace Languages
        $page->replaceLangTemplateTags();
        $page->replaceCssTemplateTags();
        $page->outputPage();
    }

    private function checkForInstallFiles(): void
    {
        $filename = dirname(__FILE__, 2) . '/install/index.php';
        if (file_exists($filename) && !file_exists(dirname(__FILE__, 2) . '/devignoreinstall') && (!isset($_GET['action']) || $_GET['action'] != 'powered_by')) {
            if (!file_exists(dirname(__FILE__) . '/common.php')) {
                $host = $_SERVER['HTTP_HOST'] ?? '';
                // phpcs:ignore
                $uri = rtrim(dirname($_SERVER['PHP_SELF'] ?? ''), '/\\');
                $extra = 'install/index.php';
                if ($host != '') {
                    header('Location: http://' . $host . $uri . '/' . $extra);
                }
                exit;
            }
            die('<html><div style="color:red;text-align:center">You must delete the file ' . $filename . ' before you can access your open-realty install.</div></html>');
        }
    }

    private function checkDefaultEmailIsSet(): void
    {
        // Check that the default email address has been changed to something other then an open-realty.org address.
        $default_email = strpos($this->config['admin_email'], 'changeme@default.com');
        if (!isset($_GET['action']) || $_GET['action'] != 'powered_by') {
            if ($default_email !== false) {
                die('<div style="color:red;text-align:center">You must set an administrative email address in the site configuration before you can use your site. </div>');
            }
        }
    }

    private function checkMaintenanceMode(PageUser|PageAdmin &$page): void
    {
        if ($this->config['maintenance_mode'] && (!isset($_SESSION['username']) || $_SESSION['username'] !== 'admin')) {
            @header('HTTP/1.1 503 Service Temporarily Unavailable');
            @header('Status: 503 Service Temporarily Unavailable');
            header('Retry-After: 7200'); // in seconds
            $page->loadPage($this->config['template_path'] . '/maintenance_mode.html', true);
        }
    }

    private function loadUserMainTemplateFile(PageUser &$page): void
    {
        if (isset($_GET['action']) && $_GET['action'] === 'listingqrcode') {
            $page->page = '{content}';
        } elseif (isset($_GET['action']) && is_string($_GET['action']) && str_starts_with($_GET['action'], 'rss_')) {
            $page->page = '{content}';
        } elseif (isset($_GET['action']) && $_GET['action'] == 'show_rss' && isset($_GET['rss_feed']) && is_string($_GET['rss_feed'])) {
            if (preg_match('/[^A-Za-z0-9_]/', $_GET['rss_feed'])) {
                //File name contains non alphanum chars die to prevent file system attacks.
                die;
            }
            $page->page = '{content}';
        } else {
            //This is a regular Open-Realty page determine which template to use.
            if (isset($_GET['popup'])) {
                if ($_GET['popup'] == 'blank') {
                    $page->loadPage($this->config['template_path'] . '/blank.html', true);
                } else {
                    $page->loadPage($this->config['template_path'] . '/popup.html', true);
                }
            } elseif (isset($_GET['printer_friendly']) && $_GET['printer_friendly'] == 'yes') {
                $page->loadPage($this->config['template_path'] . '/printer_friendly.html', true);
            } else {
                if (isset($_GET['PageID']) && is_numeric($_GET['PageID']) && file_exists($this->config['template_path'] . '/page' . $_GET['PageID'] . '_main.html')) {
                    $page->loadPage($this->config['template_path'] . '/page' . $_GET['PageID'] . '_main.html', true);
                } elseif (isset($_GET['action']) && $_GET['action'] == 'index' && file_exists($this->config['template_path'] . '/page1_main.html')) {
                    $page->loadPage($this->config['template_path'] . '/page1_main.html', true);
                } elseif (isset($_GET['action']) && $_GET['action'] == 'searchresults' && file_exists($this->config['template_path'] . '/searchresults_main.html')) {
                    $page->loadPage($this->config['template_path'] . '/searchresults_main.html', true);
                } elseif (isset($_GET['action']) && $_GET['action'] == 'listingview' && file_exists($this->config['template_path'] . '/listingview_main.html')) {
                    $page->loadPage($this->config['template_path'] . '/listingview_main.html', true);
                } else {
                    $page->loadPage($this->config['template_path'] . '/main.html', true);
                }
            }
        }
    }

    private function checkUserIsBanned(PageUser|PageAdmin &$page, string $template_path): void
    {
        $misc = new Misc($this->dbh, $this->config);
        if ($misc->isBannedSiteIp()) {
            $page->loadPage($template_path . '/banned_ip.html', true);
        }
    }

    private function enableTracking(): void
    {
        $this->recordTracking = true;
    }

    private function proccessGoogleAuthRedirect(PageUser|PageAdmin &$page): void
    {
        //Handle Google Auth Redirect
        if (isset($_GET['code']) && isset($_GET['scope']) && (!isset($_GET['action']) || $_GET['action'] == 'notfound')) {
            $login = new Login($this->dbh, $this->config);
            $login_status = $login->loginCheck('Member');
            if ($login_status !== true) {
                $page->replaceTag('content', $login_status);
            }
            if (array_key_exists('action', $_GET)) {
                $_GET['action'] = 'index';
            }
        }
    }
}
