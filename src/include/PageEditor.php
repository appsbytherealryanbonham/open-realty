<?php

declare(strict_types=1);

namespace OpenRealty;

class PageEditor extends BaseClass
{
    public function pageEditIndex(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('editpages');
        $display = '';

        if ($security === true) {
            //Load the Core Template
            $page = $this->newPageAdmin();

            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/page_edit_index.html');

            //What Access Rights does user have to pages? Access page Manager means they are at least a contributor.
            /*
             * page Permissions
             * 1 - Subscriber - A subscriber can read posts, comment on posts.
             * 2 - Contributor - A contributor can post and manage their own post but they cannot publish the posts. An administrator
             * must first approve the post before it can be published.
             * 3 - Author - The Author role allows someone to publish and manage posts. They can only manage their own posts, no one
             * else’s.
             * 4 - Editor - An editor can publish posts. They can also manage and edit other users posts. If you are looking for
             * someone to edit your posts, you would assign the Editor role to that person.
             */

            if ($this->config['demo_mode'] && $_SESSION['admin_privs'] != 'yes') {
                $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
            } else {
                if (isset($_POST['delete'])) {
                    if (isset($_POST['pageID']) && $_POST['pageID'] != 0) {
                        // Delete page
                        $pageID = intval($_POST['pageID']);
                        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'pagesmain WHERE pagesmain_id = ' . $pageID;
                        $recordSet = $ORconn->Execute($sql);
                        if (is_bool($recordSet)) {
                            $misc->logErrorAndDie($sql);
                        }
                        $_POST['pageID'] = '';
                    }
                }
            }

            //Replace Status Counts
            //{page_edit_status_all_count}
            $sql = 'SELECT count(pagesmain_id) as pagecount FROM ' . $this->config['table_prefix'] . 'pagesmain';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_all = $recordSet->fields('pagecount');

            $sql = 'SELECT count(pagesmain_id) as pagecount FROM ' . $this->config['table_prefix'] . 'pagesmain WHERE pagesmain_published
= 1';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_published = $recordSet->fields('pagecount');

            $sql = 'SELECT count(pagesmain_id) as pagecount FROM ' . $this->config['table_prefix'] . 'pagesmain WHERE pagesmain_published
= 0';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_draft = $recordSet->fields('pagecount');

            $sql = 'SELECT count(pagesmain_id) as pagecount FROM ' . $this->config['table_prefix'] . 'pagesmain WHERE pagesmain_published
= 2';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $count_review = $recordSet->fields('pagecount');

            $page->replaceTag('page_edit_status_all_count', $count_all);
            $page->replaceTag('page_edit_status_published_count', $count_published);
            $page->replaceTag('page_edit_status_draft_count', $count_draft);
            $page->replaceTag('page_edit_status_review_count', $count_review);
            //Get Status
            $statusSQL = '';
            if (isset($_GET['status']) && $_GET['status'] == 'Published') {
                $statusSQL = 'pagesmain_published = 1';
            } elseif (isset($_GET['status']) && $_GET['status'] == 'Draft') {
                $statusSQL = 'pagesmain_published = 0';
            } elseif (isset($_GET['status']) && $_GET['status'] == 'Review') {
                $statusSQL = 'pagesmain_published = 2';
            }

            //Show page List
            if (!empty($statusSQL)) {
                $sql = 'SELECT pagesmain_title, pagesmain_id, pagesmain_date, pagesmain_published, pagesmain_keywords
FROM ' . $this->config['table_prefix'] . 'pagesmain
WHERE ' . $statusSQL;
            } else {
                $sql = 'SELECT pagesmain_title, pagesmain_id, pagesmain_date, pagesmain_published, pagesmain_keywords
FROM ' . $this->config['table_prefix'] . 'pagesmain';
            }

            //Load Record Set
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            //Handle Next prev
            $num_rows = $recordSet->RecordCount();
            if (!isset($_GET['cur_page']) || !is_numeric($_GET['cur_page'])) {
                $_GET['cur_page'] = 0;
            }
            $_GET['cur_page'] = (int)$_GET['cur_page'];

            $limit_str = $_GET['cur_page'] * $this->config['listings_per_page'];
            $recordSet = $ORconn->SelectLimit($sql, $this->config['listings_per_page'], $limit_str);
            $page_edit_template = '';

            while (!$recordSet->EOF) {
                $page_edit_template .= $page->getTemplateSection('page_edit_item_block');
                //echo $page_edit_template;
                $title = $recordSet->fields('pagesmain_title');
                $pagesmain_id = $recordSet->fields('pagesmain_id');
                $keywords = $recordSet->fields('pagesmain_keywords');
                $page_date = $recordSet->fields('pagesmain_date');
                $page_published = $recordSet->fields('pagesmain_published');
                $page_date = $misc->convertTimestamp($page_date, true);
                //Get Author

                $page_edit_template = $page->parseTemplateSection($page_edit_template, 'page_edit_item_title', $title);
                $page_edit_template = $page->parseTemplateSection($page_edit_template, 'page_edit_item_id', $pagesmain_id);
                $page_edit_template = $page->parseTemplateSection($page_edit_template, 'page_edit_item_date', $page_date);
                $page_edit_template = $page->parseTemplateSection($page_edit_template, 'page_edit_item_keywords', $keywords);

                switch ($page_published) {
                    case 0:
                        $page_edit_template = $page->parseTemplateSection(
                            $page_edit_template,
                            'page_edit_item_published',
                            $lang['page_draft']
                        );
                        break;
                    case 1:
                        $page_edit_template = $page->parseTemplateSection(
                            $page_edit_template,
                            'page_edit_item_published',
                            $lang['page_published']
                        );

                        break;
                    case 2:
                        $page_edit_template = $page->parseTemplateSection(
                            $page_edit_template,
                            'page_edit_item_published',
                            $lang['page_review']
                        );
                        break;
                }
                $recordSet->MoveNext();
            }
            //Next Prev
            $next_prev = $misc->nextPrev($num_rows, $_GET['cur_page'], 'page', true);
            $page->replaceTag('next_prev', $next_prev);
            $page->replaceTemplateSection('page_edit_item_block', $page_edit_template);
            $page->replaceLangTemplateTags();
            $page->replacePermissionTags();
            $page->autoReplaceTags('', true);
            $display .= $page->returnPage();
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }

        return $display;
    }

    /**
     * **************************************************************************\
     * function pageEdit() - Display's the page editor *
     * \**************************************************************************
     */
    public function pageEdit(): string
    {
        global $lang, $ORconn;

        $misc = $this->newMisc();
        $login = $this->newLogin();
        $security = $login->verifyPriv('editpages');
        $display = '';

        if ($security === true) {
            //Load the Core Template

            $page = $this->newPageAdmin();

            $page_functions = $this->newPageFunctions();
            //Load TEmplate File
            $page->loadPage($this->config['admin_template_path'] . '/page_edit_post.html');
            //$page_user_type = intval($_SESSION['page_user_type']);
            if (isset($_GET['id'])) {
                // Save pageID to Session for Image Upload Plugin
                $_SESSION['pageID'] = intval($_GET['id']);
                $pageID = intval($_GET['id']);

                //Make Sure Image Folder Exists For this page
                if (!file_exists($this->config['basepath'] . '/images/page_upload/' . $pageID)) {
                    mkdir($this->config['basepath'] . '/images/page_upload/' . $pageID);
                }
                $_SESSION['filemanager_basepath'] = $this->config['basepath'];
                $_SESSION['filemanager_baseurl'] = $this->config['baseurl'];
                $_SESSION['filemanager_pathpart'] = '/images/page_upload/' . $pageID . '/';

                $page->replaceTag('page_id', (string)$pageID);
                $sql = 'SELECT pagesmain_published FROM ' . $this->config['table_prefix'] . 'pagesmain WHERE pagesmain_id = ' . $pageID;
                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                //Deal with Template Tag
                //$html = str_replace('{template_url}',$this->config['template_url'],$html);
                $published = intval($recordSet->fields('pagesmain_published'));

                //No Page is Dirty on load.
                $page->replaceTag('page_revert_button_state', 'display:none;');
                $page->replaceTag('page_published_lang', $lang['page_draft']);
                //Show page Categories.
                //Load JS to Handle Cat Changes.
                //$this->page_edit_category_change($pageID);
                $article_url = $page_functions->getPageUrl($pageID);
                $page->replaceTag('page_article_url', $article_url);

                $status_html = $page->getTemplateSection('page_status_option_block');
                $status_html_replace = '';
                //Build Draft Option
                $status_html_replace .= $status_html;
                if ($published == 0) {
                    $status_html_replace = $page->parseTemplateSection(
                        $status_html_replace,
                        'page_status_selected',
                        'selected="selected"'
                    );
                } else {
                    $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_selected', '');
                }
                $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_value', '0');
                $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_text', $lang['page_draft']);
                //Build Review Option
                $status_html_replace .= $status_html;
                if ($published == 2) {
                    $status_html_replace = $page->parseTemplateSection(
                        $status_html_replace,
                        'page_status_selected',
                        'selected="selected"'
                    );
                } else {
                    $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_selected', '');
                }
                $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_value', '2');
                $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_text', $lang['page_review']);
                //Build Published Option
                $status_html_replace .= $status_html;
                if ($published == 1) {
                    $status_html_replace = $page->parseTemplateSection(
                        $status_html_replace,
                        'page_status_selected',
                        'selected="selected"'
                    );
                } else {
                    $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_selected', '');
                }
                $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_value', '1');
                $status_html_replace = $page->parseTemplateSection($status_html_replace, 'page_status_text', $lang['page_published']);
                $page->replaceTemplateSection('page_status_option_block', $status_html_replace);
                //Show Link to page Manager
                $page->replaceTag('page_manager_url', 'index.php?action=edit_page');
                $page->replaceTag('page_edit_action', 'index.php?action=edit_page_post');

                if ($this->config['demo_mode'] && $_SESSION['admin_privs'] != 'yes') {
                    $page->page = $page->removeTemplateBlock('page_update', $page->page);
                    $page->page = $page->removeTemplateBlock('page_delete', $page->page);
                } else {
                    $page->page = $page->cleanupTemplateBlock('page_update', $page->page);
                    $page->page = $page->cleanupTemplateBlock('page_delete', $page->page);
                }
                $page->replacePermissionTags();

                $page->autoReplaceTags('', true);
                $display .= $page->returnPage();
            }
        } else {
            $display .= '<div class="error_text">' . $lang['access_denied'] . '</div>';
        }
        return $display;
    }
}
