<?php

declare(strict_types=1);

namespace OpenRealty;

class BlogDisplay extends BaseClass
{
    /**
     * @return string
     */
    public function displayBlogIndex(): string
    {
        global $ORconn, $meta_index, $blog_id;

        //Load the Core Template
        $misc = $this->newMisc();
        $page = $this->newPageUser();
        $addons = $page->loadAddons();
        $addon_fields = $page->getAddonTemplateFieldList($addons);
        $blog_functions = $this->newBlogFunctions();
        // Make Sure we passed the PageID
        $display = '';
        if (isset($_GET['tag_id'])) {
            //Getting only Post that have a tag in them.
            $tag_sql = ' AND blogmain_id IN (SELECT blogmain_id FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation WHERE tag_id = ' . intval($_GET['tag_id']) . ') ';
            //Add No index metatag.
            $meta_index = false;
        } else {
            $tag_sql = '';
        }

        if (isset($_GET['cat_id'])) {
            //Getting only Post that have a tag in them.
            $cat_sql = ' AND blogmain_id IN (SELECT blogmain_id FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation WHERE category_id = ' . intval($_GET['cat_id']) . ') ';
            //Add No index metatag.
            $meta_index = false;
        } else {
            $cat_sql = '';
        }

        //Deal with Dates
        if (isset($_GET['year']) && isset($_GET['month']) && is_scalar($_GET['month']) && is_scalar($_GET['year'])) {
            //First Day of Month
            $first_day = strtotime('01 ' . $_GET['month'] . ' ' . $_GET['year']);
            $month_number = date('n', $first_day);
            $month_number++;
            $last_day = strtotime($_GET['year'] . '-' . $month_number . '-00 23:59:59');
            $meta_index = false;
            if ($first_day == '' || $last_day == '') {
                $date_sql = '';
            } else {
                $date_sql = ' AND (blogmain_date >= ' . $first_day . ' AND blogmain_date <= ' . $last_day . ') ';
            }
        } else {
            $date_sql = '';
        }
        //Get Number of blog posts.
        $sql = 'SELECT blogmain_id FROM ' . $this->config['table_prefix'] . "blogmain WHERE blogmain_published = 1 $tag_sql $cat_sql $date_sql ORDER BY blogmain_date DESC";
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        $num_rows = $recordSet->RecordCount();

        $sql = 'SELECT blogmain_full,blogmain_id,userdb_id 
				FROM ' . $this->config['table_prefix'] . "blogmain 
				WHERE blogmain_published = 1 $tag_sql $cat_sql $date_sql 
				ORDER BY blogmain_date DESC";

        if (!isset($_GET['cur_page'])) {
            $_GET['cur_page'] = 0;
        }
        $limit_str = intval($_GET['cur_page']) * $this->config['blogs_per_page'];
        $next_prev = $misc->nextPrev($num_rows, intval($_GET['cur_page']));
        $next_prev_bottom = $misc->nextPrev($num_rows, intval($_GET['cur_page']), 'bottom');
        $recordSet = $ORconn->SelectLimit($sql, $this->config['blogs_per_page'], $limit_str);
        $page->loadPage($this->config['template_path'] . '/blog_index.html');
        //Display Tag HEader if needed.
        if (isset($_GET['cat_id'])) {
            //Get Tag
            $cat_name = $blog_functions->getBlogCategoryName(intval($_GET['cat_id']));
            $cat_description = $blog_functions->getBlogCategoryDescription(intval($_GET['cat_id']));
            $page->page = $page->parseTemplateSection($page->page, 'cat_name', htmlentities($cat_name, ENT_COMPAT, $this->config['charset']));
            $page->page = $page->parseTemplateSection($page->page, 'cat_description', htmlentities($cat_description, ENT_COMPAT, $this->config['charset']));
            $page->page = $page->cleanupTemplateBlock('cat', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('cat', $page->page);
        }
        //Display Tag HEader if needed.
        if (isset($_GET['tag_id'])) {
            //Get Tag
            $tag_name = $blog_functions->getTagName(intval($_GET['tag_id']));
            $tag_description = $blog_functions->getTagDescription(intval($_GET['tag_id']));
            $page->page = $page->parseTemplateSection($page->page, 'tag_name', htmlentities($tag_name, ENT_COMPAT, $this->config['charset']));
            $page->page = $page->parseTemplateSection($page->page, 'tag_description', htmlentities($tag_description, ENT_COMPAT, $this->config['charset']));
            $page->page = $page->cleanupTemplateBlock('tag', $page->page);
        } else {
            $page->page = $page->removeTemplateBlock('tag', $page->page);
        }
        $blog_entry_template = '';
        while (!$recordSet->EOF) {
            $blog_entry_template .= $page->getTemplateSection('blog_entry_block');
            $blog_author_id = (int)$recordSet->fields('userdb_id');
            //Get Fields
            $blog_id = (int)$recordSet->fields('blogmain_id');
            $full = (string)$recordSet->fields('blogmain_full');
            //Start Replacing Tags
            $blog_title = $blog_functions->getBlogTitle($blog_id);
            $blog_entry_template = $page->parseTemplateSection($blog_entry_template, 'blog_title', $blog_title);

            //Deal with Code Tags
            preg_match_all('/<code>(.*?)<\/code>/is', $full, $code_tags);
            //echo '<pre>'.print_r($code_tags).'</pre>';
            if (isset($code_tags[1])) {
                foreach ($code_tags[1] as $x => $tag) {
                    $new_tag = str_replace('{', '&#123;', $tag);
                    $new_tag = str_replace('}', '&#125;', $new_tag);
                    $new_tag = str_replace('>', '&gt;', $new_tag);
                    $new_tag = str_replace('<', '&lt;', $new_tag);
                    $code_tags[1][$x] = $new_tag;
                }
                foreach ($code_tags[0] as $x => $tag) {
                    $full = str_replace($tag, '<code>' . $code_tags[1][$x] . '</code>', $full);
                }
            }

            //Handle blog_listing_# blocks
            preg_match_all('/{(blog_listing_[\d]*)}/m', $full, $matches);
            $blog_listings = array_unique($matches[1]);
            foreach ($blog_listings as $blog_listing) {
                $listing_template = $page->getTemplateSection($blog_listing, $full);
                //Skip incomplete blogs, tags will just be stripped
                if (!$listing_template) {
                    continue;
                }
                //Get Listing ID
                preg_match('/blog_listing_([\d]*)/', $blog_listing, $id_match);
                $listing_id = (int)$id_match[1];
                $listing_template = $page->replaceListingFieldTags($listing_id, $listing_template);
                $full = $page->replaceTemplateSection($blog_listing, $listing_template, $full);
            }
            $summary_endpos = strpos($full, '<hr');
            if ($summary_endpos !== false) {
                $summary = substr($full, 0, $summary_endpos);
            } else {
                $summary = $full;
            }
            $blog_entry_template = $page->parseTemplateSection($blog_entry_template, 'blog_id', (string)$blog_id);

            $blog_entry_template = $page->parseTemplateSection($blog_entry_template, 'blog_summary', $summary);
            $blog_author = $blog_functions->getBlogAuthor($blog_id);
            $blog_entry_template = $page->replaceUserFieldTags($blog_author_id, $blog_entry_template, 'blog_author');
            $blog_entry_template = $page->parseTemplateSection($blog_entry_template, 'blog_author', $blog_author);
            $blog_comment_count = $blog_functions->getBlogCommentCount($blog_id);
            $blog_entry_template = $page->parseTemplateSection($blog_entry_template, 'blog_comment_count', (string)$blog_comment_count);
            $blog_date_posted = $blog_functions->getBlogDate($blog_id);
            $blog_entry_template = $page->parseTemplateSection($blog_entry_template, 'blog_date_posted', $blog_date_posted);
            $article_url = $page->magicURIGenerator('blog', (string)$blog_id, true);
            $blog_entry_template = $page->parseTemplateSection($blog_entry_template, 'blog_link_article', $article_url);

            $blog_entry_template = $page->parseAddonTags($blog_entry_template, $addon_fields);
            $recordSet->MoveNext();
        }
        $page->replaceBlogTemplateTags();
        $page->replaceTemplateSection('blog_entry_block', $blog_entry_template);
        $page->replacePermissionTags();
        $page->cleanupTemplateSections($next_prev, $next_prev_bottom);

        $display .= $page->returnPage();
        return $display;
    }

    public function display(): string
    {
        global $ORconn, $lang, $jscript, $meta_canonical;

        $misc = $this->newMisc();
        $userclass = $this->newUser();
        $page = $this->newPageUser();
        $blog_functions = $this->newBlogFunctions();
        //Load Template
        $page->loadPage($this->config['template_path'] . '/blog_article.html');
        // Make Sure we passed the PageID
        $display = '';
        if (!isset($_GET['ArticleID']) && intval($_GET['ArticleID']) <= 0) {
            $display .= 'ERROR. PageID not sent';
        } else {
            $blog_id = intval($_GET['ArticleID']);
            //Add Pingback headers
            header('X-Pingback: ' . $this->config['baseurl'] . '/pingback.php');
            $jscript .= '<link rel="pingback" href="' . $this->config['baseurl'] . '/pingback.php" />';
            //Check if we posted a comment.
            if (isset($_SESSION['userID']) && $_SESSION['userID'] > 0 && isset($_POST['comment_text']) && is_string($_POST['comment_text']) && strlen($_POST['comment_text']) > 0) {
                $blog_comment = $misc->makeDbSafe(strip_tags($_POST['comment_text']));
                if ($this->config['blog_requires_moderation']) {
                    if (isset($_SESSION['blog_user_type']) && $_SESSION['blog_user_type'] == 4) {
                        $moderated = 1;
                    } else {
                        $moderated = 0;
                    }
                } else {
                    $moderated = 1;
                }
                $sql = 'INSERT INTO ' . $this->config['table_prefix'] . 'blogcomments (userdb_id,blogcomments_timestamp,blogcomments_text,blogmain_id,blogcomments_moderated) 
						VALUES (' . intval($_SESSION['userID']) . ',' . time() . ",$blog_comment,$blog_id,$moderated);";

                $recordSet = $ORconn->Execute($sql);
                if (is_bool($recordSet)) {
                    $misc->logErrorAndDie($sql);
                }
                $comment_id = $ORconn->Insert_ID();
                if ($moderated == 0) {
                    $page->page = $page->cleanupTemplateBlock('comment_moderated', $page->page);
                }
                $hooks = $this->newHooks();
                $hooks->load('after_new_blog_comment', $comment_id);
            }
            $page->page = $page->removeTemplateBlock('comment_moderated', $page->page);

            //$display .= '<div class="page_display">';
            $sql = 'SELECT userdb_id,blogmain_full,blogmain_id,blogmain_published 
					FROM ' . $this->config['table_prefix'] . 'blogmain 
					WHERE blogmain_id=' . $blog_id;
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            //$full = html_entity_decode($recordSet->fields('blogmain_full'), ENT_NOQUOTES, $this->config['charset']);
            $full = (string)$recordSet->fields('blogmain_full');
            $full = preg_replace('/\<hr.*?\>/', '', $full, 1);

            //Deal with Code Tags
            preg_match_all('/<code>(.*?)<\/code>/is', $full, $code_tags);
            //echo '<pre>'.print_r($code_tags).'</pre>';
            if (isset($code_tags[1])) {
                foreach ($code_tags[1] as $x => $tag) {
                    $new_tag = str_replace('{', '&#123;', $tag);
                    $new_tag = str_replace('}', '&#125;', $new_tag);
                    $new_tag = str_replace('>', '&gt;', $new_tag);
                    $new_tag = str_replace('<', '&lt;', $new_tag);
                    $code_tags[1][$x] = $new_tag;
                }
                foreach ($code_tags[0] as $x => $tag) {
                    $full = str_replace($tag, '<code>' . $code_tags[1][$x] . '</code>', $full);
                }
            }

            //Handle blog_listing_# blocks
            preg_match_all('/{(blog_listing_[\d]*)}/m', $full, $matches);
            $blog_listings = array_unique($matches[1]);
            foreach ($blog_listings as $blog_listing) {
                $listing_template = $page->getTemplateSection($blog_listing, $full);
                //Skip incomplete blogs, tags will just be stripped
                if (!$listing_template) {
                    continue;
                }
                //Get Listing ID
                preg_match('/blog_listing_([\d]*)/', $blog_listing, $id_match);
                $listing_id = (int)$id_match[1];
                $listing_template = $page->replaceListingFieldTags($listing_id, $listing_template);
                $full = $page->replaceTemplateSection($blog_listing, $listing_template, $full);
            }
            $id = (int)$recordSet->fields('blogmain_id');
            $status = (int)$recordSet->fields('blogmain_published');
            $blog_author_id = (int)$recordSet->fields('userdb_id');
            if ($status != 1) {
                if (!isset($_SESSION['blog_user_type']) || $_SESSION['blog_user_type'] < 4) {
                    return $lang['listing_editor_permission_denied'];
                }
            }

            //Start Replacing Tags

            $page->page = $page->parseTemplateSection($page->page, 'blog_id', (string)$id);

            $blog_title = $blog_functions->getBlogTitle($id);
            $page->page = $page->parseTemplateSection($page->page, 'blog_title', $blog_title);
            $page->replaceUserFieldTags($blog_author_id, '', 'blog_author');
            $blog_comment_count = $blog_functions->getBlogCommentCount($id);
            $page->page = $page->parseTemplateSection($page->page, 'blog_comment_count', (string)$blog_comment_count);
            $blog_date_posted = $blog_functions->getBlogDate($id);
            $page->page = $page->parseTemplateSection($page->page, 'blog_date_posted', $blog_date_posted);
            $page->page = $page->parseTemplateSection($page->page, 'blog_full_article', $full);

            //Show Blog Categories
            $assigned_cats = $blog_functions->getBlogCategoriesAssignmentNames($id);
            $cat_html = $page->getTemplateSection('category_block');
            $cat_html_replace = '';
            //<input type="checkbox" name="cat_id" value="{blog_category_id}" /> {blog_category_name}
            foreach ($assigned_cats as $cat_id => $cat_name) {
                $cat_html_replace = $page->cleanupTemplateBlock('category_delimiter', $cat_html_replace);
                $cat_html_replace .= $cat_html;
                $cat_url = $page->magicURIGenerator('blog_cat', (string)$cat_id, true);
                $cat_html_replace = $page->parseTemplateSection($cat_html_replace, 'category_url', $cat_url);
                $cat_html_replace = $page->parseTemplateSection($cat_html_replace, 'category_name', htmlentities($cat_name, ENT_COMPAT, $this->config['charset']));
            }
            $cat_html_replace = $page->removeTemplateBlock('category_delimiter', $cat_html_replace);
            $page->replaceTemplateSection('category_block', $cat_html_replace);

            // Allow Admin To Edit #
            if ((isset($_SESSION['can_access_blog_manager']) || $_SESSION['admin_privs'] == 'yes') && $this->config['wysiwyg_show_edit']) {
                $admin_edit_link = $this->config['baseurl'] . '/admin/index.php?action=edit_blog_post&amp;id=' . $id;
                $page->page = $page->parseTemplateSection($page->page, 'admin_edit_link', $admin_edit_link);
                $page->page = $page->cleanupTemplateBlock('admin_edit_link', $page->page);
            } else {
                $page->page = $page->removeTemplateBlock('admin_edit_link', $page->page);
            }

            //Tag Cloud
            $assigned_tags = $blog_functions->getBlogTagAssignment($id);
            $tag_html = $page->getTemplateSection('blog_tag_cloud_block');
            $tag_html_replace = '';
            //<input type="checkbox" name="cat_id" value="{blog_category_id}" /> {blog_category_name}
            foreach ($assigned_tags as $tag_info) {
                $tag_html_replace .= $tag_html;
                $tag_html_replace = $page->parseTemplateSection($tag_html_replace, 'tag_name', $tag_info['tag_name']);
                $tag_html_replace = $page->parseTemplateSection($tag_html_replace, 'tag_link', $tag_info['tag_link']);
                $tag_html_replace = $page->parseTemplateSection($tag_html_replace, 'tag_fontsize', (string)$tag_info['tag_fontsize']);
            }
            $page->replaceTemplateSection('blog_tag_cloud_block', $tag_html_replace);

            //Deal with Comments
            $sql = 'SELECT userdb_id,blogcomments_timestamp,blogcomments_text,blogcomments_id FROM ' . $this->config['table_prefix'] . 'blogcomments WHERE blogmain_id = ' . $id . ' AND blogcomments_moderated = 1 ORDER BY blogcomments_timestamp ASC;';
            $recordSet = $ORconn->Execute($sql);
            if (is_bool($recordSet)) {
                $misc->logErrorAndDie($sql);
            }
            $blog_comment_template = '';
            while (!$recordSet->EOF) {
                //Load DB Values
                $comment_author_id = $recordSet->fields('userdb_id');
                $comment_id = $recordSet->fields('blogcomments_id');
                $blogcomments_timestamp = $recordSet->fields('blogcomments_timestamp');
                $blogcomments_text = html_entity_decode($recordSet->fields('blogcomments_text'), ENT_NOQUOTES, $this->config['charset']);
                //Load Template Block
                $blog_comment_template .= $page->getTemplateSection('blog_article_comment_item_block');
                //Lookup Blog Author..
                $author_type = $userclass->getUserType($comment_author_id);
                if ($author_type == 'member') {
                    $author_display = $userclass->getUserSingleItem('userdb_user_name', $comment_author_id);
                } else {
                    $author_display = $userclass->getUserSingleItem('userdb_user_first_name', $comment_author_id) . ' ' . $userclass->getUserSingleItem('userdb_user_last_name', $comment_author_id);
                }
                $blog_comment_template = $page->parseTemplateSection($blog_comment_template, 'blog_comment_author', $author_display);
                $blog_comment_date_posted = $misc->convertTimestamp($blogcomments_timestamp, true);
                $blog_comment_template = $page->parseTemplateSection($blog_comment_template, 'blog_comment_date_posted', $blog_comment_date_posted);
                $blog_comment_template = $page->parseTemplateSection($blog_comment_template, 'blog_comment_text', $blogcomments_text);
                $blog_comment_template = $page->parseTemplateSection($blog_comment_template, 'blog_comment_id', $comment_id);

                $recordSet->MoveNext();
            }
            $page->replaceTemplateSection('blog_article_comment_item_block', $blog_comment_template);

            //Render Add New Comment

            $article_url = $page->magicURIGenerator('blog', (string)$id, true);
            $page->page = $page->parseTemplateSection($page->page, 'blog_comments_post_url', $article_url);

            //Add Canonical Link
            $meta_canonical = $page->magicURIGenerator('blog', (string)$id, true);

            //Render Page Out
            //$page->replaceTags(array('templated_search_form', 'featured_listings_horizontal', 'featured_listings_vertical', 'company_name', 'link_printer_friendly'));
            $page->replaceSearchFieldTags();
            $page->replacePermissionTags();

            $display .= $page->returnPage();
        }
        return $display;
    }
}
