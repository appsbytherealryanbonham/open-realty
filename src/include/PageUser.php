<?php

declare(strict_types=1);

namespace OpenRealty;

use Exception;

class PageUser extends Page
{
    public function replaceUserAction(): string
    {
        global $lang;

        $login = $this->newLogin();
        $data = '';
        switch ($_GET['action']) {
            case 'index':
                if ($this->config['default_page'] == 'wysiwyg_page') {
                    $_GET['PageID'] = 1;

                    $search = $this->newPageDisplay();
                    $data = $search->display();
                } elseif ($this->config['default_page'] == 'blog_index') {
                    $blog = $this->newBlogDisplay();
                    $data = $blog->displayBlogIndex();
                }
                break;
            case 'member_login':
                $data = $login->displayLogin('Member');
                break;
            case 'search_step_2':
                $search = $this->newSearchPage();
                $data = $search->createSearchPage();
                break;
            case 'searchpage':
                $search = $this->newSearchPage();
                $data = $search->createSearchPageLogic();
                break;
            case 'searchresults':
                $search = $this->newSearchPage();
                $data = $search->searchResults();
                break;
            case 'listingview':
                $listing = $this->newListingPages();
                $data = $listing->listingView();
                break;
            case 'addtofavorites':
                $listing = $this->newMembersFavorites();
                $data = $listing->addToFavorites();
                break;
            case 'view_favorites':
                $listing = $this->newMembersFavorites();
                $data = $listing->viewFavorites();
                break;
            case 'view_saved_searches':
                $listing = $this->newMembersSearch();
                $data = $listing->viewSavedSearches();
                break;
            case 'save_search':
                $listing = $this->newMembersSearch();
                $data = $listing->saveSearch();
                break;
            case 'delete_search':
                $listing = $this->newMembersSearch();
                $data = $listing->deleteSearch();
                break;
            case 'delete_favorites':
                $listing = $this->newMembersFavorites();
                $data = $listing->deleteFavorites();
                break;
            case 'page_display':
                $search = $this->newPageDisplay();
                $data = $search->display();
                break;
            case 'calculator':
                $calc = $this->newCalculators();
                $data = $calc->startCalc();
                break;
            case 'view_listing_image':
                $image = $this->newImageHandler();
                $data = $image->viewImage('listing');
                break;
            case 'view_user_image':
                $image = $this->newImageHandler();
                $data = $image->viewImage('userimage');
                break;
            case 'show_rss':
                if (isset($_GET['rss_feed']) && is_string($_GET['rss_feed'])) {
                    $rss = $this->newRss();
                    $data = $rss->rssView($_GET['rss_feed']);
                }
                break;
            case 'rss_featured_listings':
                $rss = $this->newRss();
                $data = $rss->rssView('featured');
                break;
            case 'rss_lastmodified_listings':
                $rss = $this->newRss();
                $data = $rss->rssView('lastmodified');
                break;
            case 'view_user':
                $user = $this->newUser();
                $data = $user->viewUser();
                break;
            case 'view_users':
                $user = $this->newUser();
                $data = $user->viewUsers();
                break;
            case 'edit_profile':
                if (!isset($_GET['user_id']) || !is_numeric($_GET['user_id'])) {
                    $_GET['user_id'] = 0;
                }
                $user_managment = $this->newUserManagement();
                $data = $user_managment->editMemberProfile((int)$_GET['user_id']);
                break;
            case 'signup':
                if (isset($_GET['type']) && is_string($_GET['type'])) {
                    $listing = $this->newUserManagement();
                    $data = $listing->userSignup($_GET['type']);
                }
                break;
            case 'show_vtour':
                if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                    $vtour = $this->newVtourHandler();
                    $data = $vtour->showVtour((int)$_GET['listingID']);
                } else {
                    $data = 'No Listing ID';
                }
                break;
            case 'contact_friend':
                $contact = $this->newContact();
                if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                    $data = $contact->contactFriendForm((int)$_GET['listing_id']);
                }
                break;
            case 'contact_agent':
                if ($this->config['allow_member_signup']) {
                    $contact = $this->newContact();
                    if (isset($_GET['listing_id']) && is_numeric($_GET['listing_id'])) {
                        $data = $contact->contactAgentForm((int)$_GET['listing_id']);
                    } elseif (isset($_GET['agent_id']) && is_numeric($_GET['agent_id'])) {
                        $data = $contact->contactAgentForm(0, (int)$_GET['agent_id']);
                    }
                } else {
                    $data = '<h3>' . $lang['no_user_signup'] . '</h3>';
                }
                break;
            case 'create_vcard':
                $user = $this->newUser();
                if (isset($_GET['user']) && is_numeric($_GET['user'])) {
                    $user->createVcard((int)$_GET['user']);
                }
                break;
            case 'create_download':
                $files = $this->newFileHandler();
                if (
                    isset($_GET['ID'], $_GET['file_id'], $_GET['type'])
                    && is_numeric($_GET['ID'])
                    && is_numeric($_GET['file_id'])
                    && is_string($_GET['type'])
                ) {
                    $files->createDownload((int)$_GET['ID'], (int)$_GET['file_id'], $_GET['type']);
                } elseif (
                    isset($_POST['ID'], $_POST['file_id'], $_POST['type'])
                    && is_numeric($_POST['ID'])
                    && is_numeric($_POST['file_id'])
                    && is_string($_POST['type'])
                ) {
                    $files->createDownload((int)$_POST['ID'], (int)$_POST['file_id'], $_POST['type']);
                }
                break;
            case 'blog_index':
                $blog = $this->newBlogDisplay();
                $data = $blog->displayBlogIndex();
                break;
            case 'blog_view_article':
                $blog = $this->newBlogDisplay();
                $data = $blog->display();
                break;
            case 'verify_email':
                $user_manager = $this->newUserManagement();
                $data = $user_manager->verifyEmail();
                break;
            case 'send_forgot':
                $data = $login->forgotPassword(false);
                break;
            case 'forgot':
                $data = $login->forgotPasswordReset();
                break;
            case 'powered_by':
                return $this->outputPoweredBy();
            case 'notfound':
                $search = $this->newPageDisplay();
                $data = $search->showPageNotFound();
                break;
            case 'listingqrcode':
                if (isset($_GET['listing_id'])) {
                    $listing = $this->newListingPages();
                    $listing_id = intval($_GET['listing_id']);
                    $listing->qrCode($listing_id);
                }
                break;
            case 'userqrcode':
                if (isset($_GET['user_id'])) {
                    $user = $this->newUser();
                    $userid = intval($_GET['user_id']);
                    return $user->qrCode($userid);
                }
                break;
            default:
                $addon_name = [];
                if (is_string($_GET['action'])) {
                    if (preg_match('/^addon_(.\S*?)_.*/', $_GET['action'], $addon_name)) {
                        $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name[1] . '\\Addon';
                        if (class_exists($addonClassName)) {
                            $addonClass = new $addonClassName($this->dbh, $this->config);
                            return $addonClass->runActionUserTemplate();
                        }
                    }
                }
                break;
        }
        return $data;
    }

    public function replaceTags(array $tags = []): void
    {
        global $jscript_last;

        $misc = $this->newMisc();
        $listing_api = $this->newListingApi();
        // Remove tags not found in teh template
        $new_tags = $tags;
        $tags = [];
        foreach ($new_tags as $tag) {
            if (str_contains($this->page, '{' . $tag . '}')) {
                $tags[] = $tag;
            }
        }
        unset($new_tags);
        if (count($tags) > 0) {
            foreach ($tags as $tag) {
                $data = '';
                switch ($tag) {
                    case 'content':
                        $data = $this->replaceUserAction();
                        break;
                    case 'csrf_token':
                        $data = $misc->generateCsrfToken();
                        break;
                    case (preg_match('/templated_search_form(_[0-9]{1,2}|$)/', $tag) ? $tag : false):
                        $_GET['pclass'] = [];
                        $pclass_val = str_replace('templated_search_form', '', $tag);

                        $search = $this->newSearchPage();
                        $pclass_val = str_replace('_', '', $pclass_val);
                        $_GET['pclass'][0] = $pclass_val;
                        $data = $search->createSearchPage(true);
                        break;
                    case 'baseurl':
                        $data = $this->config['baseurl'];
                        break;
                    case 'template_url':
                        $data = $this->config['template_url'];
                        break;
                    case 'pclass_link':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $listing = $this->newListingPages();
                            $data = $listing->pClassLink((int)$_GET['listingID']);
                        }
                        break;
                    case 'addthis_button':
                        $jscript_last .= "\r\n" . '<script type="text/javascript" src="https://s7.addthis.com/js/250/addthis_widget.js"></script>';
                        $data = '<div class="addthis_toolbox addthis_default_style"><a href="https://www.addthis.com/bookmark.php?v=250" class="addthis_button_compact">Share</a></div>';
                        break;
                    case 'load_js':
                        $data = $this->loadJS();
                        break;
                    case 'load_ORjs':
                        $data = $this->loadORJS();
                        break;
                    case 'load_js_last':
                        $data = $jscript_last;
                        break;
                    case 'mobile_full_template_link':
                        $data = $this->renderMobileTemplateTag();
                        break;
                    case 'featured_listings_vertical':
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsVertical();
                        break;
                    case 'featured_listings_horizontal':
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsHorizontal();
                        break;
                    case 'random_listings_vertical':
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsVertical($this->config['num_random_listings'], true);
                        break;
                    case 'random_listings_horizontal':
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsHorizontal($this->config['num_random_listings'], true);
                        break;
                    case ' s_vertical':
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsVertical($this->config['num_latest_listings'], false, 0, true);
                        break;
                    case 'latest_listings_horizontal':
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsHorizontal($this->config['num_latest_listings'], false, 0, true);
                        break;
                    case 'popular_listings_vertical':
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsVertical($this->config['num_popular_listings'], false, 0, false, true);
                        break;
                    case 'popular_listings_horizontal':
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsHorizontal($this->config['num_popular_listings'], false, 0, false, true);
                        break;
                    case (preg_match('/^featured_listings_horizontal_class_([0-9]*)/', $tag, $feat_class) ? $tag : !$tag):
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsHorizontal(0, false, (int)$feat_class[1]);
                        break;
                    case (preg_match('/^featured_listings_vertical_class_([0-9]*)/', $tag, $feat_class) ? $tag : !$tag):
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsVertical(0, false, (int)$feat_class[1]);
                        break;
                    case (preg_match('/^random_listings_horizontal_class_([0-9]*)/', $tag, $feat_class) ? $tag : !$tag):
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsHorizontal($this->config['num_random_listings'], true, (int)$feat_class[1]);
                        break;
                    case (preg_match('/^random_listings_vertical_class_([0-9]*)/', $tag, $feat_class) ? $tag : !$tag):
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsVertical($this->config['num_random_listings'], true, (int)$feat_class[1]);
                        break;
                    case (preg_match('/^latest_listings_horizontal_class_([0-9]*)/', $tag, $feat_class) ? $tag : !$tag):
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsHorizontal($this->config['num_latest_listings'], false, (int)$feat_class[1], true);
                        break;
                    case (preg_match('/^latest_listings_vertical_class_([0-9]*)/', $tag, $feat_class) ? $tag : !$tag):
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsVertical($this->config['num_latest_listings'], false, (int)$feat_class[1], true);
                        break;
                    case (preg_match('/^popular_listings_horizontal_class_([0-9]*)/', $tag, $feat_class) ? $tag : !$tag):
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsHorizontal($this->config['num_popular_listings'], false, (int)$feat_class[1], false, true);
                        break;
                    case (preg_match('/^popular_listings_vertical_class_([0-9]*)/', $tag, $feat_class) ? $tag : !$tag):
                        $listing = $this->newListingPages();
                        $data = $listing->renderFeaturedListingsVertical($this->config['num_popular_listings'], false, (int)$feat_class[1], false, true);
                        break;
                    case 'headline':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $listing = $this->newListingPages();
                            $data = $listing->renderTemplateAreaNoCaption('headline', (int)$_GET['listingID']);
                        }
                        break;
                    case 'listing_images':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsImages((int)$_GET['listingID'], 'yes');
                        }
                        break;
                    case 'listing_images_nocaption':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsImages((int)$_GET['listingID'], 'no');
                        }
                        break;
                    case 'listing_files_select':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $files = $this->newFileHandler();
                            $data = $files->renderFilesSelect((int)$_GET['listingID'], 'listing');
                        }
                        break;
                    case 'files_listing_vertical':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $files = $this->newFileHandler();
                            $data = $files->renderTemplatedFiles((int)$_GET['listingID'], 'listing', 'vertical');
                        }
                        break;
                    case 'files_listing_horizontal':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $files = $this->newFileHandler();
                            $data = $files->renderTemplatedFiles((int)$_GET['listingID'], 'listing', 'horizontal');
                        }
                        break;
                    case 'link_calc':
                        $listing = $this->newListingPages();
                        $data = $listing->createCalcLink();
                        break;
                    case 'link_calc_url':
                        $listing = $this->newListingPages();
                        $data = $listing->createCalcLink('yes');
                        break;
                    case 'link_add_favorites':
                        $listing = $this->newListingPages();
                        $data = $listing->createAddFavoriteLink();
                        break;
                    case 'link_add_favorites_url':
                        $listing = $this->newListingPages();
                        $data = $listing->createAddFavoriteLink('yes');
                        break;
                    case 'link_printer_friendly':
                        $listing = $this->newListingPages();
                        $data = $listing->createPrinterFriendlyLink();
                        break;
                    case 'link_map':
                        $maps = $this->newMaps();
                        $data = $maps->createMapLink();
                        break;
                    case 'link_yahoo_school':
                        $listing = $this->newListingPages();
                        $data = $listing->createYahooSchoolLink();
                        break;
                    case 'link_yahoo_neighborhood':
                        $listing = $this->newListingPages();
                        $data = $listing->createBestPlacesNeighborhoodLink();
                        break;
                    case 'link_printer_friendly_url':
                        $listing = $this->newListingPages();
                        $data = $listing->createPrinterFriendlyLink('yes');
                        break;
                    case 'link_email_friend_url':
                        $listing = $this->newListingPages();
                        $data = $listing->createEmailFriendLink('yes');
                        break;
                    case 'link_email_friend':
                        $listing = $this->newListingPages();
                        $data = $listing->createEmailFriendLink();
                        break;
                    case 'link_map_url':
                        $maps = $this->newMaps();
                        $data = $maps->createMapLink('yes');
                        break;
                    case 'link_yahoo_school_url':
                        $listing = $this->newListingPages();
                        $data = $listing->createYahooSchoolLink('yes');
                        break;
                    case 'link_yahoo_neighborhood_url':
                        $listing = $this->newListingPages();
                        $data = $listing->createBestPlacesNeighborhoodLink('yes');
                        break;
                    case 'contact_agent_link_url':
                        $listing = $this->newListingPages();
                        $data = $listing->contactAgentLink('yes');
                        break;
                    case 'listing_email':
                        //get email address of listing's agent
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $listing = $this->newListingPages();
                            $data = $listing->getListingAgentValue('userdb_emailaddress', (int)$_GET['listingID']);
                        }
                        break;
                    case 'hitcount':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $listing = $this->newListingPages();
                            $data = $listing->hitCount((int)$_GET['listingID']);
                        }
                        break;
                    case 'main_image':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsMainImage((int)$_GET['listingID'], 'yes', 'no');
                        }
                        break;
                    case 'main_image_nodesc':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsMainImage((int)$_GET['listingID'], 'no', 'no');
                        }
                        break;
                    case 'main_image_java':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsMainImage((int)$_GET['listingID'], 'yes', 'yes');
                        }
                        break;
                    case 'main_image_java_nodesc':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsMainImage((int)$_GET['listingID'], 'no', 'yes');
                        }
                        break;
                    case 'listing_images_java':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsImagesJava((int)$_GET['listingID'], 'no');
                        }
                        break;
                    case 'listing_images_java_caption':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsImagesJava((int)$_GET['listingID'], 'yes');
                        }
                        break;
                    case 'listing_images_java_rows':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsImagesJavaRows((int)$_GET['listingID']);
                        }
                        break;
                    case 'listing_images_mouseover_java':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsImagesJava((int)$_GET['listingID'], 'no', 'yes');
                        }
                        break;
                    case 'listing_images_mouseover_java_caption':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsImagesJava((int)$_GET['listingID'], 'yes', 'yes');
                        }
                        break;
                    case 'listing_images_mouseover_java_rows':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $images = $this->newImageHandler();
                            $data = $images->renderListingsImagesJavaRows((int)$_GET['listingID'], 'yes');
                        }
                        break;
                    case 'vtour_button':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $vtour = $this->newVtourHandler();
                            $data = $vtour->rendervtourlink((int)$_GET['listingID']);
                        }
                        break;
                    case 'listingid':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $data = $_GET['listingID'];
                        }
                        break;
                    case 'get_creation_date':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $listing = $this->newListingPages();
                            $data = $listing->getCreationDate((int)$_GET['listingID']);
                        }
                        break;
                    case 'get_featured_raw':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $listing = $this->newListingPages();
                            $data = $listing->getFeatured((int)$_GET['listingID'], 'yes');
                        }
                        break;
                    case 'get_featured':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $listing = $this->newListingPages();
                            $data = $listing->getFeatured((int)$_GET['listingID'], 'no');
                        }
                        break;
                    case 'get_modified_date':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $listing = $this->newListingPages();
                            $data = $listing->getModifiedDate((int)$_GET['listingID']);
                        }
                        break;
                    case 'contact_agent_link':
                        $listing = $this->newListingPages();
                        $data = $listing->contactAgentLink();
                        break;
                    case 'select_language':
                        // $multilingual = new Multilingual();
                        // $data = $multilingual->multilingualSelect();
                        break;
                    case 'company_name':
                        $data = $this->config['company_name'];
                        break;
                    case 'company_location':
                        $data = $this->config['company_location'];
                        break;
                    case 'company_logo':
                        $data = $this->config['company_logo'];
                        break;
                    case 'show_vtour':
                        if (isset($_GET['listingID']) && is_numeric($_GET['listingID'])) {
                            $vtour = $this->newVtourHandler();
                            $data = $vtour->showVtour((int)$_GET['listingID'], false);
                        } else {
                            $data = 'No Listing ID';
                        }
                        break;
                    case 'charset':
                        $data = $this->config['charset'];
                        break;
                    case 'link_edit_listing':
                        $listing = $this->newListingPages();
                        $data = $listing->editListingLink();
                        break;
                    case 'link_edit_listing_url':
                        $listing = $this->newListingPages();
                        $data = $listing->editListingLink('yes');
                        break;
                    case 'template_select':
                        $data = $this->templateSelector();
                        break;
                    case 'money_sign':
                        $data = $this->config['money_sign'];
                        break;
                    case 'curley_open':
                        $data = '{';
                        break;
                    case 'curley_close':
                        $data = '}';
                        break;
                    case 'powered_by_tag':
                        $data = '<a href="https://gitlab.com/appsbytherealryanbonham/open-realty" title="Powered By Open-Realty®"><img id="or_poweredby_logo" style="display:block;width:150px;height:39px; clip: auto; clip-path: none; z-index: auto; transform: none;" src="' . $this->config['baseurl'] . '/index.php?action=powered_by" alt="Powered By Open-Realty®" /></a>';
                        break;
                    case (preg_match('/getvar_([^{}]*?)/', $tag) ? $tag : false):
                        $getvar = str_replace('getvar_', '', $tag);
                        if (!empty($_GET[$getvar])) {
                            if (is_array($_GET[$getvar])) {
                                if (isset($_GET[$getvar][0]) && is_string($_GET[$getvar][0])) {
                                    $data = htmlentities($_GET[$getvar][0]);
                                }
                            } else {
                                $data = htmlentities($_GET[$getvar]);
                            }
                        }
                        break;
                    case (preg_match('/url_blog_cat_([0-9]*)/', $tag) ? $tag : false):
                        /** @var string $cat_id */
                        $cat_id = str_replace('url_blog_cat_', '', $tag);
                        $data = $this->magicURIGenerator('blog_cat', $cat_id, true);
                        break;
                    case (preg_match('/blog_cat_title_([0-9]*)/', $tag) ? $tag : false):
                        /** @var string $cat_id */
                        $cat_id = str_replace('blog_cat_title_', '', $tag);

                        $blog_functions = $this->newBlogFunctions();
                        $data = $blog_functions->getBlogCategoryName((int)$cat_id);
                        break;
                    case (preg_match('/pclass_name_([0-9]{1,2})/', $tag) ? $tag : false):
                        /** @var string $pclass */
                        $pclass = str_replace('pclass_name_', '', $tag);
                        $data = $this->getPClassName((int)$pclass);
                        break;
                    case 'pclass_names_ul':
                        $pclass_api = $this->newPClassApi();
                        try {
                            $class_metadata = $pclass_api->metadata();
                        } catch (Exception $e) {
                            $misc->logErrorAndDie($e->getMessage());
                        }
                        $keys = array_keys($class_metadata['metadata']);
                        $output = '<ul>';
                        foreach ($keys as $class_id) {
                            $output .= '<li><a href="#">' . $class_metadata['metadata'][$class_id]['name'] . '</a></li>';
                        }
                        $output .= '</ul>';
                        $data = $output;
                        break;
                    case 'pclass_names_searchlinks':
                        $pclass_api = $this->newPClassApi();
                        try {
                            $class_metadata = $pclass_api->metadata();
                        } catch (Exception $e) {
                            $misc->logErrorAndDie($e->getMessage());
                        }
                        $keys = array_keys($class_metadata['metadata']);
                        $output = '<ul>';
                        foreach ($keys as $class_id) {
                            $output .= '<li><a href="' . $this->config['baseurl'] . '/index.php?action=search_step_2&amp;pclass%5B%5D=' . $class_id . '" title="' . $class_metadata['metadata'][$class_id]['name'] . '">' . $class_metadata['metadata'][$class_id]['name'] . '</a></li>';
                        }
                        $output .= '</ul>';
                        $data = $output;
                        break;
                    case 'current_language':
                        $data = $_SESSION['language_template'] ?? $this->config['lang'];
                        break;
                    case 'active_listing_count':
                        try {
                            $result = $listing_api->search(['parameters' => [], 'limit' => 0, 'offset' => 0, 'count_only' => true]);
                            $data = $result['listing_count'];
                        } catch (Exception) {
                        }

                        break;
                    case (preg_match('/^active_listing_count_pclass_([0-9]*)/', $tag, $pclass) ? $tag : !$tag):
                        try {
                            $result = $listing_api->search(['parameters' => ['pclass' => [$pclass[1]]], 'limit' => 0, 'offset' => 0, 'count_only' => true]);
                            $data = $result['listing_count'];
                        } catch (Exception) {
                        }

                        break;
                    case (preg_match('/^listing_stat_([a-z]*)_field_([^{}]*?)_value_pclass_([0-9]*)/', $tag, $args) ? $tag : !$tag):
                        try {
                            $result = $listing_api->getStatistics(['function' => $args[1], 'field_name' => $args[2], 'pclass' => [(int)$args[3]], 'format' => true]);
                        } catch (Exception) {
                        }
                        if (isset($result[$args[1]])) {
                            $data = $result[$args[1]];
                        }
                        break;
                    case (preg_match('/^listing_stat_([a-z]*)_field_([^{}]*?)_value/', $tag, $args) ? $tag : !$tag):
                        try {
                            $result = $listing_api->getStatistics(['function' => $args[1], 'field_name' => $args[2], 'format' => true]);
                        } catch (Exception) {
                        }
                        if (isset($result[$args[1]])) {
                            $data = $result[$args[1]];
                        }
                        break;
                    case (preg_match('/^render_menu_([0-9]*)/', $tag, $menu) ? $tag : !$tag):
                        $menu_editor = $this->newMenuEditor();
                        $data = $menu_editor->renderMenu((int)$menu[1]);
                        break;
                    default:
                        if (preg_match('/^addon_(.*?)_.*/', $tag, $addon_name)) {
                            $addonClassName = '\\OpenRealty\\Addons\\' . $addon_name[1] . '\\Addon';
                            if (class_exists($addonClassName)) {
                                $addonClass = new $addonClassName($this->dbh, $this->config);
                                $data = $addonClass->runTemplateUserFields($tag);
                            }
                        }
                        break;
                }
                if (is_string($data) || is_numeric($data) || is_null($data)) {
                    $this->page = str_replace('{' . $tag . '}', (string)$data, $this->page);
                }
            }
        }
        unset($tags);
        unset($tag);
    }

    public function templateSelector(): string
    {
        $misc = $this->newMisc();
        $display = '';
        if ($this->config['allow_template_change']) {
            $display .= '<form class="template_selector" id="template_select" action="" method="post">';
            $display .= '<input type="hidden" name="token" value="' . $misc->generateCsrfToken() . '" />';
            $display .= '<fieldset>';
            $display .= '<select id="select_users_template" name="select_users_template" onchange="this.form.submit();">';

            $template_directory = $this->config['basepath'] . '/template';
            $template = opendir($template_directory);
            if (!$template) {
                die('fail to open');
            }
            while (false !== ($file = readdir($template))) {
                if ($file != '.' && $file != '..' && $file != '.svn') {
                    if (is_dir($template_directory . '/' . $file)) {
                        $display .= '<option value="' . $file . '"';
                        if ($this->config['template'] == $file) {
                            $display .= ' selected="selected"';
                        }
                        $display .= '>' . $file . '</option>';
                    }
                }
            }
            $display .= '</select>';
            $display .= '</fieldset>';
            $display .= '</form>';
            closedir($template);
        }
        return $display;
    }

    public function getPClassName(int $pclass): string
    {
        global $ORconn;

        $misc = $this->newMisc();
        $sql = 'SELECT class_name
		FROM ' . $this->config['table_prefix'] . 'class
		WHERE class_id = ' . $pclass;
        $recordSet = $ORconn->Execute($sql);
        if (is_bool($recordSet)) {
            $misc->logErrorAndDie($sql);
        }
        return (string)$recordSet->fields('class_name');
    }
}
