<?php

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Hooks;
use OpenRealty\Login;
use OpenRealty\Misc;
use OpenRealty\PageUser;

/**
 * This is the Blog  API, it contains all api calls for creating and retrieving Blog data.
 *
 **/
class BlogApi extends BaseCommand
{
    /**
     * List of fields in the main table, others will be read of dbelements.
     *
     * @var string[]
     */
    protected array $OR_INT_FIELDS = ['blogmain_id', 'userdb_id', 'blogmain_title', 'blog_seotitle', 'blogmain_date', 'blogmain_full', 'blogmain_description', 'blogmain_keywords', 'blogmain_published'];

    /*
        protected $read_filter = array(
            'fields' =>array(
                ‘filter’ => FILTER_SANITIZE_STRING
            )
        );

        protected $read_filter = array(
            'blogmain_id' => FILTER_VALIDATE_INT,
            'userdb_id' => FILTER_VALIDATE_INT,
            'blogmain_title' => FILTER_SANITIZE_STRING,
            'blog_seotitle' => FILTER_SANITIZE_STRING,
            'blogmain_date' => FILTER_VALIDATE_INT,
            'blogmain_full' => FILTER_SANITIZE_STRING,
            'blogmain_description' => FILTER_SANITIZE_STRING,
            'blogmain_keywords' => FILTER_SANITIZE_STRING,
            'blogmain_published' => FILTER_VALIDATE_INT
        );

    */

    /**
     * This API Command searches the users
     *
     * @param array{
     *          parameters: array<string, mixed>,
     *          sortby?: string[],
     *          sorttype?: string[],
     *          offset?: integer,
     *          limit?: integer,
     *          count_only?: boolean
     *        } $data Array of data to search blogs by.
     *
     *  <ul>
     *      <li>$data['parameters'] - This is a REQUIRED array of the fields and the values we are searching for.</li>
     *      <li>$data['sortby'] - This is an optional array of fields to sort by.</li>
     *      <li>$data['sorttype'] - This is an optional array of sort types (ASC/DESC) to sort the sortby fields
     *      by.</li>
     *      <li>$data['offset'] - This is an optional integer of the number of users to offset the search by. To use
     *      offset you must set a limit.</li>
     *      <li>$data['limit'] - This is an optional integer of the number of users to limit the search by. 0 or unset
     *      will return all users.</li>
     *      <li>$data['count_only'] - This is an optional integer flag 1/0, where 1 returns a record count only,
     *      defaults to 0 if not set. Usefull if doing limit/offset search for pagenation to get the inital full record
     *      count..</li>
     *  </ul>.
     *
     * @return array{
     *          blog_count:int,
     *          blogs:int[],
     *          info:array{
     *              process_time:string,
     *              query_time: string,
     *              total_time:string
     *          },
     *          sortby:string[],
     *          sorttype: string[],
     *          limit?: int,
     *          offset?:int
     *         } Return value will be an array which will contain the following paramaters.
     *  <ul>
     *  <li>[blog_count] - Number of blogs that matched search.</li>
     *  <li>[blogs] - Array of IDs for the blogs that matched search.</li>
     *  <li>[info] - Array of performance data</li>
     *  <li>[sortby] - Array that search was sorted by</li>
     *  <li>[sorttype] - Array of search directions</li>
     *  <li>[limit] - Limit that was applied when search was run</li>
     *  <li>[offset] - Offset that was applied when search was run</li>
     * </ul>.
     * @throws Exception Any fatal errors will result in an Exception with the error msg.
     */
    public function search(array $data): array
    {
        //$DEBUG_SQL = false;
        $misc = new Misc($this->dbh, $this->config);
        $login = new Login($this->dbh, $this->config);
        $start_time = $misc->getmicrotime();
        if (!isset($data['count_only'])) {
            $data['count_only'] = false;
        }
        $searchresultSQL = '';
        // Set Default Search Options
        $tablelist = [];
        $string_where_clause = '';
        $string_where_clause_nosort = '';
        $login_status = $login->verifyPriv('can_access_blog_manager');

        if ($login_status !== true || !isset($data['parameters']['blogmain_published'])) {
            //If we are not an agent only show active agents, or if user did not specify show only actives by default.
            $data['parameters']['blogmain_published'] = 1;
        }
        // //check to see teh publishing status of this user
        // $pub_status = $this->getPublisherStatus($_SESSION['userID']);

        // if ($pub_status === 4 && $_SESSION['userID'] != 1) {
        //     $admin_not_agent = true;
        // } else {
        //     $admin_not_agent = false;
        // }

        $dbParamaters = [];
        //Loop through search paramaters
        foreach ($data['parameters'] as $k => $v) {
            if ($v == '') {
                unset($data['parameters'][$k]);
                continue;
            }

            //Search blog by blogmain_id
            if ($k == 'blogmain_id' && is_string($v)) {
                $blogmain_id = explode(',', $v);
                $i = 0;
                if ($searchresultSQL != '') {
                    $searchresultSQL .= ' AND ';
                }
                foreach ($blogmain_id as $id) {
                    $id = intval($id);
                    $dbParamaters[':blogmain_id' . $i] = $id;
                    if ($i == 0) {
                        $searchresultSQL .= '((' . $this->config['table_prefix'] . 'blogmain.blogmain_id = :blogmain_id$i)';
                    } else {
                        $searchresultSQL .= ' OR (' . $this->config['table_prefix'] . 'blogmain.blogmain_id = :blogmain_id$i)';
                    }
                    $i++;
                }
                $searchresultSQL .= ')';
            } elseif ($k == 'userdb_id') {
                if ($v != 'any') {
                    if (is_array($v)) {
                        $sstring = '';
                        foreach ($v as $u) {
                            $u = intval($u);
                            $dbParamaters[':userdb_id' . $u] = $u;
                            if (empty($sstring)) {
                                $sstring .= $this->config['table_prefix'] . 'blogmain.userdb_id = :userdb_id' . $u;
                            } else {
                                $sstring .= ' OR ' . $this->config['table_prefix'] . 'blogmain.userdb_id = :userdb_id' . $u;
                            }
                        }
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                        $searchresultSQL .= '(' . $sstring . ')';
                    } else {
                        $sql_v = intval($v);
                        $dbParamaters[':userdb_id'] = $sql_v;
                        if ($searchresultSQL != '') {
                            $searchresultSQL .= ' AND ';
                        }
                        $searchresultSQL .= '(' . $this->config['table_prefix'] . 'blogmain.userdb_id = :userdb_id)';
                    }
                }
            } elseif ($k == 'blogmain_title' && is_string($v)) {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $dbParamaters[':blogmain_title'] = "%{$v}%";
                $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_title LIKE :blogmain_title)';
                $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_title LIKE :blogmain_title)';
            } elseif ($k == 'blogmain_full' && is_string($v)) {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $dbParamaters[':blogmain_full'] = "%{$v}%";
                $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_full LIKE :blogmain_full)';
                $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_full LIKE :blogmain_full)';
            } elseif ($k == 'blogmain_description' && is_string($v)) {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $dbParamaters[':blogmain_description'] = "%{$v}%";
                $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_description LIKE :blogmain_description)';
                $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_description LIKE :blogmain_description)';
            } elseif ($k == 'blogmain_keywords' && is_string($v)) {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                $dbParamaters[':blogmain_description'] = "%{$v}%";
                $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_keywords LIKE :blogmain_description)';
                $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_keywords LIKE :blogmain_description)';
            } elseif ($k == 'blogmain_published') {
                if ($string_where_clause != '') {
                    $string_where_clause .= ' AND ';
                }
                if ($string_where_clause_nosort != '') {
                    $string_where_clause_nosort .= ' AND ';
                }
                // Get any blog regardless of publish status
                if ($v == 'any') {
                    $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 0 
												OR ' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 1 
												OR ' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 2
											)';
                    $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 0
														OR ' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 1 
														OR ' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 2
													)';
                } else {
                    $v = (int)$v;
                    if ($v == 0) {
                        //Draft
                        $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 0)';
                        $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 0)';
                    } elseif ($v == 1) {
                        //Live
                        $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 1)';
                        $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 1)';
                    } elseif ($v == 2) {
                        // Pending Review
                        $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 2)';
                        $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 2)';
                    } else {
                        $string_where_clause .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 1)';
                        $string_where_clause_nosort .= '(' . $this->config['table_prefix'] . 'blogmain.blogmain_published = 1)';
                    }
                }
            } elseif ($k == 'blog_creation_date_greater') {
                // creation date related searches
                //  date_equals is already handled via 'blogmain_date' => xxx
                $safe_v = intval($v);
                if ($safe_v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbParamaters[':blog_creation_date_greater'] = $safe_v;
                    $searchresultSQL .= ' blogmain_date > :blog_creation_date_greater';
                }
            } elseif ($k == 'blog_creation_date_less') {
                $safe_v = intval($v);
                if ($safe_v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbParamaters[':blog_creation_date_less'] = $safe_v;
                    $searchresultSQL .= ' blogmain_date < :blog_creation_date_less';
                }
            } elseif ($k == 'blog_creation_date_equal_days') {
                $safe_v = intval($v);
                if ($safe_v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbParamaters[':blog_creation_date_equal_days'] = $safe_v;
                    $searchresultSQL .= ' blogmain_date = :blog_creation_date_equal_days';
                }
            } elseif ($k == 'blog_creation_date_greater_days') {
                $safe_v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbParamaters[':blog_creation_date_greater_days'] = $safe_v;
                    $searchresultSQL .= ' blogmain_date > :blog_creation_date_greater_days';
                }
            } elseif ($k == 'blog_creation_date_less_days') {
                $safe_v = intval($v);
                if ($v > 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbParamaters[':blog_creation_date_less_days'] = $safe_v;
                    $searchresultSQL .= ' blogmain_date < :blog_creation_date_less_days';
                }
            } elseif ($k == 'blog_categories' && is_array($v)) {
                $use = false;
                $comma_separated = implode(' ', $v);
                if (trim($comma_separated) != '') {
                    $use = true;
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                }
                if ($use === true) {
                    $searchresultSQL .= ' (';
                    $vitem_count = 0;
                    foreach ($v as $vitem) {
                        if ($vitem != '') {
                            $dbParamaters[':category_id' . $vitem_count] = $vitem;
                            if ($vitem_count != 0) {
                                $searchresultSQL .= " OR category_id = :category_id$vitem_count";
                            } else {
                                $searchresultSQL .= " category_id = :category_id$vitem_count";
                            }
                            $vitem_count++;
                        }
                    }
                    $searchresultSQL .= ')';
                    $tablelist[] = $this->config['table_prefix_no_lang'] . 'blogcategory_relation';
                }
            } elseif ($k == 'blog_post_tags' && is_array($v)) {
                $use = false;
                $comma_separated = implode(' ', $v);
                if (trim($comma_separated) != '') {
                    $use = true;
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                }
                if ($use === true) {
                    $searchresultSQL .= ' (';
                    $vitem_count = 0;
                    foreach ($v as $vitem) {
                        $dbParamaters[':tag_id' . $vitem_count] = $vitem;
                        if ($vitem != '') {
                            if ($vitem_count != 0) {
                                $searchresultSQL .= " OR tag_id = :tag_id$vitem_count";
                            } else {
                                $searchresultSQL .= " tag_id = :tag_id$vitem_count";
                            }
                            $vitem_count++;
                        }
                    }
                    $searchresultSQL .= ')';
                    $tablelist[] = $this->config['table_prefix_no_lang'] . 'blogtag_relation';
                }
            } elseif ($k == 'blog_comment_count_equal') {
                //this one is non-funtional. I need to work out the joins if it is even possible.
                $safe_v = intval($v);
                if ($safe_v >= 0) {
                    if ($searchresultSQL != '') {
                        $searchresultSQL .= ' AND ';
                    }
                    $dbParamaters[':blog_comment_count_equal'] = $safe_v;
                    $searchresultSQL .= " blogcomments_moderated = :blog_comment_count_equal";
                    $tablelist[] = $this->config['table_prefix'] . 'blogcomments';
                }
            }
        }

        // Handle Sorting
        // sort the users
        // this is the main SQL that grabs the users
        // basic sort by title..
        $sortby_array = [];
        $sorttype_array = [];
        //Set array
        if (isset($data['sortby']) && !empty($data['sortby'])) {
            $sortby_array = $data['sortby'];
        }
        if (isset($data['sorttype']) && !empty($data['sorttype'])) {
            $sorttype_array = $data['sorttype'];
        }
        $sort_text = '';
        $order_text = '';
        $select_fields = [];
        $tablelist_nosort = $tablelist;
        $sort_count = count($sortby_array);
        for ($x = 0; $x < $sort_count; $x++) {
            if (!isset($sorttype_array[$x])) {
                $sorttype_array[$x] = '';
            } elseif ($sorttype_array[$x] != 'ASC' && $sorttype_array[$x] != 'DESC') {
                $sorttype_array[$x] = '';
            }

            if ($sortby_array[$x] == 'blogmain_id') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY blogmain_id ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',blogmain_id ' . $sorttype_array[$x];
                }
            } elseif ($sortby_array[$x] == 'userdb_id') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY userdb_id ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',userdb_id ' . $sorttype_array[$x];
                }
                $select_fields[] = 'userdb_id';
            } elseif ($sortby_array[$x] == 'blogmain_title') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY blogmain_title ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',blogmain_title ' . $sorttype_array[$x];
                }
                $select_fields[] = 'blogmain_title';
            } elseif ($sortby_array[$x] == 'blogmain_date') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY blogmain_date ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',blogmain_date ' . $sorttype_array[$x];
                }
                $select_fields[] = 'blogmain_date';
            } elseif ($sortby_array[$x] == 'blogmain_published') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY blogmain_published ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',blogmain_published ' . $sorttype_array[$x];
                }
                $select_fields[] = 'blogmain_published';
            } elseif ($sortby_array[$x] == 'random') {
                if ($order_text == '') {
                    $order_text .= 'ORDER BY rand() ' . $sorttype_array[$x];
                } else {
                    $order_text .= ',rand() ' . $sorttype_array[$x];
                }
            }
        }

        //        if ($DEBUG_SQL) {
        //            echo '<strong>Sort Type SQL:</strong> ' . $sql_sort_type . '<br />';
        //            echo '<strong>Sort Text:</strong> ' . $sort_text . '<br />';
        //            echo '<strong>Order Text:</strong> ' . $order_text . '<br />';
        //        }


        $select_fields_text = '';
        if (count($select_fields) > 0) {
            $select_fields_text = ',' . join(",", $select_fields);
        }

        //$guidestring_with_sort = $guidestring_with_sort . $guidestring;
        // End of Sort
        $arrayLength = count($tablelist);
        //        if ($DEBUG_SQL) {
        //            echo '<strong>Table List Array Length:</strong> ' . $arrayLength . '<br />';
        //        }
        $string_table_list = '';
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list .= ' ,' . $tablelist[$i];
        }
        $arrayLength = count($tablelist_nosort);
        $string_table_list_no_sort = '';
        for ($i = 0; $i < $arrayLength; $i++) {
            $string_table_list_no_sort .= ' ,' . $tablelist[$i];
        }

        //        if ($DEBUG_SQL) {
        //            echo '<strong>Table List String:</strong> ' . $string_table_list . '<br />';
        //        }
        $arrayLength = count($tablelist);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($string_where_clause != '') {
                $string_where_clause .= ' AND ';
            }
            $string_where_clause .= ' (' . $this->config['table_prefix'] . 'blogmain.blogmain_id = ' . $tablelist[$i] . '.blogmain_id)';
        }
        $arrayLength = count($tablelist_nosort);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($string_where_clause_nosort != '') {
                $string_where_clause_nosort .= ' AND ';
            }
            $string_where_clause_nosort .= ' (' . $this->config['table_prefix'] . 'blogmain.blogmain_id = ' . $tablelist[$i] . '.blogmain_id)';
        }

        $searchSQL = 'SELECT distinct(' . $this->config['table_prefix'] . 'blogmain.blogmain_id) ' . $select_fields_text . '
					FROM ' . $this->config['table_prefix'] . 'blogmain ' . $string_table_list . '
					WHERE ' . $string_where_clause;

        $searchSQLCount = 'SELECT COUNT(distinct(' . $this->config['table_prefix'] . 'blogmain.blogmain_id)) as total_blogs
					FROM ' . $this->config['table_prefix'] . 'blogmain ' . $string_table_list_no_sort . '
					WHERE ' . $string_where_clause_nosort;

        if ($searchresultSQL != '') {
            $searchSQL .= ' AND ' . $searchresultSQL;
            $searchSQLCount .= ' AND ' . $searchresultSQL;
        }

        $sql = $searchSQL . ' ' . $sort_text . ' ' . $order_text;
        if ($data['count_only']) {
            $sql = $searchSQLCount;
        }
        if (isset($data['limit']) && isset($data['offset']) && $data['limit'] > 0) {
            $sql . ' LIMIT :offset, :limit';
            $dbParamaters[':limit'] = $data['limit'];
            $dbParamaters[':offset'] = $data['offset'];
        }
        // We now have a complete SQL Query. Now grab the results
        $process_time = $misc->getmicrotime();
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        $recordSet = [];
        try {
            $stmt->execute($dbParamaters);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $query_time = $misc->getmicrotime();
        $query_time = $query_time - $process_time;
        $process_time = $process_time - $start_time;
        //        if ($DEBUG_SQL) {
        //            echo '<strong>Search Query:</strong> ' . $sql . '<br />';
        //        }
        if ($data['count_only']) {
            $blog_count = (int)$stmt->fetchColumn();
        } else {
            $recordSet = $stmt->fetchAll();
            $blog_count = count($recordSet);
        }

        $blogs_found = [];

        if (!$data['count_only']) {
            foreach ($recordSet as $record) {
                $blogs_found[] = (int)$record['blogmain_id'];
            }
        }
        $total_time = $misc->getmicrotime();
        $total_time = $total_time - $start_time;
        $info = [];
        $info['process_time'] = sprintf('%.3f', $process_time);
        $info['query_time'] = sprintf('%.3f', $query_time);
        $info['total_time'] = sprintf('%.3f', $total_time);
        return [
            'blog_count' => $blog_count,
            'blogs' => $blogs_found,
            'info' => $info,
            'sortby' => $sortby_array,
            'sorttype' => $sorttype_array,
            'limit' => $data['limit'] ?? 0,
            'offset' => $data['offset'] ?? 0,
        ];
    }


    /**
     * This API Command reads user info
     *
     * @param array{
     *          blog_id: integer,
     *          fields?: array
     *        } $data $data expects an array containing the following array keys.
     *
     *  <ul>
     *      <li>$data['blog_+id'] - This is the Blog ID that we are updating.</li>
     *      <li>$data['fields'] - This is an optional array of fields to retrieve, if left empty or not passed all
     *      fields will be retrieved.</li>
     *  </ul>
     *
     * @return array{blog: array{
     *          blogmain_id?: integer,
     *          userdb_id?: integer,
     *          blogmain_published?: integer,
     *          blogmain_title?: string,
     *          blog_seotitle?: string,
     *          blogmain_full?: string,
     *          blogmain_date?: string,
     *          blogmain_description?: string,
     *          blogmain_keywords?:string,
     *          blog_author_firstname?: string,
     *          blog_author_lastname?: string,
     *          blog_categories?: array<int, string>,
     *          blog_post_tags?: array<integer, array{ tag_name: string, tag_seoname: string, tag_description: string,
     *          tag_link: string }>, blog_comment_count?: int, blog_url?: mixed, blog_comments?: array<int, array{
     *          userdb_id?: integer, blogcomments_id?: integer, blogcomments_timestamp?: integer, blogcomments_text?:
     *          string, blogcomments_moderated?: boolean
     *          }>
     *         }}
     *
     * @throws Exception Any fatal errors will result in an Exception with the error msg.
     **/
    public function read(array $data): array
    {
        $login = new Login($this->dbh, $this->config);
        $page = new PageUser($this->dbh, $this->config);
        $security = $login->verifyPriv('can_access_blog_manager');

        //Check that required settings were passed
        if (!isset($data['blog_id'])) {
            throw new Exception('blog_id: correct_parameter_not_passed');
        }
        $blog_id = $data['blog_id'];
        $fields = [];
        //If no fields were passed make an empty array to save checking for if !isset later
        if (isset($data['fields'])) {
            $fields = $data['fields'];
        }

        //This array will hold our blog data
        $blog_data = [];

        if (!$security) {
            $suffix_published = ' AND blogmain_published = 1';
            $suffix_moderated = ' AND blogcomments_moderated = 1';
        } else {
            $suffix_published = '';
            $suffix_moderated = '';
        }

        //Get Base user information
        if (empty($fields)) {
            $sql = 'SELECT ' . implode(',', $this->OR_INT_FIELDS) . ' 
					FROM ' . $this->config['table_prefix'] . 'blogmain 
					WHERE blogmain_id = :blog_id 
					' . $suffix_published;

            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute([':blog_id' => $blog_id]);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->read', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();

            if (count($recordSet) == 1) {
                $blog_data['blogmain_id'] = (int)$recordSet[0]['blogmain_id'];
                $blog_data['userdb_id'] = (int)$recordSet[0]['userdb_id'];
                $blog_data['blogmain_published'] = (int)$recordSet[0]['blogmain_published'];
                $blog_data['blogmain_title'] = (string)$recordSet[0]['blogmain_title'];
                $blog_data['blog_seotitle'] = (string)$recordSet[0]['blog_seotitle'];
                $blog_data['blogmain_date'] = (string)$recordSet[0]['blogmain_date'];
                $blog_data['blogmain_full'] = (string)$recordSet[0]['blogmain_full'];
                $blog_data['blogmain_description'] = (string)$recordSet[0]['blogmain_description'];
                $blog_data['blogmain_keywords'] = (string)$recordSet[0]['blogmain_keywords'];
            } else {
                throw new Exception('Unexpected number of records returned.');
            }


            //Get Name of Blog Author
            $user_api = new UserApi($this->dbh, $this->config);

            $result = $user_api->read([
                'user_id' => $blog_data['userdb_id'],
                'resource' => 'agent',
                'fields' => [
                    'userdb_user_first_name',
                    'userdb_user_last_name',
                ],
            ]);


            $blog_data['blog_author_firstname'] = (string)$result['user']['userdb_user_first_name'];
            $blog_data['blog_author_lastname'] = (string)$result['user']['userdb_user_last_name'];
            $blog_data['blog_categories'] = $this->getBlogCategoryNames($blog_id);
            $blog_data['blog_post_tags'] = $this->getBlogTagAssignments($blog_id);

            //get comment count and comments
            $sql = 'SELECT userdb_id,blogcomments_timestamp, blogcomments_text, blogcomments_id, blogcomments_moderated 
					FROM ' . $this->config['table_prefix'] . "blogcomments 
					WHERE blogmain_id = :blog_id  $suffix_moderated ORDER BY blogcomments_timestamp ASC;";
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            try {
                $stmt->execute([':blog_id' => $blog_id]);
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $recordSet = $stmt->fetchAll();

            $blog_data['blog_url'] = $page->magicURIGenerator('blog', (string)$blog_id, true);
            $blog_data['blog_comment_count'] = count($recordSet);

            foreach ($recordSet as $record) {
                $cid = (int)$record['blogcomments_id'];
                $blog_data['blog_comments'][$cid]['userdb_id'] = (int)$record['userdb_id'];
                $blog_data['blog_comments'][$cid]['blogcomments_id'] = $cid;
                $blog_data['blog_comments'][$cid]['blogcomments_timestamp'] = (int)$record['blogcomments_timestamp'];
                $blog_data['blog_comments'][$cid]['blogcomments_text'] = (string)$record['blogcomments_text'];
                $blog_data['blog_comments'][$cid]['blogcomments_moderated'] = (bool)$record['blogcomments_moderated'];
            }
        } else {
            $core_fields = array_intersect($this->OR_INT_FIELDS, $fields);
            $noncore_fields = array_diff($fields, $this->OR_INT_FIELDS);

            if (!empty($core_fields)) {
                $sql = 'SELECT ' . implode(',', $core_fields) . ' 
						FROM ' . $this->config['table_prefix'] . "blogmain 
						WHERE blogmain_id = :bolog_id $suffix_published";

                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                try {
                    $stmt->execute([':blog_id' => $blog_id]);
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    $log_api = new LogApi($this->dbh, $this->config);
                    $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                    throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                }
                $recordSet = $stmt->fetchAll();

                if (count($recordSet) == 1) {
                    if (isset($core_fields['blogmain_id'])) {
                        $blog_data['blogmain_id'] = (int)$recordSet[0]['blogmain_id'];
                    }
                    if (isset($core_fields['userdb_id'])) {
                        $blog_data['userdb_id'] = (int)$recordSet[0]['userdb_id'];
                    }
                    if (isset($core_fields['blogmain_published'])) {
                        $blog_data['blogmain_published'] = (int)$recordSet[0]['blogmain_published'];
                    }
                    if (isset($core_fields['blogmain_title'])) {
                        $blog_data['blogmain_title'] = (string)$recordSet[0]['blogmain_title'];
                    }
                    if (isset($core_fields['blog_seotitle'])) {
                        $blog_data['blog_seotitle'] = (string)$recordSet[0]['blog_seotitle'];
                    }
                    if (isset($core_fields['blogmain_date'])) {
                        $blog_data['blogmain_date'] = (string)$recordSet[0]['blogmain_date'];
                    }
                    if (isset($core_fields['blogmain_full'])) {
                        $blog_data['blogmain_full'] = (string)$recordSet[0]['blogmain_full'];
                    }
                    if (isset($core_fields['blogmain_description'])) {
                        $blog_data['blogmain_description'] = (string)$recordSet[0]['blogmain_description'];
                    }
                    if (isset($core_fields['blogmain_keywords'])) {
                        $blog_data['blogmain_keywords'] = (string)$recordSet[0]['blogmain_keywords'];
                    }
                }
            }
            if (!empty($noncore_fields)) {
                if (in_array('blog_url', $noncore_fields)) {
                    $blog_data['blog_url'] = $page->magicURIGenerator('blog', (string)$blog_id, true);
                }
                // if either of these files is set, do stuff.
                if (in_array('blog_comment_count', $noncore_fields) || in_array('blog_comments', $noncore_fields)) {
                    //if (isset ($noncore_fields['blog_comment_count']) || isset ($noncore_fields['comments']) ) {

                    //get comment count and comments
                    $sql = 'SELECT userdb_id,blogcomments_timestamp, blogcomments_text, blogcomments_id, blogcomments_moderated 
							FROM ' . $this->config['table_prefix'] . 'blogcomments 
							WHERE blogmain_id = :blog_id 
							AND blogcomments_moderated = 1 
							ORDER BY blogcomments_timestamp ASC;';
                    $stmt = $this->dbh->prepare($sql);
                    if (is_bool($stmt)) {
                        throw new Exception('Prepairing Statement Failed');
                    }
                    try {
                        $stmt->execute([':blog_id' => $blog_id]);
                    } catch (Exception $e) {
                        $error = $e->getMessage();
                        $log_api = new LogApi($this->dbh, $this->config);
                        $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                        throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                    }
                    $recordSet = $stmt->fetchAll();

                    $blog_data['blog_comment_count'] = count($recordSet);

                    // if blog_comments is set generate that stuff.
                    if (in_array('blog_comments', $noncore_fields)) {
                        foreach ($recordSet as $record) {
                            $cid = (int)$record['blogcomments_id'];
                            $blog_data['blog_comments'][$cid]['userdb_id'] = (int)$record['userdb_id'];
                            $blog_data['blog_comments'][$cid]['blogcomments_id'] = $cid;
                            $blog_data['blog_comments'][$cid]['blogcomments_timestamp'] = (int)$record['blogcomments_timestamp'];
                            $blog_data['blog_comments'][$cid]['blogcomments_text'] = (string)$record['blogcomments_text'];
                            $blog_data['blog_comments'][$cid]['blogcomments_moderated'] = (bool)$record['blogcomments_moderated'];
                        }
                    }
                }
                if (in_array('blog_author_firstname', $noncore_fields) || in_array('blog_author_lastname', $noncore_fields)) {
                    //Get Name of Blog Author
                    $user_api = new UserApi($this->dbh, $this->config);

                    if (!isset($blog_data['userdb_id'])) {
                        $sql = 'SELECT userdb_id 
						FROM ' . $this->config['table_prefix'] . "blogmain 
						WHERE blogmain_id = :blog_id $suffix_published";

                        $stmt = $this->dbh->prepare($sql);
                        if (is_bool($stmt)) {
                            throw new Exception('Prepairing Statement Failed');
                        }
                        try {
                            $stmt->execute([':blog_id' => $blog_id]);
                        } catch (Exception $e) {
                            $error = $e->getMessage();
                            $log_api = new LogApi($this->dbh, $this->config);
                            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
                        }
                        $recordSet = $stmt->fetchAll();
                        if (count($recordSet) == 1) {
                            $user_id = (int)$recordSet[0]['userdb_id'];
                        } else {
                            throw new Exception('Unexpected number of records returned.');
                        }
                    } else {
                        $user_id = $blog_data['userdb_id'];
                    }

                    $result = $user_api->read([
                        'user_id' => $user_id,
                        'resource' => 'agent',
                        'fields' => [
                            'userdb_user_first_name',
                            'userdb_user_last_name',
                        ],
                    ]);


                    if (in_array('blog_author_firstname', $noncore_fields)) {
                        $blog_data['blog_author_firstname'] = (string)$result['user']['userdb_user_first_name'];
                    }
                    if (in_array('blog_author_lastname', $noncore_fields)) {
                        $blog_data['blog_author_lastname'] = (string)$result['user']['userdb_user_last_name'];
                    }
                }
                if (in_array('blog_categories', $noncore_fields)) {
                    $blog_data['blog_categories'] = $this->getBlogCategoryNames($blog_id);
                }
                if (in_array('blog_post_tags', $noncore_fields)) {
                    $blog_data['blog_post_tags'] = $this->getBlogTagAssignments($blog_id);
                }
            }
        }
        return ['blog' => $blog_data];
    }

    /**
     * This API Command deletes blog articles.
     *
     * @param array{
     *          blog_id: integer
     *        } $data expects an array containing the following array keys.
     *
     * <ul>
     *      <li>$data['blog_id'] - Number - blogmain_id# to delete</li>
     *  <ul>
     * @return array{
     *          blog_id: integer
     *        }
     * @throws Exception
     */
    public function delete(array $data): array
    {
        global $lang;

        $misc = new Misc($this->dbh, $this->config);
        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('can_access_blog_manager');
        // We may need to work out permissions here, the login_status
        // check above presently allows an Admin, Author, Contributor and Editor to do this.
        if ($login_status !== true) {
            throw new Exception('Login Failure');
        }

        //Check that required settings were passed
        if (!isset($data['blog_id'])) {
            throw new Exception(' Invalid Blog ID');
        }

        if ($this->config['demo_mode'] && $_SESSION['admin_privs'] != 'yes') {
            throw new Exception($lang['demo_mode'] . ' - ' . 'Permission Denied');
        }

        $blog_id = intval($data['blog_id']);
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'blogmain  
					WHERE blogmain_id = :blog_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':blog_id' => $blog_id]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->delete', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $deleted = $stmt->rowCount();

        if ($deleted != 1) {
            throw new Exception('Blog Article does not exist');
        }

        $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation  
					WHERE blogmain_id = :blog_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':blog_id' => $blog_id]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->delete', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $sql = 'DELETE FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation  
					WHERE blogmain_id = :blog_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':blog_id' => $blog_id]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->delete', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        //delete the Blog Media Folder
        $dir = $this->config['basepath'] . '/images/blog_uploads/' . $blog_id;
        if (is_dir($dir)) {
            if (!$misc->recurseRmdir($dir)) {
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->delete', 'log_message' => 'Unable To Delete Media Folder: ' . $dir . ' For Blog: ' . $blog_id]);
                throw new Exception('Unable to delete media folder');
            }
        }


        // ta da! we're done...
        $log_api = new LogApi($this->dbh, $this->config);
        $log_api->create([

            'log_api_command' => 'api->blog->delete',
            'log_message' => 'Deleted ' . $blog_id . ' by ' . $_SESSION['username'],
        ]);

        $hooks = new Hooks($this->dbh, $this->config);
        $hooks->load('after_blog_delete', $blog_id);

        return ['blog_id' => $blog_id];
    }

    /**
     * Summary of getPublisherStatus
     *
     * [1 => $lang['blog_perm_subscriber'], 2 => $lang['blog_perm_contributor'], 3 => $lang['blog_perm_author'], 4 =>
     * $lang['blog_perm_editor']]
     *
     * @param int $user_id
     *
     * @return int<0,4>
     */
    private function getPublisherStatus(int $user_id): int
    {
        $user_api = new UserApi($this->dbh, $this->config);

        try {
            $result = $user_api->read([
                'user_id' => $user_id,
                'resource' => 'agent',
                'fields' => ['userdb_blog_user_type'],
            ]);
        } catch (Exception) {
            return 0;
        }

        if (isset($result['user']['userdb_blog_user_type'])) {
            return $result['user']['userdb_blog_user_type'];
        }

        return 0;
    }

    /**
     * @param $blog_id integer
     *
     * @return array<int, string>
     * @throws \Exception
     */
    private function getBlogCategoryNames(int $blog_id): array
    {
        $sql = 'SELECT ' . $this->config['table_prefix'] . 'blogcategory.category_id, category_name 
				FROM ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation, ' . $this->config['table_prefix'] . 'blogcategory 
				WHERE ' . $this->config['table_prefix_no_lang'] . 'blogcategory_relation.category_id =  ' . $this->config['table_prefix'] . 'blogcategory.category_id 
				AND blogmain_id = :blog_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':blog_id' => $blog_id]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        $assigned = [];
        foreach ($recordSet as $record) {
            $assigned[(int)$record['category_id']] = (string)$record['category_name'];
        }
        return $assigned;
    }

    /**
     * @param $blog_id integer
     *
     * @return array<integer, array{
     *          tag_name: string,
     *          tag_seoname: string,
     *          tag_description: string,
     *          tag_link: string
     *         }>
     * @throws \Exception
     */
    private function getBlogTagAssignments(int $blog_id): array
    {
        $page = new PageUser($this->dbh, $this->config);
        $sql = 'SELECT ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation.tag_id, tag_name, tag_seoname, tag_description 
				FROM ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation 
				LEFT JOIN ' . $this->config['table_prefix'] . 'blogtags 
				ON ' . $this->config['table_prefix_no_lang'] . 'blogtag_relation.tag_id = ' . $this->config['table_prefix'] . 'blogtags.tag_id 
				WHERE blogmain_id = :blog_id';
        $stmt = $this->dbh->prepare($sql);
        if (is_bool($stmt)) {
            throw new Exception('Prepairing Statement Failed');
        }
        try {
            $stmt->execute([':blog_id' => $blog_id]);
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $recordSet = $stmt->fetchAll();
        $assigned = [];
        foreach ($recordSet as $record) {
            $tag_id = (int)$record['tag_id'];
            $tag_name = (string)$record['tag_name'];
            $tag_seoname = (string)$record['tag_seoname'];
            $tag_description = (string)$record['tag_description'];
            //Get Tag LInk
            $tag_link = $page->magicURIGenerator('blog_tag', strval($tag_id), true);
            //Get Tag Population
            $assigned[$tag_id] = ['tag_name' => $tag_name, 'tag_seoname' => $tag_seoname, 'tag_description' => $tag_description, 'tag_link' => $tag_link];
        }
        return $assigned;
    }
}
