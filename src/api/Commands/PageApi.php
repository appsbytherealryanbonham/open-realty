<?php

declare(strict_types=1);

namespace OpenRealty\Api\Commands;

use Exception;
use OpenRealty\Login;
use OpenRealty\Misc;
use OpenRealty\PageAdmin;
use PDO;

/**
 * This is the Page  API, it contains all api calls for creating and deleting
 * Page Editor Pages.
 *
 * @package    Open-Realty
 * @subpackage API
 **/
class PageApi extends BaseCommand
{
    /**
     * This API Command creates Page Editor pages.
     *
     * @param array{
     *     pagesmain_title: string,
     *     pagesmain_full?: string,
     *     pagesmain_published?: int<0,1>,
     *     pagesmain_description?: string,
     *     pagesmain_keywords?: string
     *     } $data $data expects an array containing the following array keys.
     *
     *  $data['pagesmain_title'] - TEXT - REQUIRED - The Friendly Title of your Page, e.g.: "About us".
     *  $data['pagesmain_full'] - TEXT - OPTIONAL - The HTML content (markup) to be used for the Page".
     *  $data['pagesmain_published'] - INT - OPTIONAL - The numeric publish status of the Page. 0 = Draft  1= Live.
     *  Default value is: 0/Draft
     *  $data['pagesmain_description'] TEXT - OPTIONAL - HTML Meta Description. A 160 character or less summary of the
     *  page contents.
     *  $data['pagesmain_keywords'] TEXT - OPTIONAL - Comma delimited list of HTML Meta Keywords.
     *
     * @return array{
     *     pagesmain_id: integer
     *  }
     * @throws \Exception
     */
    public function create(array $data): array
    {
        global $lang;

        $login = new Login($this->dbh, $this->config);
        $login_status = $login->verifyPriv('editpages');
        if ($login_status !== true) {
            throw new Exception('Login Failure/no permission');
        }

        $save_pagesmain_full = '';
        $save_pagesmain_published = 0;
        $save_pagesmain_description = '';
        $save_pagesmain_keywords = '';
        $save_pagesmain_summary = '';

        // this field is required, everything else is optional
        if (empty($data['pagesmain_title']) || trim($data['pagesmain_title']) == '') {
            throw new Exception('pagesmain_title: correct_parameter_not_passed');
        } else {
            $save_pagesmain_title = $data['pagesmain_title'];
        }

        if (!empty($data['pagesmain_full'])) {
            $save_full_xhtml = $data['pagesmain_full'];
            //Replace Paths with template tags
            $save_full_xhtml = str_replace($this->config['template_url'], '{template_url}', $save_full_xhtml);
            $save_full_xhtml = str_replace($this->config['baseurl'], '{baseurl}', $save_full_xhtml);
            $save_pagesmain_full = $save_full_xhtml;
        }
        if (!empty($data['pagesmain_published'])) {
            $save_pagesmain_published = intval($data['pagesmain_published']);
        }
        if (!empty($data['pagesmain_description'])) {
            $save_pagesmain_description = $data['pagesmain_description'];
        }
        if (!empty($data['pagesmain_keywords'])) {
            $save_pagesmain_keywords = $data['pagesmain_keywords'];
        }

        //Make sure page Title is unique.
        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . "pagesmain 
				WHERE pagesmain_title = :pagesmain_title";
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }
            $stmt->bindValue(':pagesmain_title', $save_pagesmain_title);
            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $title_count = (int)$stmt->fetchColumn();
        if ($title_count > 0) {
            header('Content-type: application/json');
            throw new Exception('pagesmain_id: ' . $lang['page_title_not_unique']);
        }

        //Generate seo URL
        $page = new PageAdmin($this->dbh, $this->config);
        $seotitle = $page->createSeoUri($data['pagesmain_title'], false);

        $sql = 'INSERT INTO ' . $this->config['table_prefix'] . "pagesmain
		                (pagesmain_full, pagesmain_title, pagesmain_date, pagesmain_published, pagesmain_description, pagesmain_keywords, pagesmain_summary, pagesmain_full_autosave, page_seotitle)
		        VALUES (:pagesmain_full,:pagesmain_title,:pagesmain_date,:pagesmain_published,:pagesmain_description,:pagesmain_keywords, :pagesmain_summary, :pagesmain_full_autosave, :seotitle)";
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }

            $stmt->bindValue(':pagesmain_published', $save_pagesmain_published, PDO::PARAM_INT);
            $stmt->bindValue(':pagesmain_full', $save_pagesmain_full);
            $stmt->bindValue(':pagesmain_title', $save_pagesmain_title);
            $stmt->bindValue(':pagesmain_date', date("Y-m-d", time()));
            $stmt->bindValue(':pagesmain_description', $save_pagesmain_description);
            $stmt->bindValue(':pagesmain_keywords', $save_pagesmain_keywords);
            $stmt->bindValue(':pagesmain_summary', $save_pagesmain_summary);
            $stmt->bindValue(':pagesmain_full_autosave', '');
            $stmt->bindValue(':seotitle', $seotitle);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }

        $pagesmain_id = $this->dbh->lastInsertId();
        if (is_bool($pagesmain_id)) {
            throw new Exception('Failed Getting Last Insert ID');
        }
        $pagesmain_id = (int)$pagesmain_id;

        if (!file_exists($this->config['basepath'] . '/images/page_upload/' . $pagesmain_id)) {
            mkdir($this->config['basepath'] . '/images/page_upload/' . $pagesmain_id);
        }

        //Verify the SEO title is unique
        $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE page_seotitle = :seotitle 
				AND pagesmain_id <> :pagesmain_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }

            $stmt->bindValue(':pagesmain_id', $pagesmain_id, PDO::PARAM_INT);
            $stmt->bindValue(':seotitle', $seotitle);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        $title_count = (int)$stmt->fetchColumn();
        if ($title_count > 0) {
            $seotitle = $page->createSeoUri($data['pagesmain_title'] . '-' . $pagesmain_id, false);
            $sql = 'UPDATE ' . $this->config['table_prefix'] . "pagesmain 
					SET page_seotitle = :seotitle 
					WHERE pagesmain_id = :pagesmain_id";
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }

                $stmt->bindValue(':pagesmain_id', $pagesmain_id, PDO::PARAM_INT);
                $stmt->bindValue(':seotitle', $seotitle);

                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
        }

        $log_api = new LogApi($this->dbh, $this->config);
        $log_api->create(['log_api_command' => 'api->page->create', 'log_message' => 'Page Created: ' . $data['pagesmain_title'] . '(' . $pagesmain_id . ')']);
        return ['pagesmain_id' => $pagesmain_id];
    }

    /**
     * This API Command deletes listings.
     *
     * @param array{
     *     pagesmain_id: integer
     *  } $data $data expects an array containing the following array key.
     *
     * $data['pagesmain_id'] - INT - REQUIRED - Listing ID to delete
     *
     * @returns array{
     *     pagesmain_id: integer
     *  }
     * @throws \Exception
     */
    public function delete(array $data): array
    {
        global $lang;

        $misc = new Misc($this->dbh, $this->config);
        if (!isset($data['pagesmain_id'])) {
            throw new Exception('pagesmain_id: correct_parameter_not_passed');
        }

        $login = new Login($this->dbh, $this->config);

        $login_status = $login->verifyPriv('editpages');
        if ($login_status !== true) {
            throw new Exception('Login Failure/no permission');
        }
        //Don't allow deletion of Page 1 as it is the index page
        if ($data['pagesmain_id'] == 1) {
            throw new Exception($lang['page_1_can_not_delete']);
        }
        $sql = 'DELETE FROM ' . $this->config['table_prefix'] . 'pagesmain  
				WHERE pagesmain_id = :pagesmain_id';
        try {
            $stmt = $this->dbh->prepare($sql);
            if (is_bool($stmt)) {
                throw new Exception('Prepairing Statement Failed');
            }

            $stmt->bindValue(':pagesmain_id', $data['pagesmain_id'], PDO::PARAM_INT);

            $stmt->execute();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
            throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
        }
        //get any files
        $dir = $this->config['basepath'] . '/images/page_upload/' . $data['pagesmain_id'];
        if ($dir == $this->config['basepath']) {
            throw new Exception($data['pagesmain_id'] . " No folder present");
        } elseif (file_exists($dir)) {
            if (!$misc->recurseRmdir($dir)) {
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->delete', 'log_message' => 'Unable To Delete Media Folder: ' . $dir . ' For Page: ' . $data['pagesmain_id']]);
                throw new Exception('Unable to delete media folder');
            }
        }
        $log_api = new LogApi($this->dbh, $this->config);
        $log_api->create(['log_api_command' => 'api->page->delete', 'log_message' => 'Page Deleted: ' . $data['pagesmain_id']]);
        return ['pagesmain_id' => $data['pagesmain_id']];
    }

    /**
     * @param array{
     *     pagesmain_id: integer,
     *     pagesmain_title?: string,
     *     page_seotitle?: string,
     *     pagesmain_full?: string,
     *     pagesmain_published?: int<0,1>,
     *     pagesmain_description?: string,
     *     pagesmain_keywords?: string
     *     } $data
     * @return array{
     *     pagesmain_id: integer
     *  }
     * @throws \Exception
     */
    public function update(array $data): array
    {
        global $lang;
        $login = new Login($this->dbh, $this->config);

        $login_status = $login->verifyPriv('editpages');
        if ($login_status !== true) {
            throw new Exception('Login Failure/no permission');
        }

        if (!isset($data['pagesmain_id'])) {
            throw new Exception('pagesmain_id: correct_parameter_not_passed');
        } else {
            $pagesmain_id = intval($data['pagesmain_id']);
        }
        $page = new PageAdmin($this->dbh, $this->config);
        $dbIntParam = [];
        $dbStrParam = [];
        if (isset($data['pagesmain_title']) && !empty($data['pagesmain_title'])) {
            $sql = 'SELECT 1 FROM ' . $this->config['table_prefix'] . "pagesmain 
					WHERE pagesmain_title = :pagesmain_title
					AND pagesmain_id <> :pagesmain_id";
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                $stmt->bindValue(':pagesmain_id', $pagesmain_id, PDO::PARAM_INT);
                $stmt->bindValue('pagesmain_title', $data['pagesmain_title']);

                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $title_count = (int)$stmt->fetchColumn();
            if ($title_count > 0) {
                throw new Exception('pagesmain_title: ' . $lang['page_title_not_unique']);
            } else {
                $dbStrParam['pagesmain_title'] = $data['pagesmain_title'];
                $page_seotitle = $page->createSeoUri($data['pagesmain_title'], false);
                $dbStrParam['page_seotitle'] = $page_seotitle;
            }
        }
        if (isset($data['pagesmain_full']) && !empty($data['pagesmain_full'])) {
            $save_full_xhtml = $data['pagesmain_full'];
            //Replace Paths with template tags
            $save_full_xhtml = str_replace($this->config['template_url'], '{template_url}', $save_full_xhtml);
            $save_full_xhtml = str_replace($this->config['baseurl'], '{baseurl}', $save_full_xhtml);
            $dbStrParam['pagesmain_full'] = $save_full_xhtml;
        }
        if (isset($data['pagesmain_published']) && !empty($data['pagesmain_published'])) {
            $dbIntParam['pagesmain_published'] = intval($data['pagesmain_published']);
        }
        if (isset($data['pagesmain_description']) && !empty($data['pagesmain_description'])) {
            $dbStrParam['pagesmain_description'] = $data['pagesmain_description'];
        }
        if (isset($data['pagesmain_keywords']) && !empty($data['pagesmain_keywords'])) {
            $dbStrParam['pagesmain_keywords'] = $data['pagesmain_keywords'];
        }
        if (isset($data['page_seotitle']) && !empty($data['page_seotitle'])) {
            $dbStrParam['page_seotitle'] = $data['page_seotitle'];
        }
        if (isset($dbStrParam['page_seotitle'])) {
            //Verify the SEO title is unique.
            $sql = 'SELECT pagesmain_id FROM ' . $this->config['table_prefix'] . 'pagesmain 
				WHERE page_seotitle = :page_seotitle 
				AND pagesmain_id <> :pagesmain_id';
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                $stmt->bindValue(':pagesmain_id', $pagesmain_id, PDO::PARAM_INT);
                $stmt->bindValue(':page_seotitle', $dbStrParam['page_seotitle']);

                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $title_count = (int)$stmt->fetchColumn();
            if ($title_count > 0) {
                if (isset($data['page_seotitle'])) {
                    $seotitle = $page->createSeoUri($data['page_seotitle'] . '-' . $pagesmain_id, false);
                    $dbStrParam['page_seotitle'] = $seotitle;
                } elseif (isset($data['pagesmain_title'])) {
                    $seotitle = $page->createSeoUri($data['pagesmain_title'] . '-' . $pagesmain_id, false);
                    $dbStrParam['page_seotitle'] = $seotitle;
                }
            }
        }
        $sql_a = [];
        foreach ($dbStrParam as $field => $value) {
            $sql_a[] = $field . ' = :' . $field;
        }
        foreach ($dbIntParam as $field => $value) {
            $sql_a[] = $field . ' = :' . $field;
        }
        if (!empty($sql_a)) {
            $sql = 'UPDATE ' . $this->config['table_prefix'] . 'pagesmain 
				SET ' . implode(',', $sql_a) . ' 
				WHERE pagesmain_id = :pagesmain_id';
            try {
                $stmt = $this->dbh->prepare($sql);
                if (is_bool($stmt)) {
                    throw new Exception('Prepairing Statement Failed');
                }
                foreach ($dbIntParam as $p => $v) {
                    $stmt->bindValue(':' . $p, $v, PDO::PARAM_INT);
                }
                $stmt->bindValue(':pagesmain_id', $pagesmain_id, PDO::PARAM_INT);
                foreach ($dbStrParam as $p => $v) {
                    $stmt->bindValue(':' . $p, $v);
                }
                $stmt->execute();
            } catch (Exception $e) {
                $error = $e->getMessage();
                $log_api = new LogApi($this->dbh, $this->config);
                $log_api->create(['log_api_command' => 'api->blog->search', 'log_message' => 'DB Error: ' . $error . ' Full SQL: ' . $sql]);
                throw new Exception('DB Error: ' . $error . "\r\n" . 'SQL: ' . $sql);
            }
            $log_api = new LogApi($this->dbh, $this->config);
            $log_api->create(['log_api_command' => 'api->page->update', 'log_message' => 'Page Updated: ' . $pagesmain_id]);
            return ['pagesmain_id' => $pagesmain_id];
        } else {
            throw new Exception('No Fields Found to Update');
        }
    }
}
