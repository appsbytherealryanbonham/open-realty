<?php

namespace Tests\Support;

use OpenRealty\Login;
use PDO;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class IntegrationTester extends \Codeception\Actor
{
    use _generated\IntegrationTesterActions;

    /**
     * Define custom actions here
     */
    public function loginAgent(PDO $dbh, array $config, string $user, string $pass): bool
    {
        $_POST['user_name'] = $user;
        $_POST['user_pass'] = $pass;
        $login = new Login($dbh, $config);
        $login_status = $login->loginCheck('Agent', true);
        if ($login_status !== true) {
            return false;
        }
        return true;
    }
}
