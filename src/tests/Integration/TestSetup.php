<?php

namespace Tests\Integration;

use Codeception\Test\Unit;
use OpenRealty\Api\Api;
use OpenRealty\Api\Commands\SettingsApi;
use OpenRealty\Api\Database;
use OpenRealty\Install\BaseInstall;
use PDO;
use Tests\Support\IntegrationTester;

/**
 * Undocumented class
 */
abstract class TestSetup extends Unit
{
    /**
     * @var IntegrationTester
     */
    protected $tester;

    protected PDO $dbh;

    protected array $config = [];

    protected string $int_user = 'admin';
    protected string $int_pass = 'password';

    private function setupGetVars(): void
    {
        $_GET['step'] = "autoinstall";
        $_GET['or_install_lang'] = "en";
        $_GET['or_install_type'] = "Installer";
        $_GET['table_prefix'] = "default_";
        $_GET['db_type'] = "mysqli";
        $_GET['db_server'] = "db";
        $_GET['db_user'] = "openrealty";
        $_GET['db_password'] = "orpassword";
        $_GET['db_database'] = "openrealty";
        $_GET['basepath'] = dirname(__FILE__, 3);
        $_GET['baseurl'] = "http://web.local";
        $_GET['default_email'] = "rb2297+integration-tests@gmail.com";
    }

    private function cleanupAll(): void
    {
        $_SESSION = [];
        @session_destroy();
        $_GET = [];
        $this->tester->deleteDir(dirname(__FILE__, 3) . '/images/listing_photos');
        $this->tester->deleteDir(dirname(__FILE__, 3) . '/images/blog_uploads');
        $this->tester->deleteDir(dirname(__FILE__, 3) . '/images/page_uploads');
        $this->tester->deleteDir(dirname(__FILE__, 3) . '/images/user_photos');
        $this->tester->deleteDir(dirname(__FILE__, 3) . '/images/vtour_photos');
        $testHook = dirname(__FILE__, 3) . '/hooks/TestingHooks.php';
        if (file_exists($testHook)) {
            unlink($testHook);
        }
        $testAddon = dirname(__FILE__, 3) . '/addons/TestAddon';
        if (file_exists($testAddon)) {
            $this->tester->deleteDir($testAddon);
        }
        $this->tester->copyDir(dirname(__FILE__, 3) . '/tests/_assets/images', dirname(__FILE__, 3) . '/images');
    }

    // phpcs:ignore
    protected function _afterSuite(): void
    {
        $this->cleanupAll();
    }


    // phpcs:ignore
    protected function _before()
    {
        $this->cleanupAll();
        $this->setupGetVars();
        $installer = new BaseInstall();
        $installer->dropAllTables($_GET['db_type'], $_GET['db_server'], $_GET['db_user'], $_GET['db_password'], $_GET['db_database']);
        $installer->runInstaller();
        $this->cleanupAll();
        //Needed to load Adodb until replacement is done
        new Api();
        $apiDB = new Database();
        $this->dbh = $apiDB->getDatabaseHandler();
        $settingApi = new SettingsApi($this->dbh);
        $this->config = $config = $settingApi->read(['is_mobile' => false])['config'];
        /** @psalm-suppress UnresolvableInclude */
        include_once $this->config['basepath'] . '/include/language/' . $this->config['lang'] . '/lang.inc.php';
        session_start([
            "cookie_httponly" => true,
            "cookie_samesite" => "Lax",
            "cookie_secure" => str_contains($this->config["baseurl"], "https://"),
            "use_strict_mode" => true,
            "sid_bits_per_character" => 6,
        ]);
        parent::_before();
    }

    // phpcs:ignore
    protected function _after()
    {
        @session_destroy();
        parent::_after();
    }
}
